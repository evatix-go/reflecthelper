package reflectreq

import (
	"fmt"
	"reflect"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
)

type MismatchTagStruct struct {
	FieldQueryName, Expected string
	Actual                   reflect.StructTag
}

func (it MismatchTagStruct) String() string {
	return fmt.Sprintf(
		consts.QueryFieldNameToActualExpectedLogFormat,
		it.FieldQueryName,
		it.Actual,
		it.Expected)
}
