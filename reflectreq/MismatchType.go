package reflectreq

import (
	"fmt"
	"reflect"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
)

type MismatchType struct {
	FieldQueryName   string
	Actual, Expected reflect.Type
}

func (it MismatchType) String() string {
	return fmt.Sprintf(
		consts.QueryFieldNameToActualExpectedLogFormat,
		it.FieldQueryName,
		it.Actual,
		it.Expected)
}
