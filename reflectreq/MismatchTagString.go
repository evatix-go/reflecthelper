package reflectreq

import (
	"fmt"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
)

type MismatchTagString struct {
	FieldQueryName   string
	Actual, Expected string
}

func (it MismatchTagString) String() string {
	return fmt.Sprintf(
		consts.QueryFieldNameToActualExpectedLogFormat,
		it.FieldQueryName,
		it.Actual,
		it.Expected)
}
