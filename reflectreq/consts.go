package reflectreq

const (
	missingColon             = "Missing :"
	typesExpectationMessage  = "Type(s) expectation failed:"
	valuesExpectationMessage = "Value(s) expectation failed:"
	tagsExpectationMessage   = "Tag(s) expectation failed:"
)
