package reflectreq

var (
	// ChildResponseOf
	//
	//  Get nested field quires response results
	//  using request.
	//
	// fieldQuery : field1.field2.field3.field4
	// It will expand  field queries ("field1.field2.field3.field4") each level of field and return the final map.
	//
	// What is fieldNameQuery?
	//
	//  - It can be a field name or targeting nested field names like F1.F2.F3
	//  - Here, F1 is the first field then we will find F2 inside F1, and finally F3 under f2 field.
	ChildResponseOf = &childResponseOf{}
)
