package reflectreq

import (
	"errors"
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
)

type ChildFieldValuesResponse struct {
	InputType          reflect.Type
	FoundItemsMap      map[string]interface{} // Key -> "Field1.Field2.Field3", Val => which matches expectation
	MissingQueries     []string
	MismatchFields     map[string]MismatchValue // Key -> "Field1.Field2.Field3"
	verifyError        error
	verifyErrorMessage *string
	IsOkay             bool
}

func (it *ChildFieldValuesResponse) VerifyError() error {
	if it == nil || it.IsOkay {
		return nil
	}

	if it.verifyError != nil {
		return it.verifyError
	}

	message := it.VerifyErrorMessage()
	it.verifyError = errors.New(message)

	return it.verifyError
}

func (it *ChildFieldValuesResponse) HandleError() {
	if it == nil || it.IsOkay {
		return
	}

	err := it.VerifyError()

	panic(err)
}

func (it *ChildFieldValuesResponse) PrintOnError() {
	if it == nil || it.IsOkay {
		return
	}

	err := it.VerifyError()

	fmt.Println(err)
}

func (it *ChildFieldValuesResponse) IsSuccess() bool {
	return it != nil && it.IsOkay
}

func (it *ChildFieldValuesResponse) IsValid() bool {
	return it != nil && it.IsOkay
}

func (it *ChildFieldValuesResponse) IsFailed() bool {
	return it == nil || !it.IsOkay
}

func (it *ChildFieldValuesResponse) MissingQueriesLength() int {
	if it == nil {
		return 0
	}

	return len(it.MissingQueries)
}

func (it *ChildFieldValuesResponse) MismatchFieldsLength() int {
	if it == nil {
		return 0
	}

	return len(it.MismatchFields)
}

func (it *ChildFieldValuesResponse) VerifyErrorMessage() string {
	if it == nil || it.IsOkay {
		return ""
	}

	if it.verifyErrorMessage != nil {
		return *it.verifyErrorMessage
	}

	mismatchLength := it.MismatchFieldsLength()
	errSlice := make(
		[]string,
		0,
		mismatchLength+3)

	errSlice = append(
		errSlice,
		"Type : "+it.InputType.String())

	if mismatchLength > 0 {
		missingElements := missingElementsCompiledMessage(
			it.MissingQueries)

		errSlice = append(
			errSlice,
			missingElements)
	}

	if it.MissingQueriesLength() > 0 {
		errSlice = append(
			errSlice,
			valuesExpectationMessage)
		for _, mismatchValue := range it.MismatchFields {
			errSlice = append(
				errSlice,
				mismatchValue.String())
		}
	}

	verifyErrorMessage := strings.Join(
		errSlice,
		consts.DefaultNewLine)
	it.verifyErrorMessage = &verifyErrorMessage

	return *it.verifyErrorMessage
}
