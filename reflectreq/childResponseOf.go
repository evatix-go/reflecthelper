package reflectreq

import (
	"gitlab.com/evatix-go/reflecthelper/reflectfield"
)

type childResponseOf struct{}

func (it *childResponseOf) Tags(
	request *ChildFieldTagsExpectationRequest,
) *ChildFieldTagsResponse {
	fieldsMap := reflectfield.
		New.
		StructFieldsMap.
		AnyDefault(request.RawInputStruct)

	foundQueriesMap, missingElements := fieldsMap.
		FieldNameQuery().
		MultipleToTagsMap(
			request.FieldNameQueries()...)

	mismatchMap := make(
		map[string]MismatchTagStruct,
		request.Length()/3)

	for fieldQueryKey, actualTag := range foundQueriesMap {
		expectedTag := request.FieldsNameQueryRequestMap[fieldQueryKey]

		if expectedTag == string(actualTag) {
			continue
		}

		// not matching
		mismatchMap[fieldQueryKey] = MismatchTagStruct{
			FieldQueryName: fieldQueryKey,
			Actual:         actualTag,
			Expected:       expectedTag,
		}
	}

	return &ChildFieldTagsResponse{
		InputType:      fieldsMap.Type,
		FoundItemsMap:  foundQueriesMap,
		MissingQueries: missingElements,
		MismatchFields: mismatchMap,
		IsOkay: len(missingElements) == 0 &&
			len(mismatchMap) == 0,
	}
}

func (it *childResponseOf) TagsString(
	request *ChildFieldTagsExpectationRequest,
) *ChildFieldTagsStringResponse {
	fieldsMap := reflectfield.
		New.
		StructFieldsMap.
		AnyDefault(request.RawInputStruct)

	foundQueriesMap, missingElements := fieldsMap.
		FieldNameQuery().
		MultipleToTagsMap(
			request.FieldNameQueries()...)

	mismatchMap := make(
		map[string]MismatchTagString,
		request.Length()/3)

	foundQueriesActualMap := make(
		map[string]string,
		request.Length())

	for fieldQueryKey, actualTag := range foundQueriesMap {
		expectedTag := request.FieldsNameQueryRequestMap[fieldQueryKey]
		actualString := string(actualTag)
		if expectedTag == actualString {
			continue
		}

		// not matching
		mismatchMap[fieldQueryKey] = MismatchTagString{
			FieldQueryName: fieldQueryKey,
			Actual:         actualString,
			Expected:       expectedTag,
		}
	}

	for s, tag := range foundQueriesMap {
		foundQueriesActualMap[s] = string(tag)
	}

	return &ChildFieldTagsStringResponse{
		InputType:      fieldsMap.Type,
		FoundItemsMap:  foundQueriesActualMap,
		MissingQueries: missingElements,
		MismatchFields: mismatchMap,
		IsOkay: len(missingElements) == 0 &&
			len(mismatchMap) == 0,
	}
}

func (it *childResponseOf) TypesUsingValues(
	request *ChildFieldTypesValueExpectationRequest,
) *ChildFieldTypesResponse {
	newReq := request.
		ChildFieldTypesExpectationRequest()

	return it.Types(newReq)
}

func (it *childResponseOf) Types(
	request *ChildFieldTypesExpectationRequest,
) *ChildFieldTypesResponse {
	fieldsMap := reflectfield.
		New.
		StructFieldsMap.
		AnyDefault(request.RawInputStruct)

	foundQueriesMap, missingElements := fieldsMap.
		FieldNameQuery().
		MultipleToTypeMap(
			request.FieldNameQueries()...)

	mismatchMap := make(
		map[string]MismatchType,
		request.Length()/3)

	for fieldQueryKey, actualType := range foundQueriesMap {
		expectedType := request.FieldsNameQueryRequestMap[fieldQueryKey]

		if expectedType == actualType {
			continue
		}

		// not matching
		mismatchMap[fieldQueryKey] = MismatchType{
			FieldQueryName: fieldQueryKey,
			Actual:         actualType,
			Expected:       expectedType,
		}
	}

	return &ChildFieldTypesResponse{
		InputType:      fieldsMap.Type,
		FoundItemsMap:  foundQueriesMap,
		MissingQueries: missingElements,
		MismatchFields: mismatchMap,
		IsOkay: len(missingElements) == 0 &&
			len(mismatchMap) == 0,
	}
}

func (it *childResponseOf) AllValues(
	request *ChildFieldValuesExpectationRequest,
) *ChildFieldValuesResponse {
	return it.ValuesUsingFunc(
		defaultAllValueGetter,
		request)
}

func (it *childResponseOf) PublicValues(
	request *ChildFieldValuesExpectationRequest,
) *ChildFieldValuesResponse {
	return it.ValuesUsingFunc(
		defaultPublicValueGetter,
		request)
}

func (it *childResponseOf) ValuesUsingFunc(
	valuesProcessorFunc func(wrapper *reflectfield.Wrapper) (val interface{}),
	request *ChildFieldValuesExpectationRequest,
) *ChildFieldValuesResponse {
	fieldsMap := reflectfield.
		New.
		StructFieldsMap.
		AnyDefault(request.RawInputStruct)

	foundQueriesMap, missingElements := fieldsMap.
		FieldNameQuery().
		MultipleToValuesMap(
			valuesProcessorFunc,
			request.FieldNameQueries()...)

	mismatchMap := make(
		map[string]MismatchValue,
		request.Length()/3)

	for fieldQueryKey, actualOrCurrentValue := range foundQueriesMap {
		expectedValue := request.FieldsNameQueryRequestMap[fieldQueryKey]

		if expectedValue == actualOrCurrentValue {
			// TODO fix compare method
			continue
		}

		// not matching
		mismatchMap[fieldQueryKey] = MismatchValue{
			FieldQueryName: fieldQueryKey,
			Actual:         actualOrCurrentValue,
			Expected:       expectedValue,
		}
	}

	return &ChildFieldValuesResponse{
		InputType:      fieldsMap.Type,
		FoundItemsMap:  foundQueriesMap,
		MissingQueries: missingElements,
		MismatchFields: mismatchMap,
		IsOkay: len(missingElements) == 0 &&
			len(mismatchMap) == 0,
	}
}
