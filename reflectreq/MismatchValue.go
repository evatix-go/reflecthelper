package reflectreq

import (
	"fmt"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
)

type MismatchValue struct {
	FieldQueryName   string
	Actual, Expected interface{}
}

func (it MismatchValue) String() string {
	return fmt.Sprintf(
		consts.QueryFieldNameToActualExpectedLogFormat,
		it.FieldQueryName,
		it.Actual,
		it.Expected)
}
