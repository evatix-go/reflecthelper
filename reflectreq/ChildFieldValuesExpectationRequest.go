package reflectreq

type ChildFieldValuesExpectationRequest struct {
	RawInputStruct            interface{}
	FieldsNameQueryRequestMap map[string]interface{} // Key =>"Field1.Field2.Field3" (Field name query), val => expected values
}

func (it *ChildFieldValuesExpectationRequest) Length() int {
	if it == nil {
		return 0
	}

	return len(it.FieldsNameQueryRequestMap)
}

func (it *ChildFieldValuesExpectationRequest) IsEmpty() bool {
	return it.Length() == 0
}

func (it *ChildFieldValuesExpectationRequest) FieldNameQueries() []string {
	if it == nil || len(it.FieldsNameQueryRequestMap) == 0 {
		return nil
	}

	list := make(
		[]string,
		0,
		it.Length())

	for fieldQueryKey := range it.FieldsNameQueryRequestMap {
		list = append(list, fieldQueryKey)
	}

	return list
}
