package reflectreq

import "gitlab.com/evatix-go/reflecthelper/reflectfield"

var (
	defaultPublicValueGetter = func(wrapper *reflectfield.Wrapper) (val interface{}) {
		if wrapper == nil || !wrapper.IsPublic {
			return nil
		}

		return wrapper.PublicValue()
	}

	defaultAllValueGetter = func(wrapper *reflectfield.Wrapper) (val interface{}) {
		if wrapper == nil {
			return nil
		}

		return wrapper.ValueInterfaceOrError()
	}
)
