package reflectreq

import (
	"errors"
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
)

type ChildFieldTagsStringResponse struct {
	InputType          reflect.Type
	FoundItemsMap      map[string]string // Key -> "Field1.Field2.Field3", Val => which matches expectation
	MissingQueries     []string
	MismatchFields     map[string]MismatchTagString // Key -> "Field1.Field2.Field3"
	verifyError        error
	verifyErrorMessage *string
	IsOkay             bool
}

func (it *ChildFieldTagsStringResponse) VerifyError() error {
	if it == nil || it.IsOkay {
		return nil
	}

	if it.verifyError != nil {
		return it.verifyError
	}

	message := it.VerifyErrorMessage()
	it.verifyError = errors.New(message)

	return it.verifyError
}

func (it *ChildFieldTagsStringResponse) HandleError() {
	if it == nil || it.IsOkay {
		return
	}

	err := it.VerifyError()

	panic(err)
}

func (it *ChildFieldTagsStringResponse) PrintOnError() {
	if it == nil || it.IsOkay {
		return
	}

	err := it.VerifyError()

	fmt.Println(err)
}

func (it *ChildFieldTagsStringResponse) IsSuccess() bool {
	return it != nil && it.IsOkay
}

func (it *ChildFieldTagsStringResponse) IsValid() bool {
	return it != nil && it.IsOkay
}

func (it *ChildFieldTagsStringResponse) IsFailed() bool {
	return it == nil || !it.IsOkay
}

func (it *ChildFieldTagsStringResponse) MissingQueriesLength() int {
	if it == nil {
		return 0
	}

	return len(it.MissingQueries)
}

func (it *ChildFieldTagsStringResponse) MismatchFieldsLength() int {
	if it == nil {
		return 0
	}

	return len(it.MismatchFields)
}

func (it *ChildFieldTagsStringResponse) VerifyErrorMessage() string {
	if it == nil || it.IsOkay {
		return ""
	}

	if it.verifyErrorMessage != nil {
		return *it.verifyErrorMessage
	}

	mismatchLength := it.MismatchFieldsLength()
	errSlice := make(
		[]string,
		0,
		mismatchLength+3)

	errSlice = append(
		errSlice,
		"Type : "+it.InputType.String())

	if it.MissingQueriesLength() > 0 {
		missingElements := missingElementsCompiledMessage(
			it.MissingQueries)

		errSlice = append(
			errSlice,
			missingElements)
	}

	if mismatchLength > 0 {
		errSlice = append(
			errSlice,
			tagsExpectationMessage)
		for _, mismatchTagString := range it.MismatchFields {
			errSlice = append(
				errSlice,
				mismatchTagString.String())
		}
	}

	verifyErrorMessage := strings.Join(
		errSlice,
		consts.DefaultNewLine)
	it.verifyErrorMessage = &verifyErrorMessage

	return *it.verifyErrorMessage
}
