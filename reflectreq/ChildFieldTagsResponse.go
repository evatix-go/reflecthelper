package reflectreq

import (
	"errors"
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
)

type ChildFieldTagsResponse struct {
	InputType          reflect.Type
	FoundItemsMap      map[string]reflect.StructTag // Key -> "Field1.Field2.Field3", Val => which matches expectation
	MissingQueries     []string
	MismatchFields     map[string]MismatchTagStruct // Key -> "Field1.Field2.Field3"
	verifyError        error
	verifyErrorMessage *string
	IsOkay             bool
}

func (it *ChildFieldTagsResponse) VerifyError() error {
	if it == nil || it.IsOkay {
		return nil
	}

	if it.verifyError != nil {
		return it.verifyError
	}

	message := it.VerifyErrorMessage()
	it.verifyError = errors.New(message)

	return it.verifyError
}

func (it *ChildFieldTagsResponse) HandleError() {
	if it == nil || it.IsOkay {
		return
	}

	err := it.VerifyError()

	panic(err)
}

func (it *ChildFieldTagsResponse) PrintOnError() {
	if it == nil || it.IsOkay {
		return
	}

	err := it.VerifyError()

	fmt.Println(err)
}

func (it *ChildFieldTagsResponse) IsSuccess() bool {
	return it != nil && it.IsOkay
}

func (it *ChildFieldTagsResponse) IsValid() bool {
	return it != nil && it.IsOkay
}

func (it *ChildFieldTagsResponse) IsFailed() bool {
	return it == nil || !it.IsOkay
}

func (it *ChildFieldTagsResponse) MissingQueriesLength() int {
	if it == nil {
		return 0
	}

	return len(it.MissingQueries)
}

func (it *ChildFieldTagsResponse) MismatchFieldsLength() int {
	if it == nil {
		return 0
	}

	return len(it.MismatchFields)
}

func (it *ChildFieldTagsResponse) VerifyErrorMessage() string {
	if it == nil || it.IsOkay {
		return ""
	}

	if it.verifyErrorMessage != nil {
		return *it.verifyErrorMessage
	}

	mismatchLength := it.MismatchFieldsLength()
	errSlice := make(
		[]string,
		0,
		mismatchLength+3)

	errSlice = append(
		errSlice,
		"Type : "+it.InputType.String())

	if it.MissingQueriesLength() > 0 {
		missingElements := missingElementsCompiledMessage(
			it.MissingQueries)

		errSlice = append(
			errSlice,
			missingElements)
	}

	if mismatchLength > 0 {
		errSlice = append(
			errSlice,
			tagsExpectationMessage)
		for _, mismatchInfo := range it.MismatchFields {
			errSlice = append(
				errSlice,
				mismatchInfo.String())
		}
	}

	verifyErrorMessage := strings.Join(
		errSlice,
		consts.DefaultNewLine)

	it.verifyErrorMessage = &verifyErrorMessage

	return *it.verifyErrorMessage
}
