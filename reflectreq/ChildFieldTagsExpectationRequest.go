package reflectreq

type ChildFieldTagsExpectationRequest struct {
	RawInputStruct            interface{}
	FieldsNameQueryRequestMap map[string]string // Key =>"Field1.Field2.Field3" (Field name query), Value => Tag Struct Name
}

func (it *ChildFieldTagsExpectationRequest) Length() int {
	if it == nil {
		return 0
	}

	return len(it.FieldsNameQueryRequestMap)
}

func (it *ChildFieldTagsExpectationRequest) IsEmpty() bool {
	return it.Length() == 0
}

func (it *ChildFieldTagsExpectationRequest) FieldNameQueries() []string {
	if it == nil || len(it.FieldsNameQueryRequestMap) == 0 {
		return nil
	}

	list := make(
		[]string,
		0,
		it.Length())

	for fieldQueryKey := range it.FieldsNameQueryRequestMap {
		list = append(list, fieldQueryKey)
	}

	return list
}
