package reflectreq

import (
	"sort"
	"strings"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
)

func missingElementsCompiledMessage(missingFields []string) string {
	if len(missingFields) == 0 {
		return ""
	}

	missingElements := make(
		[]string,
		0,
		len(missingFields)+2)

	missingElements = append(
		missingElements,
		missingColon)

	sort.Strings(missingFields)

	for _, missingField := range missingFields {
		missingElements = append(
			missingElements,
			missingField)
	}

	return strings.Join(
		missingElements,
		consts.NewLineTabJoiner)
}
