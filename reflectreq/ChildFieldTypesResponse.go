package reflectreq

import (
	"errors"
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
)

type ChildFieldTypesResponse struct {
	InputType          reflect.Type
	FoundItemsMap      map[string]reflect.Type // Key -> "Field1.Field2.Field3" , Val => Which matches expectation.
	MissingQueries     []string
	MismatchFields     map[string]MismatchType // Key -> "Field1.Field2.Field3"
	verifyError        error
	verifyErrorMessage *string
	IsOkay             bool
}

func (it *ChildFieldTypesResponse) VerifyError() error {
	if it == nil || it.IsOkay {
		return nil
	}

	if it.verifyError != nil {
		return it.verifyError
	}

	message := it.VerifyErrorMessage()
	it.verifyError = errors.New(message)

	return it.verifyError
}

func (it *ChildFieldTypesResponse) HandleError() {
	if it == nil || it.IsOkay {
		return
	}

	err := it.VerifyError()

	panic(err)
}

func (it *ChildFieldTypesResponse) PrintOnError() {
	if it == nil || it.IsOkay {
		return
	}

	err := it.VerifyError()

	fmt.Println(err)
}

func (it *ChildFieldTypesResponse) IsSuccess() bool {
	return it != nil && it.IsOkay
}

func (it *ChildFieldTypesResponse) IsValid() bool {
	return it != nil && it.IsOkay
}

func (it *ChildFieldTypesResponse) IsFailed() bool {
	return it == nil || !it.IsOkay
}

func (it *ChildFieldTypesResponse) MissingQueriesLength() int {
	if it == nil {
		return 0
	}

	return len(it.MissingQueries)
}

func (it *ChildFieldTypesResponse) MismatchFieldsLength() int {
	if it == nil {
		return 0
	}

	return len(it.MismatchFields)
}

func (it *ChildFieldTypesResponse) VerifyErrorMessage() string {
	if it == nil || it.IsOkay {
		return ""
	}

	if it.verifyErrorMessage != nil {
		return *it.verifyErrorMessage
	}

	mismatchLength := it.MismatchFieldsLength()
	errSlice := make(
		[]string,
		0,
		mismatchLength+3)

	errSlice = append(
		errSlice,
		"Type : "+it.InputType.String())

	if it.MissingQueriesLength() > 0 {
		missingElements := missingElementsCompiledMessage(
			it.MissingQueries)

		errSlice = append(
			errSlice,
			missingElements)
	}

	if mismatchLength > 0 {
		errSlice = append(
			errSlice,
			typesExpectationMessage)
		for _, mismatchType := range it.MismatchFields {
			errSlice = append(
				errSlice,
				mismatchType.String())
		}
	}

	verifyErrorMessage := strings.Join(
		errSlice,
		consts.DefaultNewLine)
	it.verifyErrorMessage = &verifyErrorMessage

	return *it.verifyErrorMessage
}
