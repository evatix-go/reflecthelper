package reflectcast

import (
	"fmt"
)

// FieldsErrorMap is a map[string]error wrapper
type FieldsErrorMap struct {
	items *map[string]error
}

// NewFieldsErrorMap creates a new FieldsErrorMap object and returns a pointer to it
func NewFieldsErrorMap() *FieldsErrorMap {
	items := make(map[string]error)

	return &FieldsErrorMap{
		items: &items,
	}
}

func (fieldErrMap *FieldsErrorMap) IsEmpty() bool {
	return fieldErrMap.items == nil || len(*fieldErrMap.items) == 0
}

// AddError adds error to the FieldsErrorMap
func (fieldErrMap *FieldsErrorMap) AddError(fieldName string, err error) {
	(*fieldErrMap.items)[fieldName] = err
}

// AddErrorf is the formatted version of AddError
func (fieldErrMap *FieldsErrorMap) AddErrorf(fieldName, formatString string, params ...interface{}) {
	(*fieldErrMap.items)[fieldName] = fmt.Errorf(formatString, params...)
}

// NilFields returns all the nil fields of the struct
func (fieldErrMap *FieldsErrorMap) NilFields() *[]string {
	panic("not implemented")
}

// Has returns true if all the passed fields are present in FieldsErrorMap
func (fieldErrMap *FieldsErrorMap) Has(key string) bool {
	err, isFound := (*fieldErrMap.items)[key]

	return isFound && err != nil
}

// HasAll returns true if all the passed fields are present in FieldsErrorMap
func (fieldErrMap *FieldsErrorMap) HasAll(keys ...string) bool {
	for _, key := range keys {
		err, isFound := (*fieldErrMap.items)[key]

		if !isFound || err == nil {
			// not found
			return false
		}
	}

	// all found.
	return true
}
