package reflectcast

import (
	"reflect"
	"unsafe"

	"gitlab.com/evatix-go/reflecthelper/internal/errmsg"
	"gitlab.com/evatix-go/reflecthelper/internal/fieldname"
)

// ToActual assigns each field in actual struct with the
// corresponding fields from the model struct
//
// Assuming model struct has Exported fields and actual struct has unexported fields
// with the same name and same type
//
// Contract violation:
//		we are assigning from model which should have only exported
// 		fields. But it turns out we've got an unexported field here.
// 		let's register error and ignore this field
func ToActual(model, actualPtr interface{}) (*FieldsErrorMap, error) {
	actualValue := reflect.ValueOf(actualPtr)

	if actualValue.Kind() != reflect.Ptr {
		return nil, errmsg.ExpectedButFound(reflect.Ptr, actualValue.Kind())
	}

	// get the underlying type, assuming *T, not **T or ***T...
	actualValue = actualValue.Elem()
	modelValue := reflect.ValueOf(model)

	// if user gives model as ptr, get the underlying type
	if modelValue.Kind() == reflect.Ptr {
		modelValue = modelValue.Elem()
	}

	if modelValue.Kind() != reflect.Struct {
		return nil, errmsg.ExpectedButFound(reflect.Struct, modelValue.Kind())
	}

	actualType := actualValue.Type()
	modelType := modelValue.Type()

	actualNumFields := actualValue.NumField()
	modelNumFields := modelType.NumField()

	// the string representations are for err messages
	modelTypeString := modelType.String()
	actualTypeString := actualType.String()

	modelFieldsNameToStructField := make(map[string]*reflect.StructField, modelNumFields)

	for i := 0; i < modelNumFields; i++ {
		field := modelType.Field(i)
		modelFieldsNameToStructField[field.Name] = &field
	}

	errMap := NewFieldsErrorMap()

	for i := 0; i < actualNumFields; i++ {
		// we are assigning to actual from model
		actualFieldValue := actualValue.Field(i)
		actualFieldName := actualType.Field(i).Name

		modelFieldName, err := fieldname.TranspileToPublic(actualFieldName)

		// field is empty
		if err != nil {
			continue
		}

		modelStructFieldPtr, hasField := modelFieldsNameToStructField[modelFieldName]

		// no matching field found
		if !hasField {
			err2 := errmsg.FieldIsNotPresentIn.Error(modelTypeString)
			errMap.AddError(actualFieldName, err2)

			continue
		}

		// contract violation:
		//		we are assigning from model which should have only exported
		//		fields. But it turns out we've got an unexported field here.
		//		let's register error and ignore this field
		if modelStructFieldPtr.PkgPath != "" {
			err2 := errmsg.FieldIsNotExportedIn.Error(modelTypeString)
			errMap.AddError(actualFieldName, err2)

			continue
		}

		// get the field
		modelField := modelValue.FieldByIndex(modelStructFieldPtr.Index)

		// this field is not found in
		// model but present in actual
		if !modelField.IsValid() {
			// TODO : this shouldn't happen,
			//  because we've checked this above with type
			//  we may remove this check
			err2 := errmsg.FieldIsNotPresentIn.Error(modelTypeString)
			errMap.AddError(actualFieldName, err2)

			continue
		}

		// value found in model, but value is nil
		if isNil(modelField) {
			err2 := errmsg.FieldIsNilIn.Error(modelTypeString)
			errMap.AddError(actualFieldName, err2)

			continue
		}

		// type mismatch, but field names are the same
		if actualFieldValue.Type() != modelField.Type() {
			err2 := errmsg.FieldTypeMismatch.Error(
				modelField.Type().String(), modelTypeString,
				actualFieldValue.Type().String(), actualTypeString)
			errMap.AddError(actualFieldName, err2)

			continue
		}

		reflect.NewAt(
			actualFieldValue.Type(),
			unsafe.Pointer(actualFieldValue.UnsafeAddr())).
			Elem().
			Set(modelField)
	}

	return errMap, nil
}
