package reflectcast

import (
	"reflect"
	"unsafe"

	"gitlab.com/evatix-go/reflecthelper/internal/errmsg"
	"gitlab.com/evatix-go/reflecthelper/internal/fieldname"
)

// ToModel assigns each field in model struct with the corresponding fields from the actual struct
//
// Assuming model struct has Exported fields and actual struct has unexported fields
// with the same name and same type
func ToModel(actualPtr, modelPtr interface{}) (*FieldsErrorMap, error) {
	modelValue := reflect.ValueOf(modelPtr)

	if modelValue.Kind() != reflect.Ptr {
		return nil, errmsg.ExpectedButFound(reflect.Ptr, modelValue.Kind())
	}

	actualValue := reflect.ValueOf(actualPtr)
	// src is not addressable, let's create a new copy of src.
	// this temporary version will be addressable and will let
	// us do the assignment
	if actualValue.Kind() != reflect.Ptr {
		actualValueCopyPtr := reflect.New(actualValue.Type())
		actualValueCopyPtr.Elem().Set(actualValue)
		actualValue = actualValueCopyPtr
	}

	// get the underlying type, assuming *T, not **T, or ***T ...
	modelValue = modelValue.Elem()
	actualValue = actualValue.Elem()

	actualType := actualValue.Type()
	modelType := modelValue.Type()

	modelNumFields := modelValue.NumField()
	actualNumFields := actualType.NumField()

	modelTypeString := modelType.String()
	actualTypeString := actualType.String()

	actualFieldsNameToStructField := make(
		map[string]*reflect.StructField,
		actualNumFields)

	for i := 0; i < actualNumFields; i++ {
		field := actualType.Field(i)
		actualFieldsNameToStructField[field.Name] = &field
	}

	errMap := NewFieldsErrorMap()

	for i := 0; i < modelNumFields; i++ {
		// we are assigning to model from actual
		modelField := modelValue.Field(i)
		modelFieldStruct := modelType.Field(i)
		modelFieldName := modelFieldStruct.Name

		// contract violation:
		// 		this field is unexported in Model
		if modelFieldStruct.PkgPath != "" {
			err := errmsg.FieldIsNotExportedIn.Error(modelTypeString)
			errMap.AddError(modelFieldName, err)

			continue
		}

		actualFieldName, err := fieldname.TranspileToPrivate(modelFieldName)

		// field name is empty
		if err != nil {
			continue
		}

		actualStructField, hasField := actualFieldsNameToStructField[actualFieldName]

		// this field is not found in actual struct but present in model struct
		if !hasField {
			err2 := errmsg.FieldIsNotPresentIn.Error(actualTypeString)
			errMap.AddError(modelFieldName, err2)

			continue
		}

		actualField := actualValue.FieldByIndex(actualStructField.Index)

		if !actualField.IsValid() {
			err2 := errmsg.FieldIsNotPresentIn.Error(actualTypeString)
			errMap.AddError(modelFieldName, err2)

			continue
		}

		if isNil(actualField) {
			err2 := errmsg.FieldIsNilIn.Error(actualType.String())
			errMap.AddError(modelFieldName, err2)

			continue
		}

		// type mismatch, but field names are same
		if modelField.Type() != actualField.Type() {
			err2 := errmsg.FieldTypeMismatch.Error(
				actualField.Type().String(), actualTypeString,
				modelField.Type().String(), modelTypeString)

			errMap.AddError(modelFieldName, err2)

			continue
		}

		readableSrc := reflect.NewAt(actualField.Type(),
			unsafe.Pointer(actualField.UnsafeAddr())).Elem()
		modelField.Set(readableSrc)
	}

	return errMap, nil
}
