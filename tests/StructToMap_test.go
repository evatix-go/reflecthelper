package tests

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	"gitlab.com/evatix-go/reflecthelper/reflectfields"
)

type args struct {
	structObj interface{}
}

type structToMapTestCaseWrapper struct {
	name     string
	args     args
	expected map[string]interface{}
	hasError bool
}

var structToMapTestCases = []structToMapTestCaseWrapper{
	{
		name: "Simple struct with one exported field",
		args: args{
			struct {
				i int
				S string
			}{},
		},
		expected: map[string]interface{}{
			"S": "",
		},
		hasError: false,
	},
}

func Test_StructToMap(t *testing.T) {
	for _, testCase := range structToMapTestCases {
		Convey(testCase.name, t, func() {
			// Act
			actual, err := reflectfields.GetPublicValuesToMap(testCase.args.structObj)

			// Assert
			if testCase.hasError {
				Convey("Error is not nil", func() {
					So(err, ShouldNotBeNil)
				})
			} else {
				Convey("Error is nil", func() {
					So(err, ShouldBeNil)
				})
			}

			Convey("Expected map equals actual map", func() {
				So(actual, ShouldResemble, testCase.expected)
			})
		})
	}
}
