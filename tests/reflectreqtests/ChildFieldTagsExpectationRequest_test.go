package reflectreqtests

import (
	"fmt"
	"testing"

	"github.com/smartystreets/goconvey/convey"
	"gitlab.com/evatix-go/reflecthelper/reflectreq"
)

func Test_ChildFieldTagsExpectationRequest(t *testing.T) {
	for index, testCase := range childFieldTagsTestCases {
		// Act
		response := reflectreq.
			ChildResponseOf.
			TagsString(&testCase.Request)

		// Assert
		convey.Convey(fmt.Sprintf("%v", index)+" : "+testCase.Header, t, func() {
			errMessage := response.VerifyErrorMessage()

			convey.So(errMessage, convey.ShouldBeEmpty)
		})
	}
}
