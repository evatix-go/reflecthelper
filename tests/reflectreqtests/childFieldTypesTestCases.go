package reflectreqtests

import (
	"reflect"

	"gitlab.com/evatix-go/reflecthelper/reflectreq"
)

var (
	childFieldTypesTestCases = []RequestResponseTestWrapper{
		{
			Header: "t2 has (\"A.C.D.d1\", \"A.C.D.d2\") and mismatch (\"A.C.D.d4\") and missing of (\"A.B.C\", \"A.C.D.d*\")!",
			Request: reflectreq.ChildFieldTypesExpectationRequest{
				RawInputStruct: &t2{},
				FieldsNameQueryRequestMap: map[string]reflect.Type{
					"A.B.C":    reflect.TypeOf("will not be found"),
					"A.C.D.d*": reflect.TypeOf("will not be found"),
					"A.C.D.d1": reflect.TypeOf(1),
					"A.C.D.d2": reflect.TypeOf(""),
					"A.C.D.d4": reflect.TypeOf("mismatch type"),
				},
			},
			ExpectedMismatch: map[string]reflect.Type{
				"A.C.D.d4": reflect.TypeOf([]string{}),
			},
			ExpectedMissingFieldQueries: []string{
				"A.B.C",
				"A.C.D.d*",
			},
		},
		{
			Header: "t1 all missing fields (\"A.B.C\", \"A.C.D.d*\", \"A.C.D.d1\", \"A.C.D.d2\", \"A.C.D.d4\")!",
			Request: reflectreq.ChildFieldTypesExpectationRequest{
				RawInputStruct: &t1{},
				FieldsNameQueryRequestMap: map[string]reflect.Type{
					"A.B.C":    reflect.TypeOf("will not be found"),
					"A.C.D.d*": reflect.TypeOf("will not be found"),
					"A.C.D.d1": reflect.TypeOf(1),
					"A.C.D.d2": reflect.TypeOf(""),
					"A.C.D.d4": reflect.TypeOf([]string{}),
				},
			},
			ExpectedMismatch: map[string]reflect.Type{},
			ExpectedMissingFieldQueries: []string{
				"A.B.C",
				"A.C.D.d*",
				"A.C.D.d1",
				"A.C.D.d2",
				"A.C.D.d4",
			},
		},
		{
			Header: "t2 none of the fields found from field quires.",
			Request: reflectreq.ChildFieldTypesExpectationRequest{
				RawInputStruct: &t1{},
				FieldsNameQueryRequestMap: map[string]reflect.Type{
					"A.B.C":     reflect.TypeOf("will not be found"),
					"A.C.D.d*":  reflect.TypeOf("will not be found"),
					"A.C.D.d1*": reflect.TypeOf(1),
					"A.C.D.d2*": reflect.TypeOf(""),
					"A.C.D.d4*": reflect.TypeOf([]string{}),
				},
			},
			ExpectedMismatch: map[string]reflect.Type{},
			ExpectedMissingFieldQueries: []string{
				"A.B.C",
				"A.C.D.d*",
				"A.C.D.d1*",
				"A.C.D.d2*",
				"A.C.D.d4*",
			},
		},
		{
			Header: "t2 all fields are found.",
			Request: reflectreq.ChildFieldTypesExpectationRequest{
				RawInputStruct: &t2{},
				FieldsNameQueryRequestMap: map[string]reflect.Type{
					"A.C.D.d1": reflect.TypeOf(1),
					"A.C.D.d2": reflect.TypeOf(""),
					"A.C.D.d4": reflect.TypeOf([]string{}),
				},
			},
		},
	}
)
