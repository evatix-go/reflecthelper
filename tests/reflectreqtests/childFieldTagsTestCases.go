package reflectreqtests

import (
	"gitlab.com/evatix-go/reflecthelper/reflectreq"
)

var (
	childFieldTagsTestCases = []RequestResponseTagTestWrapper{
		{
			Header: "t1 has (\"c.q\" : `json:\"blah\" xml:\"more_blah\"`) tags!",
			Request: reflectreq.ChildFieldTagsExpectationRequest{
				RawInputStruct: t1{},
				FieldsNameQueryRequestMap: map[string]string{
					"C.Q": `json:"blah" xml:"more_blah"`,
				},
			},
		},
	}
)
