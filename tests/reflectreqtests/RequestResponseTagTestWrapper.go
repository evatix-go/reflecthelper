package reflectreqtests

import "gitlab.com/evatix-go/reflecthelper/reflectreq"

type RequestResponseTagTestWrapper struct {
	Header  string
	Request reflectreq.ChildFieldTagsExpectationRequest
}
