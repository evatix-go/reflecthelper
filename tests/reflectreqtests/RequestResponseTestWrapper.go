package reflectreqtests

import (
	"reflect"
	"sort"

	"gitlab.com/evatix-go/reflecthelper/reflectreq"
)

type RequestResponseTestWrapper struct {
	Header                      string
	Request                     reflectreq.ChildFieldTypesExpectationRequest
	ExpectedMismatch            map[string]reflect.Type
	ExpectedMissingFieldQueries []string
}

func (it *RequestResponseTestWrapper) IsMismatchAndMissingFieldsOkay(
	response *reflectreq.ChildFieldTypesResponse,
) bool {
	return it.IsMissingFieldsOkay(response) &&
		it.IsMismatchOkay(response)
}

func (it *RequestResponseTestWrapper) IsMismatchOkay(
	response *reflectreq.ChildFieldTypesResponse,
) bool {
	responseMismatchLength := response.MismatchFieldsLength()

	if responseMismatchLength == 0 && len(it.ExpectedMismatch) == 0 {
		return true
	}

	if responseMismatchLength == 0 || len(it.ExpectedMismatch) == 0 {
		return false
	}

	if responseMismatchLength != len(it.ExpectedMismatch) {
		return false
	}

	for fieldName, mismatchFieldType := range response.MismatchFields {
		receivedMismatched, has := it.ExpectedMismatch[fieldName]

		if !has || receivedMismatched != mismatchFieldType.Actual {
			return false
		}
	}

	return true
}

func (it *RequestResponseTestWrapper) IsMissingFieldsOkay(
	response *reflectreq.ChildFieldTypesResponse,
) bool {
	responseMissingLength := response.MissingQueriesLength()

	if responseMissingLength == 0 && len(it.ExpectedMissingFieldQueries) == 0 {
		return true
	}

	if responseMissingLength == 0 || len(it.ExpectedMissingFieldQueries) == 0 {
		return false
	}

	if responseMissingLength != len(it.ExpectedMissingFieldQueries) {
		return false
	}

	sort.Strings(response.MissingQueries)
	sort.Strings(it.ExpectedMissingFieldQueries)

	for i, missingFieldName := range response.MissingQueries {
		if it.ExpectedMissingFieldQueries[i] != missingFieldName {
			return false
		}
	}

	return true
}
