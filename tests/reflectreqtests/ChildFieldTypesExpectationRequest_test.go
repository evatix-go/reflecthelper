package reflectreqtests

import (
	"fmt"
	"testing"

	"github.com/smartystreets/goconvey/convey"
	"gitlab.com/evatix-go/reflecthelper/reflectreq"
)

func Test_ChildFieldTypesExpectationRequest(t *testing.T) {
	for index, testCase := range childFieldTypesTestCases {
		// Act
		response := reflectreq.
			ChildResponseOf.
			Types(&testCase.Request)
		isOkay := testCase.IsMismatchAndMissingFieldsOkay(response)

		// Assert
		convey.Convey(fmt.Sprintf("%v", index)+" : "+testCase.Header, t, func() {
			if isOkay {
				return
			}

			// must have issues
			errMessage := response.VerifyErrorMessage()
			convey.So(errMessage, convey.ShouldBeEmpty)
		})
	}
}
