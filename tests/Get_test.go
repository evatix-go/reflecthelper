package tests

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	"gitlab.com/evatix-go/reflecthelper"
)

// TODO: Need to organize tests
func Test_Get_Int(t *testing.T) {
	// arrange
	var from = 17
	var to int

	// act
	err := reflecthelper.Get(nil, &from, &to)

	Convey("Get int", t, func() {
		Convey("Error is nil", func() {
			// assert
			So(err, ShouldBeNil)
		})

		Convey("Expected should be actual", func() {
			// assert
			So(to, ShouldEqual, from)
		})
	})
}

func TestGet_SameStruct(t *testing.T) {
	type T struct {
		a int
		b string
		c []string
		d *[]int
		e *string
		f struct {
			p int
			q map[string]int
		}
	}

	msg := "One None and a hundred thousands"

	original := T{
		a: 17,
		b: "Hello World",
		c: []string{"foo", "bar", "baz"},
		d: &[]int{1757, 1855, 1901},
		e: &msg,
		f: struct {
			p int
			q map[string]int
		}{
			p: 9001,
			q: map[string]int{
				"one":   1,
				"two":   2,
				"three": 3,
			},
		},
	}

	var cpy T

	err := reflecthelper.Get(nil, original, &cpy)

	Convey("Get from same struct", t, func() {
		Convey("Err should be nil", func() {
			So(err, ShouldBeNil)
		})
		Convey("Objects should be equal", func() {
			So(cpy, ShouldResemble, original)
		})
	})
}

func Test_Get_SameStructFieldName(t *testing.T) {
	type T1 struct {
		a string
		b []int
	}

	type T2 struct {
		a string
		b []int
	}

	original := T1{
		a: "Hello World",
		b: []int{1, 2, 3, 4},
	}

	var cpy T2

	err := reflecthelper.Get(nil, original, &cpy)

	Convey("Get from different struct, but same name and field", t, func() {
		Convey("Err should be nil", func() {
			So(err, ShouldBeNil)
		})
		Convey("Fields should be equal", func() {
			So(cpy.a, ShouldEqual, original.a)
			So(cpy.b, ShouldResemble, original.b)
		})
	})
}

func Test_Get_SameStructFieldNameButDifferentType(t *testing.T) {
	type T1 struct {
		a []int
		b string
	}

	type T2 struct {
		a string
		b []int
	}

	original := T1{
		a: []int{1, 2, 3, 4},
		b: "Hello World",
	}

	var cpy T2

	err := reflecthelper.Get(nil, original, &cpy)

	Convey("Get from different struct, but same name filed but different type", t, func() {
		Convey("Err should be nil", func() {
			So(err, ShouldBeNil)
		})
		Convey("Fields should be equal", func() {
			So(cpy.a, ShouldEqual, "")
			So(cpy.b, ShouldEqual, nil)
		})
	})
}

func Test_Get_PointerInStruct(t *testing.T) {
	type T1 struct {
		a *string
	}

	type T2 struct {
		a *string
	}

	hello := "Hello"
	original := T1{
		a: &hello,
	}

	var cpy T2

	err := reflecthelper.Get(nil, original, &cpy)

	Convey("Get from different struct, but same name and field", t, func() {
		Convey("Err should be nil", func() {
			So(err, ShouldBeNil)
		})
		Convey("Fields should be equal", func() {
			So(cpy.a, ShouldEqual, original.a)
			So(cpy.a, ShouldResemble, original.a)
		})
	})
}

func Test_Get_FieldTypeMismatchPanic(t *testing.T) {
	type T1 struct {
		a string
	}

	type T2 struct {
		a int
	}

	original := T1{
		a: "Hello world",
	}

	var cpy T2

	opt := reflecthelper.NewReflectGetOptions(false, true)

	Convey("Get from different struct, but same name but different field type", t, func() {
		Convey("Panic option set, should panic", func() {
			So(
				func() {
					_ = reflecthelper.Get(opt, original, &cpy)
				},
				ShouldPanicWith,
				"can not convert from string to int",
			)
		})
	})
}

func Test_Get_FieldTypeMismatchErr(t *testing.T) {
	type T1 struct {
		a string
	}

	type T2 struct {
		a int
	}

	original := T1{
		a: "Hello world",
	}

	var cpy T2

	err := reflecthelper.GetSafe(original, &cpy)

	Convey("Get from different struct, but same name but different field type", t, func() {
		Convey("Should return error", func() {
			So(err, ShouldNotBeNil)
			So(err, ShouldBeError, "can not convert from string to int")
		})
	})
}
