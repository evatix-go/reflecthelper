package reflectfieldtests

import (
	"reflect"
	"testing"

	"github.com/smartystreets/goconvey/convey"
	"gitlab.com/evatix-go/reflecthelper/reflectfield"
)

func Test_StructWithPtrField(t *testing.T) {
	type MyStruct struct {
		p *int
	}

	es := MyStruct{new(int)}
	fm := reflectfield.New.
		StructFieldsMap.
		Any(Level1, &es)

	convey.Convey("Empty struct with no fields", t, func() {
		convey.Convey("Should be invalid", func() {
			convey.So(fm.IsValid, convey.ShouldBeTrue)
			// TODO: need to think about this
			// because reduction makes it struct
			convey.So(fm.Kind, convey.ShouldEqual, reflect.Struct)
		})

		convey.Convey("Methods should work properly", func() {
			convey.So(fm.IsEmpty(), convey.ShouldBeFalse)
			convey.So(fm.HasItems(), convey.ShouldBeTrue)
			convey.So(fm.HasField("p"), convey.ShouldBeTrue)
			convey.So(fm.HasFields("p"), convey.ShouldBeTrue)
			convey.So(fm.HasFieldsByPublicNames("P"), convey.ShouldBeTrue)
			convey.So(fm.HasFieldsByPrivateNames("p"), convey.ShouldBeTrue)
			convey.So(fm.GetWrapper("p"), convey.ShouldNotBeNil)
			convey.So(fm.AllNullFieldsNames(), convey.ShouldHaveLength, 0)
		})
	})
}
