package reflectfieldtests

import (
	"reflect"
	"testing"

	"github.com/smartystreets/goconvey/convey"
	"gitlab.com/evatix-go/reflecthelper/reflectfield"
)

func Test_PrimitiveTypeNotValid(t *testing.T) {
	es := "1"

	fm := reflectfield.New.StructFieldsMap.Any(Level1, es)

	convey.Convey("Primitive type with no fields will be invalid", t, func() {
		convey.Convey("Should be invalid", func() {
			convey.So(fm.IsValid, convey.ShouldBeFalse)
			convey.So(fm.Kind, convey.ShouldEqual, reflect.String)
			convey.So(fm.IsEmpty(), convey.ShouldBeTrue)
			convey.So(fm.HasItems(), convey.ShouldBeFalse)
			convey.So(fm.HasField("any"), convey.ShouldBeFalse)
			convey.So(fm.HasFields("one", "two", "three"), convey.ShouldBeFalse)
			convey.So(fm.HasFieldsByPublicNames("PublicName1", "PublicName2"), convey.ShouldBeFalse)
			convey.So(fm.HasFieldsByPrivateNames("privateName1", "privateName2"), convey.ShouldBeFalse)
			convey.So(fm.GetWrapper("fieldName"), convey.ShouldBeNil)
			convey.So(fm.AllNullFieldsNames(), convey.ShouldHaveLength, 0)
		})
	})
}

func Test_EmptyStructValid(t *testing.T) {
	type EmptyStruct struct{}
	es := EmptyStruct{}

	fm := reflectfield.New.StructFieldsMap.Any(Level1, es)

	convey.Convey("Empty struct with no fields", t, func() {
		convey.Convey("Should be invalid", func() {
			convey.So(fm.IsValid, convey.ShouldBeFalse)
			convey.So(fm.Kind, convey.ShouldEqual, reflect.Struct)
			convey.So(fm.IsEmpty(), convey.ShouldBeTrue)
			convey.So(fm.HasItems(), convey.ShouldBeFalse)
			convey.So(fm.HasField("any"), convey.ShouldBeFalse)
			convey.So(fm.HasFields("one", "two", "three"), convey.ShouldBeFalse)
			convey.So(fm.HasFieldsByPublicNames("PublicName1", "PublicName2"), convey.ShouldBeFalse)
			convey.So(fm.HasFieldsByPrivateNames("privateName1", "privateName2"), convey.ShouldBeFalse)
			convey.So(fm.GetWrapper("fieldName"), convey.ShouldBeNil)
			convey.So(fm.AllNullFieldsNames(), convey.ShouldHaveLength, 0)
		})
	})
}
