package reflectfieldtests

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	"gitlab.com/evatix-go/reflecthelper/reflectfield"
)

func Test_ComplexStruct(t *testing.T) {
	type p struct {
		ptr *int64
	}

	type q struct {
		items []int
	}

	type r struct {
		x int32
		y byte
		z []string
	}

	type MyStruct struct {
		iSlice    []int
		iSlicePtr *[]int
		mp        map[string]int
		Mp        *map[string]string
	}

	ms := &MyStruct{
		iSlice:    []int{1, 2, 3},
		iSlicePtr: &[]int{42, 43, 44},
		mp: map[string]int{
			"one": 1,
			"two": 2,
		},
		Mp: &map[string]string{
			"name": "jon",
			"addr": "noland",
		},
	}

	fm := reflectfield.New.StructFieldsMap.Any(LevelAll, ms)

	Convey("Complex struct with multiple fields", t, func() {
		Convey("Should initialize all fields", func() {
			So(fm.WrappersMap, ShouldNotBeNil)
			So(fm.FieldsMap, ShouldNotBeNil)
			So(fm.Wrappers, ShouldNotBeNil)
			So(fm.FieldNamesMap, ShouldNotBeNil)
			So(fm.TranspiledPrivateNames, ShouldNotBeNil)
			So(fm.TranspiledPublicNames, ShouldNotBeNil)
		})

		fieldMapExpectedLen := 4

		Convey("Map and slices should be of len 4", func() {
			So(fm.Length, ShouldEqual, fieldMapExpectedLen)
			So(len(fm.WrappersMap), ShouldEqual, fieldMapExpectedLen)
			So(len(fm.FieldsMap), ShouldEqual, fieldMapExpectedLen)
			So(len(fm.Wrappers), ShouldEqual, fieldMapExpectedLen)
			So(len(fm.FieldNames), ShouldEqual, fieldMapExpectedLen)
			So(len(fm.FieldNamesMap), ShouldEqual, fieldMapExpectedLen)
			So(len(fm.TranspiledPublicNames), ShouldEqual, 3)  // because of key mp -> Mp
			So(len(fm.TranspiledPrivateNames), ShouldEqual, 3) // because of key Mp -> mp
		})

		filedNames := []string{"iSlice", "iSlicePtr", "mp", "Mp"}
		Convey("Maps should contain key names...", func() {
			for _, name := range filedNames {
				So(fm.FieldsMap, ShouldContainKey, name)
				So(fm.WrappersMap, ShouldContainKey, name)
				So(fm.FieldNamesMap, ShouldContainKey, name)
				So(fm.FieldNames, ShouldContain, name)
			}
		})
	})
}
