package reflectfieldtests

import (
	"testing"

	"github.com/smartystreets/goconvey/convey"
	"gitlab.com/evatix-go/reflecthelper/reflectfield"
)

func Test_StructWithOneElement(t *testing.T) {
	type MyStruct struct {
		s string
	}

	ms := MyStruct{"Hello World"}

	fm := reflectfield.New.StructFieldsMap.Any(Level1, ms)

	convey.Convey("Struct with one element", t, func() {
		convey.Convey("Should initialize all fields", func() {
			convey.So(fm.WrappersMap, convey.ShouldNotBeNil)
			convey.So(fm.FieldsMap, convey.ShouldNotBeNil)
			convey.So(fm.Wrappers, convey.ShouldNotBeNil)
			convey.So(fm.FieldNamesMap, convey.ShouldNotBeNil)
			convey.So(fm.TranspiledPrivateNames, convey.ShouldNotBeNil)
			convey.So(fm.TranspiledPublicNames, convey.ShouldNotBeNil)
			convey.So(fm.Length, convey.ShouldBeGreaterThanOrEqualTo, 0)
		})

		convey.Convey("Length should be 1", func() {
			convey.So(fm.Length, convey.ShouldEqual, 1)
		})

		convey.Convey("Map and slices should be of len 1", func() {
			convey.So(fm.Length, convey.ShouldEqual, 1)
			convey.So(len(fm.WrappersMap), convey.ShouldEqual, 1)
			convey.So(len(fm.FieldsMap), convey.ShouldEqual, 1)
			convey.So(len(fm.Wrappers), convey.ShouldEqual, 1)
			convey.So(len(fm.FieldNames), convey.ShouldEqual, 1)
			convey.So(len(fm.FieldNamesMap), convey.ShouldEqual, 1)
			convey.So(len(fm.TranspiledPublicNames), convey.ShouldEqual, 1)
			convey.So(len(fm.TranspiledPrivateNames), convey.ShouldEqual, 1)
		})

		convey.Convey("Map should contain key name 's'", func() {
			convey.So(fm.FieldsMap, convey.ShouldContainKey, "s")
			convey.So(fm.WrappersMap, convey.ShouldContainKey, "s")
			convey.So(fm.FieldNamesMap, convey.ShouldContainKey, "s")
		})
	})
}
