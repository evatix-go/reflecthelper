package reflectfieldtests

import (
	"reflect"
	"testing"

	"github.com/smartystreets/goconvey/convey"
	"gitlab.com/evatix-go/reflecthelper/reflectfield"
)

func Test_StructWithNilPtrField(t *testing.T) {
	type MyStruct struct {
		p *int
	}

	es := MyStruct{nil}

	fm := reflectfield.New.StructFieldsMap.Any(LevelAll, &es)

	convey.Convey("Empty struct with 1 nil field", t, func() {
		convey.Convey("Should be invalid", func() {
			convey.So(fm.IsValid, convey.ShouldBeTrue)
			convey.So(fm.Kind, convey.ShouldEqual, reflect.Struct)
			convey.So(fm.HasAnyNull, convey.ShouldBeTrue)
		})

		convey.Convey("Methods should work properly", func() {
			convey.So(fm.AllNullFieldsNames(), convey.ShouldHaveLength, 1)
		})
	})
}
