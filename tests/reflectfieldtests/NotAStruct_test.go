package reflectfieldtests

import (
	"testing"

	"github.com/smartystreets/goconvey/convey"
	"gitlab.com/evatix-go/reflecthelper/reflectfield"
)

// Test_NotAStruct
//
// TODO: need to organize
// It's quite hard to organize properly,
// we're checking different things each time
func Test_NotAStruct(t *testing.T) {
	value := 75

	fm := reflectfield.New.StructFieldsMap.Any(
		LevelAll,
		value)

	convey.Convey("Not a struct", t, func() {
		convey.Convey("Should not be valid", func() {
			convey.So(fm.IsValid, convey.ShouldBeFalse)
		})

		convey.Convey("Should not be nil", func() {
			convey.So(fm.HasAnyNull, convey.ShouldBeFalse)
		})

		convey.Convey("Length should be 0", func() {
			convey.So(fm.Length, convey.ShouldEqual, 0)
		})
	})
}
