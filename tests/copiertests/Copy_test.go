package copiertests

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	"gitlab.com/evatix-go/reflecthelper/reflectcopier"
)

// TODO: Sample copy cases, need to organize tests
func Test_Copy_Primitive(t *testing.T) {
	Convey("Test copying primitive", t, func() {
		// arrange
		fromInt := 17
		var toInt int

		// act
		errCopyInt := reflectcopier.Copy(&toInt, fromInt, 1)

		Convey("Err should be nil for copying int", func() {
			So(errCopyInt, ShouldBeNil)
		})

		Convey("Int value should be equal", func() {
			// assert
			So(toInt, ShouldEqual, fromInt)
		})

		// arrange
		fromFloat := 3.15159265
		var toFloat float64

		// act
		errCopyFloat := reflectcopier.Copy(&toFloat, fromFloat, 1)

		Convey("Err should be nil for copying float", func() {
			So(errCopyFloat, ShouldBeNil)
		})

		Convey("Float value should be equal", func() {
			// assert
			So(toFloat, ShouldEqual, fromFloat)
		})
	})
}

func Test_Copy_Level(t *testing.T) {
	type T struct {
		X int
		S string
		A []int
	}

	Convey("Test reflectcopier.Copy levels", t, func() {
		// arrange
		fromObject := T{17, "Hello World", []int{1, 2, 3}}

		Convey("Copying with level 1", func() {
			toObject := T{}

			// act
			errCopyObjWithLevel1 := reflectcopier.Copy(&toObject, fromObject, 1)

			Convey("Err copy object should be nil", func() {
				So(errCopyObjWithLevel1, ShouldBeNil)
			})

			Convey("fields should not be copied", func() {
				So(fromObject, ShouldNotResemble, toObject)
			})
		})

		Convey("Copying with level 2", func() {
			toObject := T{}

			errCopyObjWithLevel2 := reflectcopier.Copy(&toObject, fromObject, 2)

			Convey("Err copy object should be nil", func() {
				So(errCopyObjWithLevel2, ShouldBeNil)
			})

			Convey("fields should be created", func() {
				Convey("primitive should be copied", func() {
					So(toObject.X, ShouldEqual, fromObject.X)
					So(toObject.S, ShouldEqual, fromObject.S)
				})

				Convey("slice should be created with same size and zero values", func() {
					So(toObject.A, ShouldResemble, []int{0, 0, 0})
				})
			})
		})

		Convey("Copying with level -1", func() {
			toObject := T{}

			// act
			errCopyObjWithAllLevel := reflectcopier.Copy(&toObject, fromObject, -1)

			Convey("Err should be nil", func() {
				So(errCopyObjWithAllLevel, ShouldBeNil)
			})

			Convey("Everything should be copied", func() {
				So(fromObject, ShouldResemble, toObject)
			})
		})
	})
}
