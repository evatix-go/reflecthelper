package benchmark

import (
	"math/rand"
	"testing"

	"gitlab.com/evatix-go/reflecthelper/reflectcopier"
)

// sample benchmark

func BenchmarkCopyReflect(b *testing.B) {
	s := make([]int, 1000)

	for i := range s {
		s[i] = rand.Int()
	}

	for i := 0; i < b.N; i++ {
		var d []int
		reflectcopier.Copy(&d, s, 2)
	}
}

func BenchmarkCopyNative(b *testing.B) {
	s := make([]int, 1000)

	for i := range s {
		s[i] = rand.Int()
	}

	for i := 0; i < b.N; i++ {
		var d []int
		d = make([]int, len(s))

		for j := range d {
			d[j] = s[j]
		}
	}
}
