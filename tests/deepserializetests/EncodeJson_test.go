package deepserializetests

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	"gitlab.com/evatix-go/reflecthelper/deepserialize"
)

// TODO: organize the tests
func Test_EncodeJson(t *testing.T) {
	type T struct {
		a int
		b string
		c []string
		d *[]int
		e *string
		f struct {
			p int
			q map[string]int
		}
	}

	// arrange
	msg := "One None and a hundred thousands"
	original := T{
		a: 17,
		b: "Hello World",
		c: []string{"foo", "bar", "baz"},
		d: &[]int{1757, 1855, 1901},
		e: &msg,
		f: struct {
			p int
			q map[string]int
		}{
			p: 9001,
			q: map[string]int{
				"one":   1,
				"two":   2,
				"three": 3,
			},
		},
	}

	Convey("EncodeJSON arbitrary object", t, func() {
		// act: serialize
		jsonBytes, encodeErr := deepserialize.EncodeJSON(original)

		Convey("Encoding should throw no error", func() {
			// assert
			So(encodeErr.IsEmpty(), ShouldBeTrue)
		})

		// act: deserialize
		var current T
		decodeErr := deepserialize.DecodeJSON(jsonBytes, &current)

		Convey("Decoding should throw no error", func() {
			// assert
			So(decodeErr.IsEmpty(), ShouldBeTrue)
		})

		Convey("Decoding the encoded object should be same as original object", func() {
			// assert
			So(current, ShouldResemble, original)
		})
	})
}
