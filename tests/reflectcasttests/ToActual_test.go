package reflectcasttests

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	"gitlab.com/evatix-go/reflecthelper/reflectcast"
)

func getActualStruct(i int) interface{} {
	switch i {
	case 1:
		return &u1{}
	case 2:
		return &u2{}
	}
	return nil
}

type toActualTestCaseWrapper struct {
	name      string
	structNum int
	arg       interface{}
	expected  interface{}
	wantErr   bool
}

var toActualTestCases = []toActualTestCaseWrapper{
	{
		name:      "Simple case 1",
		structNum: 1,
		arg:       E1{17, "Hello World"},
		expected:  &u1{17, "Hello World"},
		wantErr:   false,
	},
	{
		name:      "Simple case 2",
		structNum: 2,
		arg:       E2{Ip: new(int), Arr: [3]int{1, 2, 3}},
		expected:  &u2{ip: new(int), arr: [3]int{1, 2, 3}},
		wantErr:   false,
	},
}

func TestToActual(t *testing.T) {
	for _, testCase := range toActualTestCases {
		Convey(testCase.name, t, func() {
			// act
			actual := getActualStruct(testCase.structNum)

			fieldErrMap, err := reflectcast.ToActual(testCase.arg, actual)

			// assert
			if testCase.wantErr {
				Convey("Error is not nil", func() {
					So(err, ShouldNotBeNil)
					So(fieldErrMap, ShouldBeNil)
				})
			} else {
				Convey("Error is nil", func() {
					So(err, ShouldBeNil)
					So(fieldErrMap, ShouldNotBeNil)
				})
			}

			Convey("Expected map equals actual map", func() {
				So(actual, ShouldResemble, testCase.expected)
			})
		})
	}
}
