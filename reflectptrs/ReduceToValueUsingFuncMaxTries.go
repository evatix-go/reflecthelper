package reflectptrs

import (
	"reflect"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
)

// ReduceToValueUsingFuncMaxTries
//
// isKindSkipFunc return true when want to skip for reduction
// Examples can be
//  - For ptr reduce return true for isKindSkipFunc -> kind == reflect.Ptr
func ReduceToValueUsingFuncMaxTries(
	input interface{},
	isKindSkipFunc func(kind reflect.Kind) bool,
	maxTries int,
) *ReducedResult {
	if maxTries == consts.Invalid {
		return ReduceToValue(input)
	}

	initialValue := reflect.ValueOf(input)

	return ReflectValReduceToValUsingFuncMaxTries(
		&initialValue,
		isKindSkipFunc,
		maxTries)
}
