package reflectptrs

import (
	"reflect"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
)

func ReduceToValueUsingMaxTries(
	input interface{},
	maxTries int,
) *ReducedResult {
	if maxTries == consts.Invalid {
		return ReduceToValue(input)
	}

	initialValue := reflect.ValueOf(input)

	return ReflectValReduceToValUsingMaxTries(
		&initialValue,
		maxTries)
}
