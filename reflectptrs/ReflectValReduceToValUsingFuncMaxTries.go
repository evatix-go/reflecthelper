package reflectptrs

import (
	"reflect"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
	"gitlab.com/evatix-go/reflecthelper/internal/defaulterr"
)

// isKindSkipFunc return true when want to skip for reduction
// Examples can be
//  - For ptr reduce return true for isKindSkipFunc -> kind == reflect.Ptr
func ReflectValReduceToValUsingFuncMaxTries(
	initialValue *reflect.Value,
	isKindSkipFunc func(kind reflect.Kind) bool,
	maxTries int,
) *ReducedResult {
	if maxTries == consts.Invalid {
		return ReflectValReduceToValUsingFunc(
			initialValue,
			isKindSkipFunc)
	}

	structValue := *initialValue
	initialKind := structValue.Kind()
	structValueKind := initialKind

	for isKindSkipFunc(structValueKind) {
		structValue = structValue.Elem()
		// mutating dangerous code
		structValueKind = structValue.Kind()

		maxTries--
		if maxTries <= 0 {
			break
		}
	}

	if structValueKind == reflect.Invalid {
		return &ReducedResult{
			InputRawReflectValue: initialValue,
			ReducedReflectValue:  &structValue,
			InitialKind:          initialKind,
			ReducedKind:          structValueKind,
			Error:                defaulterr.InvalidTypeCannotReduce,
			IsReducedSuccess:     false,
		}
	}

	return &ReducedResult{
		InputRawReflectValue: initialValue,
		ReducedReflectValue:  &structValue,
		InitialKind:          initialKind,
		ReducedKind:          structValueKind,
		Error:                nil,
		IsReducedSuccess:     false,
	}
}
