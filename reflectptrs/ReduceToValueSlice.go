package reflectptrs

import (
	"reflect"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
)

// ReduceToValueSlice
//
// input can be ptr, interface or or slice or struct to struct.
//
// Best input to be struct. Returns struct, if it was not struct then returns error
func ReduceToValueSlice(input interface{}) *ReducedResult {
	initialValue := reflect.ValueOf(input)

	return ReflectValReduceToValUsingFunc(
		&initialValue,
		isPtrInterfaceSliceSkipFunc)
}

// ReduceToValueSliceUsingMaxTries
//
// input can be ptr, interface or slice or struct to struct.
//
// Best input to be struct. Returns struct, if it was not struct then returns error
//
// maxTries == -1 return and visits all.
func ReduceToValueSliceUsingMaxTries(input interface{}, maxTries int) *ReducedResult {
	initialValue := reflect.ValueOf(input)

	if maxTries == consts.Invalid {
		return ReflectValReduceToValUsingFunc(
			&initialValue,
			isPtrInterfaceSliceSkipFunc)
	}

	return ReflectValReduceToValUsingFuncMaxTries(
		&initialValue,
		isPtrInterfaceSliceSkipFunc,
		maxTries)
}
