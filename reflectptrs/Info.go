package reflectptrs

import "reflect"

// Info
//
// Pointer :
//  (*[]CastingType)(unsafe.Pointer(reflectValueOfAny.Pointer())) will return the casted type
//  (*[]CastingType)(unsafe.Pointer(Info.Pointer)) will return the casted type
type Info struct {
	IsPointer    bool
	ReflectValue reflect.Value
	// (*[]CastingType)(unsafe.Pointer(reflectValueOfAny.Pointer())) will return the casted type
	Pointer   *uintptr
	Kind      reflect.Kind
	IsInvalid bool
}

// GetIndirectOrPointerValue
//
// returns nil or reflect.Indirect(receiver.ReflectValue)
//
// if not ptr or invalid then returns nil
func (it *Info) GetIndirectOrPointerValue() *reflect.Value {
	if it.IsInvalid || !it.IsPointer {
		return nil
	}

	reflectValueOfPtr := reflect.Indirect(it.ReflectValue)

	return &reflectValueOfPtr
}

func (it *Info) GetIndirectType() reflect.Type {
	if it.IsInvalid || !it.IsPointer {
		return nil
	}

	return reflect.Indirect(it.ReflectValue).Type()
}

func (it *Info) GetReducedResultUsingFunc(
	isKindSkipFunc func(kind reflect.Kind) bool,
) *ReducedResult {
	if it.IsInvalid {
		return nil
	}

	return ReflectValReduceToValUsingFunc(
		&it.ReflectValue,
		isKindSkipFunc)
}

func (it *Info) GetReducedResultUsingFuncMaxTries(
	maxTries int,
	isKindSkipFunc func(kind reflect.Kind) bool,
) *ReducedResult {
	if it.IsInvalid {
		return nil
	}

	return ReflectValReduceToValUsingFuncMaxTries(
		&it.ReflectValue,
		isKindSkipFunc,
		maxTries)
}

func (it *Info) GetReducedResult() *ReducedResult {
	if it.IsInvalid {
		return nil
	}

	return ReflectValReduceToVal(
		&it.ReflectValue)
}

func (it *Info) GetReducedResultUsingMaxTry(maxTries int) *ReducedResult {
	if it.IsInvalid {
		return nil
	}

	return ReflectValReduceToValUsingMaxTries(
		&it.ReflectValue,
		maxTries)
}
