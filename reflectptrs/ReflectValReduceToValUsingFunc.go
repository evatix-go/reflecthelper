package reflectptrs

import (
	"reflect"

	"gitlab.com/evatix-go/reflecthelper/internal/defaulterr"
)

// isKindSkipFunc return true when want to skip for reduction
// Examples can be
//  - For ptr reduce return true for isKindSkipFunc -> kind == reflect.Ptr
func ReflectValReduceToValUsingFunc(
	initialValue *reflect.Value,
	isKindSkipFunc func(kind reflect.Kind) bool,
) *ReducedResult {
	structValue := *initialValue
	initialKind := structValue.Kind()
	structValueKind := initialKind

	for isKindSkipFunc(structValueKind) {
		structValue = structValue.Elem()
		// mutating dangerous code
		structValueKind = structValue.Kind()
	}

	if structValueKind == reflect.Invalid {
		return &ReducedResult{
			InputRawReflectValue: initialValue,
			ReducedReflectValue:  &structValue,
			InitialKind:          initialKind,
			ReducedKind:          structValueKind,
			Error:                defaulterr.InvalidTypeCannotReduce,
			IsReducedSuccess:     false,
		}
	}

	return &ReducedResult{
		InputRawReflectValue: initialValue,
		ReducedReflectValue:  &structValue,
		InitialKind:          initialKind,
		ReducedKind:          structValueKind,
		Error:                nil,
		IsReducedSuccess:     false,
	}
}
