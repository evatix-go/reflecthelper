package reflectptrs

import "reflect"

func GetPointerInfo(any interface{}) Info {
	reflectValueOfAny := reflect.ValueOf(any)
	kind := reflectValueOfAny.Kind()
	isPtr := kind == reflect.Ptr
	isInvalid := kind == reflect.Invalid

	if isPtr {
		ptr := reflectValueOfAny.Pointer()
		return Info{
			IsPointer:    isPtr,
			ReflectValue: reflectValueOfAny,
			Pointer:      &ptr,
			Kind:         kind,
			IsInvalid:    isInvalid,
		}
	}

	return Info{
		IsPointer:    isPtr,
		ReflectValue: reflectValueOfAny,
		Pointer:      nil,
		Kind:         kind,
		IsInvalid:    isInvalid,
	}
}
