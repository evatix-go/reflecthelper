package reflectptrs

import (
	"reflect"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
	"gitlab.com/evatix-go/reflecthelper/internal/defaulterr"
)

func ReflectValReduceToValUsingMaxTries(
	initialValue *reflect.Value,
	maxTries int,
) *ReducedResult {
	if maxTries == consts.Invalid {
		return ReflectValReduceToVal(initialValue)
	}

	structValue := *initialValue
	initialKind := structValue.Kind()
	structValueKind := initialKind

	for structValueKind == reflect.Ptr || structValueKind == reflect.Interface {
		structValue = structValue.Elem()
		// mutating dangerous code
		structValueKind = structValue.Kind()
		maxTries--

		if maxTries <= 0 {
			break
		}
	}

	if structValueKind == reflect.Invalid {
		return &ReducedResult{
			InputRawReflectValue: initialValue,
			ReducedReflectValue:  &structValue,
			InitialKind:          initialKind,
			ReducedKind:          structValueKind,
			Error:                defaulterr.InvalidTypeCannotReduce,
			IsReducedSuccess:     false,
		}
	}

	return &ReducedResult{
		InputRawReflectValue: initialValue,
		ReducedReflectValue:  &structValue,
		InitialKind:          initialKind,
		ReducedKind:          structValueKind,
		Error:                nil,
		IsReducedSuccess:     false,
	}
}
