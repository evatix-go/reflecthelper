package reflecthelper

func GetStrict(from, to interface{}, skipFieldNames ...string) {
	opts := NewReflectGetOptions(
		true, // is panic on field not present
		true, // is panic on field type mismatch
		skipFieldNames...,
	)

	g := newGetter(opts, false)

	// not handling error, because it'll panic immediately
	_ = g.Get(from, to)
}
