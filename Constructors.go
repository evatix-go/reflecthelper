package reflecthelper

func NewReflectGetOptions(
	isPanicOnFieldNotPresent, isPanicOnFieldTypeMismatch bool,
	skippingFieldNames ...string,
) *Options {
	skippingFieldNamesMap := make(
		map[string]bool,
		len(skippingFieldNames),
	)

	for _, name := range skippingFieldNames {
		skippingFieldNamesMap[name] = true
	}

	return &Options{
		SkippingFieldsNames:        skippingFieldNamesMap,
		IsPanicOnFieldNotPresent:   isPanicOnFieldNotPresent,
		IsPanicOnFieldTypeMismatch: isPanicOnFieldTypeMismatch,
	}
}

func NewDefaultOptions() *Options {
	return &Options{
		SkippingFieldsNames:        nil,
		IsPanicOnFieldNotPresent:   false,
		IsPanicOnFieldTypeMismatch: false,
	}
}
