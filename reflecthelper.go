package reflecthelper

import (
	"fmt"
	"reflect"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
)

func GetTypeName(any interface{}) string {
	return fmt.Sprintf(consts.SprintFTypeFormat, any)
}

// Calls GetElementTypeMaxTry with maxTries 4
//
// Examples:
//  - Cite : https://stackoverflow.com/a/39468423
//  - https://play.golang.org/p/hRQmtclUqkx
func GetElementType(any interface{}) reflect.Type {
	if any == nil {
		return nil
	}

	return GetElementTypeMaxTry(any, consts.N4)
}

// Get actual element type by reducing type by ptr, slice, interface
//
// Examples:
//  - Cite : https://stackoverflow.com/a/39468423
//  - https://play.golang.org/p/hRQmtclUqkx
func GetElementTypeMaxTry(
	any interface{},
	maxTries int,
) reflect.Type {
	if any == nil {
		return nil
	}

	for t := reflect.TypeOf(any); maxTries >= 0; maxTries-- {
		switch t.Kind() {
		case reflect.Ptr, reflect.Slice:
			t = t.Elem()
		default:
			return t
		}
	}

	return nil
}

// Get actual element type by reducing type by ptr, slice, interface
//
// Examples:
//  - Cite : https://stackoverflow.com/a/39468423
//  - https://play.golang.org/p/UswVNOUcjZi
func GetElementTypesMaxTry(
	any interface{}, maxTries int,
) (
	finalType reflect.Type,
	visitedTypes *[]reflect.Type,
) {
	if any == nil {
		return nil, nil
	}

	visitedTypesList := make([]reflect.Type, 0, maxTries)

	for finalType = reflect.TypeOf(any); maxTries >= 0; maxTries-- {
		visitedTypesList = append(visitedTypesList, finalType)

		switch finalType.Kind() {
		case reflect.Ptr, reflect.Slice, reflect.Interface:
			finalType = finalType.Elem()
		default:
			return finalType, &visitedTypesList
		}
	}

	return finalType, &visitedTypesList
}
