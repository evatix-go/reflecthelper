package isreflect

import (
	"fmt"
	"reflect"
	"unsafe"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
	"gitlab.com/evatix-go/reflecthelper/reflecttype"
)

func TypeSame(type1 reflect.Type, type2 reflect.Type) bool {
	return type1 == type2
}

func Type(any interface{}, typeName string) bool {
	return fmt.Sprintf(consts.SprintFTypeFormat, any) == typeName
}

// BytesOrBytesPointer
//
// Examples :
//  https://play.golang.org/p/9XUt9Jf11WG | https://play.golang.org/p/oMAxxSzAP7F
func BytesOrBytesPointer(any interface{}) (isBytes bool, bytesPtr *[]byte) {
	reflectValueOfAny := reflect.ValueOf(any)
	isPtr := reflectValueOfAny.Kind() == reflect.Ptr

	if isPtr {
		indirectType := reflect.Indirect(reflectValueOfAny).Type()

		if indirectType == reflecttype.Uint8sType && indirectType.Kind() == reflect.Slice {
			bytes := (*[]byte)(unsafe.Pointer(reflectValueOfAny.Pointer()))

			return true, bytes
		}
	}

	typeOf := reflect.TypeOf(any)
	if typeOf == reflecttype.Uint8sType && typeOf.Kind() == reflect.Slice {
		bytes := reflectValueOfAny.Bytes()

		if bytes != nil {
			return true, &bytes
		}
	}

	return false, nil
}

// StringOrStringPointer
//
// Examples :
//  https://play.golang.org/p/9XUt9Jf11WG | https://play.golang.org/p/oMAxxSzAP7F
func StringOrStringPointer(any interface{}) (isString bool, stringPointer *string) {
	reflectValueOfAny := reflect.ValueOf(any)
	isPtr := reflectValueOfAny.Kind() == reflect.Ptr

	if isPtr {
		indirectType := reflect.Indirect(reflectValueOfAny).Type()

		if indirectType == reflecttype.StringType {
			strPtr := (*string)(unsafe.Pointer(reflectValueOfAny.Pointer()))

			return true, strPtr
		}
	}

	typeOf := reflect.TypeOf(any)

	if typeOf == reflecttype.StringType {
		str, _ := any.(string)

		return true, &str
	}

	return false, nil
}

// StringsOrStringsPointer
//
// Examples :
//  https://play.golang.org/p/9XUt9Jf11WG | https://play.golang.org/p/oMAxxSzAP7F
func StringsOrStringsPointer(any interface{}) (isStrings bool, stringsPtr *[]string) {
	reflectValueOfAny := reflect.ValueOf(any)
	isPtr := reflectValueOfAny.Kind() == reflect.Ptr

	if isPtr {
		indirectType := reflect.Indirect(reflectValueOfAny).Type()

		if indirectType == reflecttype.StringsType && indirectType.Kind() == reflect.Slice {
			strings := (*[]string)(unsafe.Pointer(reflectValueOfAny.Pointer()))

			return true, strings
		}
	}

	typeOf := reflect.TypeOf(any)

	if typeOf == reflecttype.StringsType && typeOf.Kind() == reflect.Slice {
		strings, _ := any.([]string)
		isStrings = true

		if strings != nil {
			return isStrings, &strings
		}

		return isStrings, nil
	}

	return false, nil
}

// IntegersOrIntegersPointer
//
// Examples :
//  https://play.golang.org/p/9XUt9Jf11WG | https://play.golang.org/p/oMAxxSzAP7F
func IntegersOrIntegersPointer(any interface{}) (isIntegers bool, integersResults *[]int) {
	reflectValueOfAny := reflect.ValueOf(any)
	isPtr := reflectValueOfAny.Kind() == reflect.Ptr

	if isPtr {
		indirectType := reflect.Indirect(reflectValueOfAny).Type()

		if indirectType == reflecttype.IntegersType && indirectType.Kind() == reflect.Slice {
			integers := (*[]int)(unsafe.Pointer(reflectValueOfAny.Pointer()))

			return true, integers
		}
	}

	typeOf := reflect.TypeOf(any)

	if typeOf == reflecttype.IntegersType && typeOf.Kind() == reflect.Slice {
		integers, _ := any.([]int)
		isIntegers = true
		if integers != nil {
			return isIntegers, &integers
		}

		return isIntegers, nil
	}

	return false, nil
}

// IntegerOrIntegerPointer
//
// Examples :
//  https://play.golang.org/p/9XUt9Jf11WG | https://play.golang.org/p/oMAxxSzAP7F
func IntegerOrIntegerPointer(any interface{}) (isInteger bool, intPtr *int) {
	reflectValueOfAny := reflect.ValueOf(any)
	isPtr := reflectValueOfAny.Kind() == reflect.Ptr

	if isPtr {
		indirectType := reflect.Indirect(reflectValueOfAny).Type()

		if indirectType == reflecttype.IntegerType {
			integer := (*int)(unsafe.Pointer(reflectValueOfAny.Pointer()))

			return true, integer
		}
	}

	typeOf := reflect.TypeOf(any)

	if typeOf == reflecttype.IntegerType {
		integer, _ := any.(int)

		return true, &integer
	}

	return false, nil
}

// Integer
//
// Examples :
//  https://play.golang.org/p/9XUt9Jf11WG | https://play.golang.org/p/oMAxxSzAP7F
func Integer(any interface{}) (isInteger bool, intValue int) {
	isIntegerResult, intPtr := IntegerOrIntegerPointer(any)

	if intPtr == nil {
		return isIntegerResult, 0
	}

	return isIntegerResult, *intPtr
}

func Byte(any interface{}) (isByte bool, result byte) {
	reflectValueOfAny := reflect.ValueOf(any)
	isPtr := reflectValueOfAny.Kind() == reflect.Ptr

	if isPtr {
		indirectType := reflect.Indirect(reflectValueOfAny).Type()

		if indirectType == reflecttype.IntegerType {
			currentByte := *(*byte)(unsafe.Pointer(reflectValueOfAny.Pointer()))

			return true, currentByte
		}
	}

	typeOf := reflect.TypeOf(any)

	if typeOf == reflecttype.IntegerType {
		currentByte, _ := any.(byte)

		return true, currentByte
	}

	return false, 0
}

func Boolean(any interface{}) (isBool bool, isResult bool) {
	reflectValueOfAny := reflect.ValueOf(any)
	isPtr := reflectValueOfAny.Kind() == reflect.Ptr

	if isPtr {
		indirectType := reflect.Indirect(reflectValueOfAny).Type()

		if indirectType == reflecttype.BooleanType {
			currentBoolPtr := (*bool)(unsafe.Pointer(reflectValueOfAny.Pointer()))

			if currentBoolPtr != nil {
				isResult = *currentBoolPtr
			}

			// pointer is nil so found = true, result = false.
			return true, isResult
		}
	}

	typeOf := reflect.TypeOf(any)

	if typeOf == reflecttype.BooleanType {
		currentBool, _ := any.(bool)

		return true, currentBool
	}

	return false, false
}

func BooleanPointer(any interface{}) (isBool bool, isResult *bool) {
	reflectValueOfAny := reflect.ValueOf(any)
	isPtr := reflectValueOfAny.Kind() == reflect.Ptr

	if isPtr {
		indirectType := reflect.Indirect(reflectValueOfAny).Type()

		if indirectType == reflecttype.BooleanType {
			return true, (*bool)(unsafe.Pointer(reflectValueOfAny.Pointer()))
		}
	}

	typeOf := reflect.TypeOf(any)

	if typeOf == reflecttype.BooleanType {
		currentBool, _ := any.(bool)

		return true, &currentBool
	}

	return false, nil
}

func Float64sOrFloat64sPointer(any interface{}) (isFloat64 bool, floats *[]float64) {
	reflectValueOfAny := reflect.ValueOf(any)
	isPtr := reflectValueOfAny.Kind() == reflect.Ptr

	if isPtr {
		indirectType := reflect.Indirect(reflectValueOfAny).Type()

		if indirectType == reflecttype.Float64sType {
			return true, (*[]float64)(unsafe.Pointer(reflectValueOfAny.Pointer()))
		}
	}

	typeOf := reflect.TypeOf(any)

	if typeOf == reflecttype.Float64sType {
		float64s, isConvertSuccess := any.([]float64)

		if float64s != nil {
			return isConvertSuccess, &float64s
		}

		return isConvertSuccess, nil
	}

	return false, nil
}

// String
//
// Examples :
//  https://play.golang.org/p/9XUt9Jf11WG | https://play.golang.org/p/oMAxxSzAP7F
func String(any interface{}) (isString bool, str string) {
	isStr, strPtr := StringOrStringPointer(any)

	if strPtr != nil {
		return isStr, *strPtr
	}

	return isStr, ""
}
