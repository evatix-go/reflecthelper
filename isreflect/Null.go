package isreflect

import "reflect"

func Null(rv reflect.Value) bool {
	// chan, func, interface, map, pointer, or slice
	// are nullable
	switch rv.Kind() {
	case
		reflect.Chan,
		reflect.Func,
		reflect.Interface,
		reflect.Map,
		reflect.Ptr,
		reflect.Slice:
		return rv.IsNil()
	default:
		return false
	}
}
