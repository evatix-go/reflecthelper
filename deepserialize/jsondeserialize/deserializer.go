package jsondeserialize

import (
	"reflect"
	"unsafe"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
	"gitlab.com/evatix-go/reflecthelper/internal/defaulterr"
	"gitlab.com/evatix-go/reflecthelper/internal/errmsg"
	"gitlab.com/evatix-go/reflecthelper/reflecterrs"
	"gitlab.com/evatix-go/reflecthelper/reflecttype"
)

// what is the basic idea?
// we have json object as map/[]interface/interface, and we have user object
// we want to iterate through user object and create/assign as we go along

// This is how golang represents JSON data
// bool, for JSON booleans
// float64, for JSON numbers
// string, for JSON strings
// []interface{}, for JSON arrays
// map[string]interface{}, for JSON objects
// nil for JSON null

// OUR Mappings
// primitive types 	-> map{float/int/bool/string, ThisType}
// slice/array 		-> map{[]interface{}, ThisType}
// map  			-> map{[]interface{Key, Value}, ThisType}
// struct 			-> map{FieldName, ThisType}

// createAndSetValueAt creates a zero value of type `destinationValue.Type()`
// at the address of destinationValue and assign `value` to it
//
// Returns : error
//  - If the types are different and not convertible
//
// Conditions (for panic):
//  - value must be readable
func createAndSetValueAt(destinationValue, srcValue reflect.Value) error {
	destinationType := destinationValue.Type()
	srcType := srcValue.Type()

	if srcType != destinationType {
		return errmsg.ExpectedButFoundString(destinationType.String(), srcType.String())
	}

	reflect.NewAt(destinationType, unsafe.Pointer(destinationValue.UnsafeAddr())).
		Elem().
		Set(srcValue)

	// all set
	return nil
}

func setUnexportedField(field reflect.Value, interfaceValue interface{}) error {
	value := reflect.ValueOf(interfaceValue)

	fieldType := field.Type()
	valueType := value.Type()

	if valueType != fieldType {
		// value type is not convertible, abort
		if !valueType.ConvertibleTo(fieldType) {
			return errmsg.ExpectedButFound(field.Kind(), value.Kind())
		}

		value = value.Convert(fieldType)
	}

	reflect.NewAt(fieldType, unsafe.Pointer(field.UnsafeAddr())).
		Elem().
		Set(value)

	// all set
	return nil
}

// map is stored as a slice of [{Key1, Value1}, {Key2, Value2}, ...]
func decodeMap(jsonSlice *[]interface{}, userMap reflect.Value) *reflecterrs.Wrapper {
	errDecodeMap := reflecterrs.New()

	if jsonSlice == nil || *jsonSlice == nil {
		errDecodeMap.Add(defaulterr.GenericMapIsNil)

		return errDecodeMap
	}

	if userMap.Kind() != reflect.Map {
		errDecodeMap.Add(errmsg.ExpectedButFound(reflect.Map, userMap.Kind()))

		return errDecodeMap
	}

	sliceLen := len(*jsonSlice)

	// map is empty, don't create a map altogether
	if sliceLen == 0 {
		return errDecodeMap
	}

	newMap := reflect.MakeMapWithSize(userMap.Type(), sliceLen)
	mapType := newMap.Type()

	mapKeyType := mapType.Key()
	mapValueType := mapType.Elem()

	for i := 0; i < sliceLen; i++ {
		// Map Key and Value are stored as {"Key": SomeKey,"Value": SomeValue}
		keyValuePair, isOfTypeMap := (*jsonSlice)[i].(map[string]interface{})

		if !isOfTypeMap {
			continue
		}

		// check if "Key" is present
		key, isKeyPresent := keyValuePair[consts.Key]

		if !isKeyPresent {
			continue
		}

		// check if "Value" is present
		value, isValuePresent := keyValuePair[consts.Value]

		if !isValuePresent {
			continue
		}

		// each key is also a map of {"Value": Anything, "ThisType": ValueType}
		keyMap, isKeyOfTypeMap := key.(map[string]interface{})

		if !isKeyOfTypeMap {
			continue
		}

		// each value is also a map of {"Value": Anything, "ThisType": ValueType}
		valueMap, isValueOfTypeMap := value.(map[string]interface{})

		if !isValueOfTypeMap {
			continue
		}

		// create new key and value
		rvKey := reflect.New(mapKeyType).Elem()
		rvValue := reflect.New(mapValueType).Elem()

		errDecodeAnyForKey := decodeAny(keyMap, rvKey)

		if !errDecodeAnyForKey.IsEmpty() {
			errDecodeMap.AppendInto(errDecodeAnyForKey)

			continue
		}

		errDecodeAnyForValue := decodeAny(valueMap, rvValue)

		if !errDecodeAnyForValue.IsEmpty() {
			errDecodeMap.AppendInto(errDecodeAnyForValue)

			continue
		}

		newMap.SetMapIndex(rvKey, rvValue)
	}

	// set the map directly
	reflect.NewAt(userMap.Type(), unsafe.Pointer(userMap.UnsafeAddr())).
		Elem().
		Set(newMap)

	// all set
	return errDecodeMap
}

func decodeStruct(jsonMap map[string]interface{}, userStruct reflect.Value) *reflecterrs.Wrapper {
	errDecodeStruct := reflecterrs.New()

	if userStruct.Kind() != reflect.Struct {
		errDecodeStruct.Add(errmsg.ExpectedButFound(reflect.Struct, userStruct.Kind()))

		return errDecodeStruct
	}

	structType := userStruct.Type()
	structNumFields := structType.NumField()

	for i := 0; i < structNumFields; i++ {
		field := userStruct.Field(i)
		fieldName := structType.Field(i).Name

		value, isFieldNamePresent := jsonMap[fieldName]

		// fieldName is not present in map
		if !isFieldNamePresent {
			continue
		}

		// each value is also a map of map{"Value": SomeValue, "ThisType": SomeType}
		valueMap, isValueOfTypeMap := value.(map[string]interface{})

		// value is not a map, but expecting a map here
		if !isValueOfTypeMap {
			continue
		}

		errDecodeAnyForField := decodeAny(valueMap, field)
		errDecodeStruct.AppendInto(errDecodeAnyForField)
	}

	// all decoded
	return errDecodeStruct
}

func decodeSlice(jsonSlice *[]interface{}, userSlice reflect.Value) *reflecterrs.Wrapper {
	errDecodeSlice := reflecterrs.New()

	if jsonSlice == nil || *jsonSlice == nil {
		errDecodeSlice.Add(defaulterr.GenericSliceIsNil)

		return errDecodeSlice
	}

	if userSlice.Kind() != reflect.Slice {
		errDecodeSlice.Add(errmsg.ExpectedButFound(reflect.Slice, userSlice.Kind()))

		return errDecodeSlice
	}

	sliceLen := len(*jsonSlice)
	sliceCap := cap(*jsonSlice)

	newSlice := reflect.MakeSlice(userSlice.Type(), sliceLen, sliceCap)
	err := createAndSetValueAt(userSlice, newSlice)

	if err != nil {
		errDecodeSlice.Add(err)

		return errDecodeSlice
	}

	for i := 0; i < sliceLen; i++ {
		jsonMap, isOfTypeMap := (*jsonSlice)[i].(map[string]interface{})

		// each slice value is expected as map{"Value": SomeValue, "ThisType": SomeType}
		if !isOfTypeMap {
			continue
		}

		errDecodeAny := decodeAny(jsonMap, userSlice.Index(i))
		errDecodeSlice.AppendInto(errDecodeAny)
	}

	// all value set
	return errDecodeSlice
}

// Everything is wrapped into a map of {
//		Value:
// 		ThisType:
// }
func decodeAny(jsonMap map[string]interface{}, rv reflect.Value) *reflecterrs.Wrapper {
	errDecodeAny := reflecterrs.New()

	thisTypeInterface, hasTypeInfo := jsonMap[consts.ThisType]

	// no type information
	if !hasTypeInfo {
		errDecodeAny.Add(defaulterr.NoTypeInfo)

		return errDecodeAny
	}

	// JSON numbers are stored as float64
	thisTypeValue, isTypeNumber := thisTypeInterface.(float64)

	if !isTypeNumber {
		errDecodeAny.Add(defaulterr.InvalidType)

		return errDecodeAny
	}

	thisType := reflecttype.ToIdentifier(thisTypeValue)

	if !reflecttype.IsTypeValid(thisType) {
		errDecodeAny.Add(defaulterr.InvalidType)

		return errDecodeAny
	}

	switch kind := rv.Kind(); kind {
	case reflect.Slice:
		if !reflecttype.IsSame(thisType, reflect.Slice) {
			errDecodeAny.Add(errmsg.ExpectedButFoundString(reflect.Slice.String(), thisType.String()))

			return errDecodeAny
		}

		jsonSliceInterface, hasResultSlice := jsonMap[consts.ResultSlice]

		// expected key "ResultSlice" in map
		if !hasResultSlice {
			errDecodeAny.Add(defaulterr.InvalidSlice)

			return errDecodeAny
		}

		jsonSlice, isConvertibleToInterfaceSlice := jsonSliceInterface.([]interface{})

		// slice is a slice of interface
		if !isConvertibleToInterfaceSlice {
			errDecodeAny.Add(defaulterr.InvalidSlice)

			return errDecodeAny
		}

		errDecodeSlice := decodeSlice(&jsonSlice, rv)
		errDecodeAny.AppendInto(errDecodeSlice)

		return errDecodeAny
	case reflect.Struct:
		if !reflecttype.IsSame(thisType, reflect.Struct) {
			errDecodeAny.Add(errmsg.ExpectedButFoundString(reflect.Struct.String(), thisType.String()))

			return errDecodeAny
		}

		jsonStructInterface, hasResultMap := jsonMap[consts.ResultMap]

		if !hasResultMap {
			errDecodeAny.Add(defaulterr.InvalidStruct)

			return errDecodeAny
		}

		jsonStruct, isConvertibleToMap := jsonStructInterface.(map[string]interface{})

		if !isConvertibleToMap {
			errDecodeAny.Add(defaulterr.InvalidStruct)

			return errDecodeAny
		}

		errDecodeStruct := decodeStruct(jsonStruct, rv)
		errDecodeAny.AppendInto(errDecodeStruct)

		return errDecodeAny
	case reflect.Map:
		if !reflecttype.IsSame(thisType, reflect.Map) {
			errDecodeAny.Add(errmsg.ExpectedButFoundString(reflect.Map.String(), thisType.String()))

			return errDecodeAny
		}

		jsonMapInterface, hasResultMapEntrySlice := jsonMap[consts.ResultKeyValuePairSlice]

		if !hasResultMapEntrySlice {
			errDecodeAny.Add(defaulterr.InvalidMap)

			return errDecodeAny
		}

		jsonMap, isConvertibleToInterfaceSlice := jsonMapInterface.([]interface{})

		if !isConvertibleToInterfaceSlice {
			errDecodeAny.Add(defaulterr.InvalidMap)

			return errDecodeAny
		}

		errDecodeMap := decodeMap(&jsonMap, rv)
		errDecodeAny.AppendInto(errDecodeMap)

		return errDecodeAny
	case reflect.Ptr:
		underlyingType := rv.Type().Elem()
		underlyingTypePtr := reflect.New(underlyingType)

		err := createAndSetValueAt(rv, underlyingTypePtr)

		if err != nil {
			errDecodeAny.Add(err)

			return errDecodeAny
		}

		errDecodeAnyRecursive := decodeAny(jsonMap, rv.Elem())
		errDecodeAny.AppendInto(errDecodeAnyRecursive)

		return errDecodeAny
	// TODO: add support for Array and Interface
	case reflect.Array:
	case reflect.Interface:
	case reflect.Invalid:
		errDecodeAny.Add(defaulterr.InvalidType)

		return errDecodeAny
	default:
		value, hasValue := jsonMap[consts.Value]

		if !hasValue {
			errDecodeAny.Add(errmsg.ExpectedButFoundString("primitive", thisType.String()))

			return errDecodeAny
		}

		errSetUnexportedField := setUnexportedField(rv, value)
		errDecodeAny.Add(errSetUnexportedField)

		return errDecodeAny
	}

	errDecodeAny.Add(errmsg.ExpectedButFoundString(rv.Kind().String(), thisType.String()))

	return errDecodeAny
}

// Decode decodes encoded value into realObj
func Decode(actualObj, encodedValue interface{}) *reflecterrs.Wrapper {
	errDecode := reflecterrs.New()

	actualReflectValue := reflect.ValueOf(actualObj)

	if actualReflectValue.Kind() != reflect.Ptr {
		errDecode.Add(errmsg.ExpectedButFound(reflect.Ptr, actualReflectValue.Kind()))

		return errDecode
	}

	if actualReflectValue.Kind() == reflect.Ptr {
		actualReflectValue = actualReflectValue.Elem()
	}

	encodedMap, isConvertibleToMap := encodedValue.(map[string]interface{})

	if !isConvertibleToMap {
		errDecode.Add(defaulterr.InvalidMap)

		return errDecode
	}

	return decodeAny(encodedMap, actualReflectValue)
}
