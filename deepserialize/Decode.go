package deepserialize

import (
	"gitlab.com/evatix-go/reflecthelper/deepserialize/serializer"
	"gitlab.com/evatix-go/reflecthelper/reflecterrs"
)

// Decode decodes generic type data to real object
func Decode(actualObj, encodedValue interface{}) *reflecterrs.Wrapper {
	return serializer.Decode(actualObj, encodedValue)
}
