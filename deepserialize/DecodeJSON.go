package deepserialize

import (
	"encoding/json"

	"gitlab.com/evatix-go/reflecthelper/deepserialize/jsondeserialize"
	"gitlab.com/evatix-go/reflecthelper/reflecterrs"
)

// DecodeJSON decodes raw data to object
//
// The raw data must be the marshalled JSON version of generic types
func DecodeJSON(data []byte, actualObj interface{}) *reflecterrs.Wrapper {
	var any interface{}
	err := json.Unmarshal(data, &any)

	errDecodeJSON := reflecterrs.New()
	if err != nil {
		errDecodeJSON.Add(err)

		return errDecodeJSON
	}

	return jsondeserialize.Decode(actualObj, any)
}
