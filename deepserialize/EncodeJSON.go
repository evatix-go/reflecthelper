package deepserialize

import (
	"encoding/json"

	"gitlab.com/evatix-go/reflecthelper/reflecterrs"
)

// EncodeJSON encodes any object to generic types and then
// it encode them to JSON
func EncodeJSON(anyObj interface{}) ([]byte, *reflecterrs.Wrapper) {
	serializedData, errWrapper := Encode(anyObj)

	bytesData, err := json.Marshal(serializedData)
	errWrapper.Add(err)

	return bytesData, errWrapper
}
