// Package experimental serializer, try to make sense things

package serializer

import (
	"reflect"

	"gitlab.com/evatix-go/reflecthelper/internal/defaulterr"
	"gitlab.com/evatix-go/reflecthelper/internal/errmsg"
	"gitlab.com/evatix-go/reflecthelper/isreflect"
	"gitlab.com/evatix-go/reflecthelper/reflecterrs"
	"gitlab.com/evatix-go/reflecthelper/reflecttype"
)

// will decide later whether should we keep this
func encodePrimitive(v reflect.Value) (*reflecttype.GenericPrimitive, error) {
	switch v.Kind() {
	case reflect.Bool:
		genericPrimitive := &reflecttype.GenericPrimitive{
			Value:          v.Bool(),
			ReflectionType: reflect.Bool.String(),
		}

		return genericPrimitive, nil

	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		genericPrimitive := &reflecttype.GenericPrimitive{
			Value:          v.Int(),
			ReflectionType: reflect.Int.String(),
		}

		return genericPrimitive, nil

	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		genericPrimitive := &reflecttype.GenericPrimitive{
			Value:          v.Uint(),
			ReflectionType: reflect.Uint.String(),
		}

		return genericPrimitive, nil

	case reflect.Float32, reflect.Float64:
		genericPrimitive := &reflecttype.GenericPrimitive{
			Value:          v.Float(),
			ReflectionType: reflect.Float32.String(),
		}

		return genericPrimitive, nil

	case reflect.String:
		genericPrimitive := &reflecttype.GenericPrimitive{
			Value:          v.String(),
			ReflectionType: reflect.String.String(),
		}

		return genericPrimitive, nil

	default:
		return nil, errmsg.ExpectedPrimitiveButFound(v.Kind())
	}
}

func encodeAny(rv reflect.Value) (*reflecttype.Arbitrary, *reflecterrs.Wrapper) {
	errWrapper := reflecterrs.New()

	switch kind := rv.Kind(); kind {
	case reflect.Invalid:
		errWrapper.Add(defaulterr.EncodingInvalidType)

		return nil, errWrapper

	case reflect.Array:
		// fallthrough works for encode, but won't work in decode methods
		// creating array is different from creating slice
	case reflect.Slice:
		encodedSlice, errSlice := encodeSlice(rv)

		if !errSlice.IsEmpty() {
			errWrapper.AppendInto(errSlice)

			return nil, errWrapper
		}

		return reflecttype.NewArbitrarySlice(encodedSlice), errWrapper
	case reflect.Struct:
		encodedStruct, errStruct := encodeStruct(rv)

		if !errStruct.IsEmpty() {
			errWrapper.AppendInto(errStruct)

			return nil, errWrapper
		}

		return reflecttype.NewArbitraryStruct(encodedStruct), errWrapper
	case reflect.Map:
		encodedMap, errMap := encodeMap(rv)

		if !errMap.IsEmpty() {
			errWrapper.AppendInto(errMap)

			return nil, errWrapper
		}

		return reflecttype.NewArbitraryMap(encodedMap), errWrapper
	case reflect.Ptr:
		return encodeAny(rv.Elem())

	case reflect.Interface:
		return encodeAny(rv.Elem())
	default:
		if isreflect.Primitive(kind) {
			primitiveType, err := encodePrimitive(rv)

			if err != nil {
				errWrapper.Add(err)

				return nil, errWrapper
			}

			return reflecttype.NewArbitraryPrimitive(primitiveType), errWrapper
		}

		errWrapper.Add(errmsg.ExpectedPrimitiveButFound(kind))

		return nil, errWrapper
	}

	errWrapper.Add(defaulterr.UnexpectedValueInEncoder)

	return nil, errWrapper
}

func encodeMap(mapValue reflect.Value) (*reflecttype.GenericMap, *reflecterrs.Wrapper) {
	encodeMapErr := reflecterrs.New()

	resultEntrySlice := make([]*reflecttype.KeyValuePair, mapValue.Len())
	mapIter := mapValue.MapRange()

	i := 0

	for mapIter.Next() {
		key, encodeAnyErrForKey := encodeAny(mapIter.Key())

		if !encodeAnyErrForKey.IsEmpty() {
			encodeMapErr.AppendInto(encodeAnyErrForKey)

			continue
		}

		value, encodeAnyErrForValue := encodeAny(mapIter.Value())

		if !encodeAnyErrForValue.IsEmpty() {
			encodeMapErr.AppendInto(encodeAnyErrForKey)

			continue
		}

		resultEntrySlice[i] = &reflecttype.KeyValuePair{
			Key:   key,
			Value: value,
		}

		i++
	}

	genericMap := &reflecttype.GenericMap{
		ResultKeyValuePairSlice: &resultEntrySlice,
		ReflectionType:          mapValue.Kind().String(),
	}

	return genericMap, encodeMapErr
}

func encodeSlice(sliceValue reflect.Value) (*reflecttype.GenericSlice, *reflecterrs.Wrapper) {
	encodeSliceErr := reflecterrs.New()

	if sliceValue.Kind() != reflect.Slice {
		encodeSliceErr.Add(errmsg.ExpectedButFound(reflect.Slice, sliceValue.Kind()))

		return nil, encodeSliceErr
	}

	sliceLen := sliceValue.Len()
	sliceCap := sliceValue.Cap()

	resultSlice := make([]*reflecttype.Arbitrary, sliceLen, sliceCap)

	for i := 0; i < sliceLen; i++ {
		value := sliceValue.Index(i)
		any, errEncodeAnyForValue := encodeAny(value)

		if !errEncodeAnyForValue.IsEmpty() {
			encodeSliceErr.AppendInto(errEncodeAnyForValue)

			continue
		}

		resultSlice[i] = any
	}

	genericSlice := &reflecttype.GenericSlice{
		ResultSlice:    &resultSlice,
		ReflectionType: sliceValue.Kind().String(),
	}

	return genericSlice, encodeSliceErr
}

func encodeStruct(structValue reflect.Value) (*reflecttype.GenericStruct, *reflecterrs.Wrapper) {
	encodeStructErr := reflecterrs.New()

	structType := structValue.Type()
	structNumFields := structType.NumField()

	resultMap := make(map[string]*reflecttype.Arbitrary, structNumFields)

	for i := 0; i < structNumFields; i++ {
		value := structValue.Field(i)
		fieldName := structType.Field(i).Name

		encodedValue, errEncodeAnyForValue := encodeAny(value)

		if !errEncodeAnyForValue.IsEmpty() {
			encodeStructErr.AppendInto(errEncodeAnyForValue)

			continue
		}

		resultMap[fieldName] = encodedValue
	}

	genericStruct := &reflecttype.GenericStruct{
		ResultMap:      &resultMap,
		ReflectionType: structType.Kind().String(),
	}

	return genericStruct, encodeStructErr
}

// Encode returns generic serialized data
func Encode(anyObj interface{}) (*reflecttype.Arbitrary, *reflecterrs.Wrapper) {
	rv := reflect.ValueOf(anyObj)
	encodedValue, errWrapper := encodeAny(rv)

	return encodedValue, errWrapper
}
