package serializer

import (
	"reflect"
	"unsafe"

	"gitlab.com/evatix-go/reflecthelper/internal/defaulterr"
	"gitlab.com/evatix-go/reflecthelper/internal/errmsg"
	"gitlab.com/evatix-go/reflecthelper/reflecterrs"
	"gitlab.com/evatix-go/reflecthelper/reflecttype"
)

func setUnexportedField(field reflect.Value, interfaceValue interface{}) error {
	value := reflect.ValueOf(interfaceValue)

	fieldType := field.Type()
	valueType := value.Type()

	if valueType != fieldType {
		// value type is not convertible, abort
		if !valueType.ConvertibleTo(fieldType) {
			return errmsg.ExpectedButFound(field.Kind(), value.Kind())
		}

		value = value.Convert(fieldType)
	}

	reflect.NewAt(fieldType, unsafe.Pointer(field.UnsafeAddr())).
		Elem().
		Set(value)

	// all set
	return nil
}

func decodeMap(genericMap *reflecttype.GenericMap, rv reflect.Value) *reflecterrs.Wrapper {
	errDecodeMap := reflecterrs.New()

	if genericMap == nil ||
		genericMap.ResultKeyValuePairSlice == nil ||
		len(*genericMap.ResultKeyValuePairSlice) == 0 {
		errDecodeMap.Add(defaulterr.GenericMapIsNil)

		return errDecodeMap
	}

	resultMapEntrySlice := genericMap.ResultKeyValuePairSlice
	sliceLen := len(*resultMapEntrySlice)

	newMap := reflect.MakeMap(rv.Type())
	mapKeyType := newMap.Type().Key()
	mapValueType := newMap.Type().Elem()

	for i := 0; i < sliceLen; i++ {
		key := (*resultMapEntrySlice)[i].Key
		value := (*resultMapEntrySlice)[i].Value

		if key == nil {
			continue
		}

		rvKey := reflect.New(mapKeyType)
		rvValue := reflect.New(mapValueType)

		errDecodeAnyForKey := decodeAny(key, rvKey.Elem())

		if !errDecodeAnyForKey.IsEmpty() {
			errDecodeMap.AppendInto(errDecodeAnyForKey)

			continue
		}

		errDecodeAnyForValue := decodeAny(value, rvValue.Elem())

		if !errDecodeAnyForValue.IsEmpty() {
			errDecodeMap.AppendInto(errDecodeAnyForValue)

			continue
		}

		newMap.SetMapIndex(rvKey.Elem(), rvValue.Elem())
	}

	// set directly
	reflect.NewAt(rv.Type(), unsafe.Pointer(rv.UnsafeAddr())).
		Elem().
		Set(newMap)

	// all set
	return errDecodeMap
}

func decodeStruct(genericStruct *reflecttype.GenericStruct, rv reflect.Value) *reflecterrs.Wrapper {
	errDecodeStruct := reflecterrs.New()

	if genericStruct == nil ||
		genericStruct.ResultMap == nil ||
		len(*genericStruct.ResultMap) == 0 {
		errDecodeStruct.Add(defaulterr.GenericStructIsNil)

		return errDecodeStruct
	}

	resultsMap := genericStruct.ResultMap
	structType := rv.Type()
	structNumFields := structType.NumField()

	for i := 0; i < structNumFields; i++ {
		field := rv.Field(i)
		fieldName := structType.Field(i).Name

		value, hasField := (*resultsMap)[fieldName]

		if !hasField {
			continue
		}

		errDecodeAnyForField := decodeAny(value, field)
		errDecodeStruct.AppendInto(errDecodeAnyForField)
	}

	// all decoded
	return errDecodeStruct
}

func decodeSlice(genericSlice *reflecttype.GenericSlice, rv reflect.Value) *reflecterrs.Wrapper {
	errDecodeSlice := reflecterrs.New()

	if genericSlice == nil ||
		genericSlice.ResultSlice == nil ||
		len(*genericSlice.ResultSlice) == 0 {
		errDecodeSlice.Add(errmsg.GenericValueIsNilOrHasNoElement(reflect.Slice))

		return errDecodeSlice
	}

	resultSlice := genericSlice.ResultSlice
	sliceLen := 0

	valueLen := rv.Len()
	resultSliceLen := len(*resultSlice)

	// taking the minimum of two, if they differ in sizes
	if resultSliceLen < valueLen {
		sliceLen = resultSliceLen
	} else {
		sliceLen = valueLen
	}

	for i := 0; i < sliceLen; i++ {
		errDecodeAny := decodeAny((*resultSlice)[i], rv.Index(i))
		errDecodeSlice.AppendInto(errDecodeAny)
	}

	return errDecodeSlice
}

// any could be:
// - interface
// - primitive
// - ptr
// - *GenericStruct
// - *GenericSlice
// - *Map
// - *GenericPrimitive
func decodeAny(any *reflecttype.Arbitrary, rv reflect.Value) *reflecterrs.Wrapper {
	errDecodeAny := reflecterrs.New()

	switch kind := rv.Kind(); kind {
	case reflect.Slice:
		genericSlice, err := any.Slice()

		if err != nil {
			errDecodeAny.Add(errmsg.ExpectedButFound(reflect.Slice, reflect.TypeOf(any).Kind()))

			return errDecodeAny
		}

		if genericSlice == nil ||
			genericSlice.ResultSlice == nil ||
			len(*genericSlice.ResultSlice) == 0 {
			errDecodeAny.Add(errmsg.GenericValueIsNilOrHasNoElement(reflect.Slice))

			return errDecodeAny
		}

		sliceLen := len(*genericSlice.ResultSlice)
		sliceCap := cap(*genericSlice.ResultSlice)

		newSlice := reflect.MakeSlice(rv.Type(), sliceLen, sliceCap)

		err = setUnexportedField(rv, newSlice.Interface())

		if err != nil {
			errDecodeAny.Add(err)

			return errDecodeAny
		}

		errDecodeSlice := decodeSlice(genericSlice, rv)
		errDecodeAny.AppendInto(errDecodeSlice)

		return errDecodeAny
	case reflect.Struct:
		genericStruct, err := any.Struct()

		if err != nil {
			errDecodeAny.Add(errmsg.ExpectedButFound(reflect.Struct, reflect.TypeOf(any).Kind()))

			return errDecodeAny
		}

		errDecodeStruct := decodeStruct(genericStruct, rv)
		errDecodeAny.AppendInto(errDecodeStruct)

		return errDecodeAny
	case reflect.Map:
		genericMap, err := any.Map()

		if err != nil {
			errDecodeAny.Add(errmsg.ExpectedButFound(reflect.Map, reflect.TypeOf(any).Kind()))

			return errDecodeAny
		}

		errDecodeMap := decodeMap(genericMap, rv)
		errDecodeAny.AppendInto(errDecodeMap)

		return errDecodeAny
	case reflect.Ptr:
		underlyingType := rv.Type().Elem()
		underlyingTypePtr := reflect.New(underlyingType)

		err := setUnexportedField(rv, underlyingTypePtr.Interface())

		if err != nil {
			errDecodeAny.Add(err)

			return errDecodeAny
		}

		errDecodeAnyRecursive := decodeAny(any, rv.Elem())
		errDecodeAny.AppendInto(errDecodeAnyRecursive)

		return errDecodeAny
	// TODO: Array creation at runtime
	case reflect.Array:
	case reflect.Interface:
	case reflect.Invalid:
		errDecodeAny.Add(defaulterr.DecodeInvalidType)
	default:
		genericPrimitive, err := any.Primitive()

		if err != nil {
			errDecodeAny.Add(err)
			errDecodeAny.Add(errmsg.ExpectedPrimitiveButFound(kind))

			return errDecodeAny
		}

		if genericPrimitive == nil ||
			genericPrimitive.Value == nil {
			// assign Zero value to rv, but by default it should get a Zero value,
			// that's why we do nothing
			return errDecodeAny
		}

		errSetUnexportedField := setUnexportedField(rv, genericPrimitive.Value)
		errDecodeAny.Add(errSetUnexportedField)

		return errDecodeAny
	}

	errDecodeAny.Add(errmsg.ExpectedButFound(rv.Kind(), reflect.TypeOf(any).Kind()))

	return errDecodeAny
}

// Decode decodes encoded value into real object
func Decode(actualObj, encodedValue interface{}) *reflecterrs.Wrapper {
	errDecode := reflecterrs.New()

	actualReflectValue := reflect.ValueOf(actualObj)

	if actualReflectValue.Kind() != reflect.Ptr {
		errDecode.Add(errmsg.ExpectedButFound(reflect.Ptr, actualReflectValue.Kind()))

		return errDecode
	}

	if actualReflectValue.Kind() == reflect.Ptr {
		actualReflectValue = actualReflectValue.Elem()
	}

	genericType, isConvertibleToGenericType := encodedValue.(*reflecttype.Arbitrary)

	if !isConvertibleToGenericType {
		errDecode.Add(defaulterr.ExpectedPtrToGenericType)

		return errDecode
	}

	return decodeAny(genericType, actualReflectValue)
}
