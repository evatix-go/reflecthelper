package deepserialize

import (
	"gitlab.com/evatix-go/reflecthelper/deepserialize/serializer"
	"gitlab.com/evatix-go/reflecthelper/reflecterrs"
	"gitlab.com/evatix-go/reflecthelper/reflecttype"
)

// Encode encodes anything to generic type
//
// TODO: The implementation will fail in the following case.
// There's no check for this.
//
//	type Node struct {
//		Value int
//		Next *Node
//	}
//
//	n := Node{1, nil}
//	n.Next = &n  // <- pointing to itself
//
// TODO: For long linked list it may cause stack overflow.
func Encode(anyObj interface{}) (*reflecttype.Arbitrary, *reflecterrs.Wrapper) {
	return serializer.Encode(anyObj)
}
