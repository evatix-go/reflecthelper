package reflecterrs

import "gitlab.com/evatix-go/reflecthelper/internal/defaulterr"

func New() *Wrapper {
	return Empty()
}

func Empty() *Wrapper {
	items := make([]error, 0)

	return &Wrapper{
		items: items,
	}
}

func NewUsingCap(cap int) *Wrapper {
	if cap < 0 {
		panic(defaulterr.CapacityCantBeNegative)
	}

	items := make([]error, 0, cap)

	return &Wrapper{
		items: items,
	}
}
