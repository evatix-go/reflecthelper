package reflecterrs

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
)

type Wrapper struct {
	items    []error
	lastErr  error
	hasError bool
}

func (it *Wrapper) IsEmpty() bool {
	return it == nil || !it.hasError
}

func (it *Wrapper) HasAnyError() bool {
	return !it.IsEmpty()
}

func (it *Wrapper) IsSuccess() bool {
	return it.IsEmpty()
}

func (it *Wrapper) IsFailed() bool {
	return !it.IsEmpty()
}

func (it *Wrapper) IsValid() bool {
	return it.IsEmpty()
}

func (it *Wrapper) Add(err error) *Wrapper {
	if err == nil {
		return it
	}

	it.items = append(it.items, err)
	it.collectedError(err)

	return it
}

func (it *Wrapper) collectedError(err error) {
	it.setLastError(err)
	it.setHasError()
}

func (it *Wrapper) setLastError(err error) {
	it.lastErr = err
}

func (it *Wrapper) setHasError() {
	// we should set it.hasError = it.hasError || other.hasError
	// but as we've checked earlier, other must not be empty here
	// thus, we can safely set true
	it.hasError = true
}

func (it *Wrapper) AppendInto(other *Wrapper) *Wrapper {
	if other.IsEmpty() {
		return it
	}

	it.items = append(it.items, other.items...)
	it.collectedError(other.lastErr)

	return it
}

func (it *Wrapper) LastErr() error {
	return it.lastErr
}

func (it *Wrapper) Length() int {
	if it == nil || !it.hasError {
		return 0
	}

	return len(it.items)
}

func (it *Wrapper) Strings() []string {
	if it.IsEmpty() {
		return nil
	}

	numErrors := it.Length()
	errStrings := make([]string, numErrors)

	for i := 0; i < numErrors; i++ {
		errStrings[i] = it.items[i].Error()
	}

	return errStrings
}

func (it *Wrapper) String() string {
	if it.IsEmpty() {
		return ""
	}

	errStrings := it.Strings()

	if errStrings == nil {
		return ""
	}

	return strings.Join(
		errStrings,
		consts.UnixNewLine)
}

func (it *Wrapper) Handle() {
	if it.IsEmpty() {
		return
	}

	panic(it.String())
}

func (it *Wrapper) HandleWithMsg(newMessage string) {
	if it.IsEmpty() {
		return
	}

	message := fmt.Sprint(newMessage, consts.UnixNewLine, it.String())

	panic(message)
}

func (it *Wrapper) GetAsSingleError() error {
	if it.IsEmpty() {
		return nil
	}

	return errors.New(it.String())
}
