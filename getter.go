package reflecthelper

import (
	"errors"
	"fmt"
	"reflect"
	"strings"
	"unsafe"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
	"gitlab.com/evatix-go/reflecthelper/internal/errmsg"
	"gitlab.com/evatix-go/reflecthelper/internal/reflectmath"
)

type getter struct {
	opts        *Options
	isReportErr bool
	errs        []string
}

func newGetter(opt *Options, isReportErr bool) *getter {
	if opt == nil {
		return &getter{
			NewDefaultOptions(),
			isReportErr,
			make([]string, 0, 2),
		}
	}

	return &getter{
		opt,
		isReportErr,
		make([]string, 0, 2),
	}
}

func (g *getter) AddError(err string) {
	g.errs = append(g.errs, err)
}

func (g *getter) Get(from, to interface{}) error {
	if from == nil {
		return nil
	}

	valueOfIn := reflect.ValueOf(from)
	valueOfOut := reflect.ValueOf(to)

	if valueOfOut.Kind() != reflect.Ptr {
		return errmsg.ExpectedButFound(reflect.Ptr, valueOfOut.Kind())
	}

	// to support unexported struct
	if valueOfIn.Kind() != reflect.Ptr {
		srcCopyPtr := reflect.New(valueOfIn.Type())
		srcCopyPtr.Elem().Set(valueOfIn)
		valueOfIn = srcCopyPtr
	}

	readableSrc := valueOfIn.Elem()

	g.getHelperRecursive(readableSrc, valueOfOut.Elem())

	if len(g.errs) != 0 {
		return errors.New(strings.Join(g.errs, consts.UnixNewLine))
	}

	return nil
}

func (g *getter) isConvertibleTo(from, to reflect.Value) bool {
	fromType := from.Type()
	toType := to.Type()

	// both types are same, thus convertible
	if fromType == toType {
		return true
	}

	// let's check if value is convertible
	if !fromType.ConvertibleTo(toType) {
		return false
	}

	return true
}

func (g *getter) setPrivateValueAt(to, from reflect.Value) {
	toType := to.Type()
	fromType := from.Type()

	if to.Kind() != from.Kind() {
		msg := fmt.Sprintf(errmsg.CanNotConvertFromToMsgFormat.String(),
			fromType.String(), toType.String(),
		)

		if g.opts.IsPanicOnFieldTypeMismatch {
			panic(msg)
		}

		if g.isReportErr {
			g.AddError(msg)
		}
	}

	// TODO: in golang `int` is ConvertibleTo(string), which is weird
	if fromType != toType {
		if !fromType.ConvertibleTo(toType) {
			return
		}

		from = from.Convert(toType)
	}

	reflect.NewAt(toType, unsafe.Pointer(to.UnsafeAddr())).
		Elem().
		Set(from)
}

func (g *getter) getHelperRecursive(from, to reflect.Value) {
	if !to.IsValid() || !from.IsValid() {
		return
	}

	if from.Kind() == reflect.Ptr &&
		to.Kind() != reflect.Ptr {
		g.getHelperRecursive(from.Elem(), to)

		return
	}

	switch to.Kind() {
	case reflect.Ptr:
		isFromPtr := from.Kind() == reflect.Ptr
		// Case 1: from is also ptr of same type, assign pointer
		if isFromPtr && to.Type() == from.Type() {
			if from.IsNil() {
				return
			}

			// assign pointer; no value copy
			readableSrc := reflect.NewAt(from.Type(),
				unsafe.Pointer(from.UnsafeAddr())).Elem()
			reflect.NewAt(to.Type(), unsafe.Pointer(to.UnsafeAddr())).
				Elem().
				Set(readableSrc)

			return
		}

		// allocate memory for the new data
		toType := to.Type()
		toUnderlyingType := toType.Elem()
		newElement := reflect.New(toUnderlyingType)
		reflect.NewAt(toType, unsafe.Pointer(to.UnsafeAddr())).
			Elem().
			Set(newElement)

		if isFromPtr {
			g.getHelperRecursive(from.Elem(), to.Elem())

			return
		}

		// from is not pointer, try assigning value
		g.getHelperRecursive(from, to.Elem())

	case reflect.Interface:
		if from.Kind() == reflect.Interface {
			g.getHelperRecursive(from.Elem(), to)

			return
		}

		// create whatever the from is, because to is interface and
		// it can hold any value.
		newValueCreatedAsSrcType := reflect.New(from.Type()).Elem()

		// assign to newly created value
		g.getHelperRecursive(from, newValueCreatedAsSrcType)

		// now assign the newly created value to to
		g.setPrivateValueAt(to, newValueCreatedAsSrcType)

	case reflect.Struct: // to is struct
		if from.Kind() == reflect.Ptr {
			g.getHelperRecursive(from.Elem(), to)
		}

		// expecting `from` as struct
		if from.Kind() != reflect.Struct {
			if g.opts.IsPanicOnFieldTypeMismatch {
				panic(errmsg.ExpectedButFound(to.Kind(), from.Kind()))
			}

			if g.isReportErr {
				g.AddError(errmsg.ExpectedButFound(to.Kind(), from.Kind()).Error())
			}

			return
		}

		toStructType := to.Type()
		fromStructType := from.Type()
		toStructNumFields := toStructType.NumField()

		for i := 0; i < toStructNumFields; i++ {
			toStructField := to.Field(i)
			toStructFieldTypeStruct := toStructType.Field(i)
			toStructFieldName := toStructFieldTypeStruct.Name

			// skip field names
			if g.opts.IsSkipField(toStructFieldName) {
				continue
			}

			fromStructFieldTypeStruct, isFound :=
				fromStructType.FieldByName(toStructFieldName)

			// skipping filed names
			if !isFound {
				if g.opts.IsPanicOnFieldNotPresent {
					panic(
						fmt.Sprintf(errmsg.FieldNameIsNotPresentInMsgFormat.String(),
							toStructFieldName, fromStructType.String(),
						),
					)
				}

				if g.isReportErr {
					g.AddError(
						fmt.Sprintf(errmsg.FieldNameIsNotPresentInMsgFormat.String(),
							toStructFieldName, fromStructType.String(),
						),
					)
				}

				continue
			}

			fromStructField := from.FieldByIndex(fromStructFieldTypeStruct.Index)

			g.getHelperRecursive(fromStructField, toStructField)
		}

	case reflect.Slice:
		// expecting `from` as slice
		if from.Kind() != reflect.Slice {
			if g.opts.IsPanicOnFieldTypeMismatch {
				panic(errmsg.ExpectedButFound(to.Kind(), from.Kind()))
			}

			if g.isReportErr {
				g.AddError(errmsg.ExpectedButFound(to.Kind(), from.Kind()).Error())
			}

			return
		}

		if from.IsNil() {
			return
		}

		sliceLen := from.Len()
		sliceCap := from.Cap()
		newSlice := reflect.MakeSlice(from.Type(), sliceLen, sliceCap)
		reflect.NewAt(to.Type(), unsafe.Pointer(to.UnsafeAddr())).
			Elem().
			Set(newSlice)

		for i := 0; i < sliceLen; i++ {
			g.getHelperRecursive(from.Index(i), to.Index(i))
		}

	case reflect.Array:
		// expecting `from` as array
		if from.Kind() != reflect.Array {
			if g.opts.IsPanicOnFieldTypeMismatch {
				panic(errmsg.ExpectedButFound(to.Kind(), from.Kind()))
			}

			if g.isReportErr {
				g.AddError(errmsg.ExpectedButFound(to.Kind(), from.Kind()).Error())
			}

			return
		}

		arrayLen := reflectmath.Min(to.Len(), from.Len())

		for i := 0; i < arrayLen; i++ {
			g.getHelperRecursive(from.Index(i), to.Index(i))
		}

	default:
		readableSrc := reflect.NewAt(from.Type(),
			unsafe.Pointer(from.UnsafeAddr())).Elem()

		g.setPrivateValueAt(to, readableSrc)
	}
}
