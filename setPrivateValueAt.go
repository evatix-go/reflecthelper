package reflecthelper

import (
	"reflect"
	"unsafe"
)

// It is a hack to set private field values, it should work for public fields as well.
//
// However, we should not use it for private field, as it is slow process.
func setPrivateValueAt(to, from reflect.Value) {
	if to.Kind() != from.Kind() {
		return
	}

	toType := to.Type()
	fromType := from.Type()

	if fromType != toType {
		// from type is not convertible, abort
		if !fromType.ConvertibleTo(toType) {
			return
		}

		from = from.Convert(toType)
	}

	reflect.NewAt(toType, unsafe.Pointer(to.UnsafeAddr())).
		Elem().
		Set(from)
}
