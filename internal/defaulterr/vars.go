package defaulterr

import (
	"errors"

	"gitlab.com/evatix-go/reflecthelper/internal/errmsg"
)

var (
	TypeMismatchMapType       = errors.New("this type is not holding a map type")
	TypeMismatchSliceType     = errors.New("this type is not holding a slice type")
	TypeMismatchStructType    = errors.New("this type is not holding a struct type")
	TypeMismatchPrimitiveType = errors.New("this type is not holding a primitive type")
	GenericMapIsNil           = errors.New("generic map is nil")
	GenericStructIsNil        = errors.New("generic struct is nil")
	GenericSliceIsNil         = errors.New("generic slice is nil")
	ExpectedPtrToGenericType  = errors.New("expected ptr to generic type")
	UnexpectedValueInEncoder  = errors.New("unexpected value in encoder")
	NoTypeInfo                = errors.New("json-map does not contain valid type info")
	InvalidType               = errors.New("json-map contains invalid type")
	InvalidSlice              = errors.New("json-map does not contain a valid slice")
	InvalidMap                = errors.New("json-map does not contain a valid map")
	InvalidStruct             = errors.New("json-map does not contain a valid struct")
	EncodingInvalidType       = errors.New("encoding invalid type")
	DecodeInvalidType         = errors.New("decode invalid type")
	CapacityCantBeNegative    = errors.New("capacity can't be negative")
	InvalidTypeCannotReduce   = errors.New("cannot reduce type to anything further, invalid type given")
	FieldIsEmpty              = errmsg.FieldIsEmpty.Error()
)
