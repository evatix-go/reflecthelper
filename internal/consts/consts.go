package consts

const (
	UnixNewLine                                    = "\n"
	Comma                                          = ","
	SprintFValueFormat                             = "%+v"
	SprintFTypeFormat                              = "%T"
	Invalid                                        = -1
	N4                                             = 4
	N5                                             = 5
	ResultKeyValuePairSlice                 string = "ResultKeyValuePairSlice" // ResultKeyValuePairSlice for Map
	ReflectionType                          string = "ReflectionType"          // ReflectionType for all generic types
	ResultSlice                             string = "ResultSlice"             // ResultSlice for generic slice type
	ResultMap                               string = "ResultMap"               // ResultMap for generic struct
	Key                                     string = "Key"                     // Key for KeyValuePair Key
	Value                                   string = "Value"                   // Value for KeyValuePair Value and primitive types
	ThisType                                string = "ThisType"                // ThisType
	Dot                                            = "."
	NewLineTabJoiner                               = "\n\t"
	QueryFieldNameToActualExpectedLogFormat        = "%s -> (Actual) => `%v` | `%v` <= (Expected)" // key, actual, expected
	DefaultNewLine                                 = "\n"
)
