package fieldname

import "unicode"

func TranspileToPrivateMust(fieldName string) string {
	if len(fieldName) == 0 {
		return ""
	}

	fieldNameRune := []rune(fieldName)
	// make the first char lowercase
	fieldNameRune[0] = unicode.ToLower(fieldNameRune[0])

	return string(fieldNameRune)
}
