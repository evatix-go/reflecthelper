package fieldname

import (
	"unicode"

	"gitlab.com/evatix-go/reflecthelper/internal/defaulterr"
)

func TranspileToPrivate(fieldName string) (string, error) {
	if len(fieldName) == 0 {
		return "", defaulterr.FieldIsEmpty
	}

	fieldNameRune := []rune(fieldName)
	// make the first char lowercase
	fieldNameRune[0] = unicode.ToLower(fieldNameRune[0])

	return string(fieldNameRune), nil
}
