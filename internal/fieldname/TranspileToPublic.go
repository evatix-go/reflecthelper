package fieldname

import (
	"unicode"

	"gitlab.com/evatix-go/reflecthelper/internal/defaulterr"
)

func TranspileToPublic(fieldName string) (string, error) {
	if len(fieldName) == 0 {
		return "", defaulterr.FieldIsEmpty
	}

	fieldNameRune := []rune(fieldName)
	// make the first char uppercase
	fieldNameRune[0] = unicode.ToUpper(fieldNameRune[0])

	return string(fieldNameRune), nil
}
