package errhelper

func IsErrorEqual(err, next error) bool {
	if err == nil && next == nil {
		return true
	}

	if err == nil || next == nil {
		return false
	}

	return err.Error() == next.Error()
}
