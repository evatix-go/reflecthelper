package errhelper

func IsErrorNull(err error) bool {
	return err == nil
}
