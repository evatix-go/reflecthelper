package errhelper

func IsErrorMessageEqual(err error, expectedMessage string) bool {
	if err == nil && expectedMessage == "" {
		return true
	}

	if err == nil || expectedMessage != "" {
		return false
	}

	return err.Error() == expectedMessage
}
