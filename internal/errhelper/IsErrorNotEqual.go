package errhelper

func IsErrorNotEqual(err, next error) bool {
	if err == nil && next == nil {
		return false
	}

	if err == nil || next == nil {
		return true
	}

	return err.Error() != next.Error()
}
