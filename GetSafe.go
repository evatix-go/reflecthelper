package reflecthelper

func GetSafe(from, to interface{}, skippingFieldNames ...string) error {
	opts := NewReflectGetOptions(
		false, // is panic on field not present
		false, // is panic on field type mismatch
		skippingFieldNames...,
	)

	g := newGetter(opts, true)

	return g.Get(from, to)
}
