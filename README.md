![](Logo)

# reflecthelper

Get Set Value dynamically

## Git Clone

`git clone https://gitlab.com/evatix-go/reflecthelper.git`

### 2FA enabled, for linux

`git clone https://[YourGitLabUserName]:[YourGitlabAcessTokenGenerateFromGitlabsTokens]@gitlab.com/evatix-go/reflecthelper.git`

### Prerequisites

- Update git to latest 2.29
- Update or install the latest of Go 1.15.2
- Either add your ssh key to your gitlab account
- Or, use your access token to clone it.

## Installation

`go get gitlab.com/evatix-go/reflecthelper`

### Go get issue for private package

- Update git to 2.29
- Enable go modules. (Windows : `go env -w GO111MODULE=on`, Unix : `export GO111MODULE=on`)
- Add `gitlab.com/evatix-go` to go env private

To set for Windows:

`go env -w GOPRIVATE=[AddExistingOnes;]gitlab.com/evatix-go`

To set for Unix:

`expoort GOPRIVATE=[AddExistingOnes;]gitlab.com/evatix-go`

## Why `reflecthelper`?

## Examples

```go
package main

import (
    "fmt"
    "log"
    "reflect"

    "gitlab.com/evatix-go/reflecthelper"
)

func main() {
    type SomeStruct struct {
        SomeInt         int
        SomeStringPtr   *string
        SomeSlice       *[]int
    }

    type SomeWrapperStruct struct {
        PtrToSomeStruct *SomeStruct
    }

    myMsg := "Hello World"
    var destStruct  SomeWrapperStruct
    srcStruct := SomeWrapperStruct{
        &SomeStruct{
            17,
            &myMsg,
            &[]int{1, 2, 3},
        },
    }

    err := reflecthelper.Get(&destStruct, srcStruct)

    if err != nil {
        log.Fatal(err)
    }

    fmt.Println(reflect.DeepEqual(srcStruct, destStruct))
}
```

## Acknowledgement

Any other packages used

## Links

* [go - Using reflect, how do you set the value of a struct field? - Stack Overflow](https://stackoverflow.com/questions/6395076/using-reflect-how-do-you-set-the-value-of-a-struct-field/6396678#6396678)

## Issues

- [Create your issues](https://gitlab.com/evatix-go/reflecthelper/-/issues)

## Notes

## Contributors

## License

[Evatix MIT License](/LICENSE)
