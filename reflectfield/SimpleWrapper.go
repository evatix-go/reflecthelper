package reflectfield

import (
	"fmt"
	"reflect"
	"unsafe"

	"gitlab.com/evatix-go/reflecthelper/isreflect"
	"gitlab.com/evatix-go/reflecthelper/reflectkind"
)

type SimpleWrapper struct {
	FieldIndex   int
	Name         string
	Field        reflect.StructField
	ReflectValue reflect.Value
	Kind         reflect.Kind
	IsValid      bool
}

func (it *SimpleWrapper) ExpandFieldToMap() map[string]*SimpleWrapper {
	return New.SimpleWrapper.ReflectValueToMap(it.ReflectValue)
}

func (it *SimpleWrapper) IsPrivateField() bool {
	return it.Field.PkgPath == ""
}

func (it *SimpleWrapper) IsPublicField() bool {
	return it.Field.PkgPath != ""
}

func (it *SimpleWrapper) IsPointer() bool {
	return it.Kind == reflect.Ptr
}

func (it *SimpleWrapper) IsNotPointer() bool {
	return it.Kind != reflect.Ptr
}

func (it *SimpleWrapper) IsValueType() bool {
	return it.Kind != reflect.Ptr
}

func (it *SimpleWrapper) IsKind(kind reflect.Kind) bool {
	return it.Kind == kind
}

func (it *SimpleWrapper) IsNotKind(kind reflect.Kind) bool {
	return it.Kind != kind
}

func (it *SimpleWrapper) IsSlice() bool {
	return it.Kind == reflect.Slice
}

func (it *SimpleWrapper) IsSliceOrArray() bool {
	return it.Kind == reflect.Slice || it.Kind == reflect.Array
}

func (it *SimpleWrapper) IsStruct() bool {
	return it.Kind == reflect.Struct
}

func (it *SimpleWrapper) EnhanceKind() reflectkind.Enhance {
	return reflectkind.Enhance(it.Kind)
}

func (it *SimpleWrapper) IsInvalid() bool {
	return it == nil || !it.IsValid
}

func (it *SimpleWrapper) IsValueNull() bool {
	return it == nil || !it.IsValid || isreflect.Null(it.ReflectValue)
}

func (it *SimpleWrapper) IsPrimitive() bool {
	return isreflect.Primitive(it.Kind)
}

func (it *SimpleWrapper) IsMap() bool {
	return it.Kind == reflect.Map
}

func (it *SimpleWrapper) ReducePointerValue() interface{} {
	if it.IsPointer() {
		return it.ReflectValue.Elem().Interface()
	}

	return it.ReflectValue.Interface()
}

func (it *SimpleWrapper) ValueIntegerDefault() int {
	return it.ValueInteger(0)
}

func (it *SimpleWrapper) ValueInteger(defaultVal int) int {
	valInf := it.ReducePointerValue()
	valInt, isOkay := valInf.(int)

	if isOkay {
		return valInt
	}

	return defaultVal
}

func (it *SimpleWrapper) ValueStringDefault() string {
	return it.ValueString("")
}

func (it *SimpleWrapper) ValueString(defaultString string) string {
	valInf := it.ReducePointerValue()
	valueCasted, isOkay := valInf.(string)

	if isOkay {
		return valueCasted
	}

	return defaultString
}

func (it *SimpleWrapper) ConvValueString() string {
	val := it.PublicValueInterface()

	if val == nil {
		return ""
	}

	return fmt.Sprintf(
		"%v",
		it.PublicValueInterface())
}

func (it *SimpleWrapper) ValueStrings() []string {
	valInf := it.ReducePointerValue()
	valueCasted, isOkay := valInf.([]string)

	if isOkay {
		return valueCasted
	}

	return nil
}

func (it *SimpleWrapper) PublicValueInterface() interface{} {
	if it.IsPublicField() {
		return it.ReducePointerValue()
	}

	return nil
}

func (it *SimpleWrapper) ValueInterfaceIncludingPrivateField() interface{} {
	if it.IsPublicField() {
		return it.ReducePointerValue()
	}

	return it.PrivateValueInterface()
}

func (it *SimpleWrapper) PrivateValueInterface() interface{} {
	if it.IsPublicField() {
		return nil
	}

	unexportedField := reflect.NewAt(
		it.Field.Type,
		unsafe.Pointer(it.ReflectValue.UnsafeAddr())).
		Elem()

	return unexportedField.Interface()
}

func (it *SimpleWrapper) String() string {
	return it.ConvValueString()
}

func (it *SimpleWrapper) Wrapper() *Wrapper {
	if it.IsInvalid() {
		return New.Wrapper.Invalid()
	}

	return New.
		Wrapper.
		UsingAllParams(
			it.FieldIndex,
			&it.Field,
			it.ReflectValue)
}
