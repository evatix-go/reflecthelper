package reflectfield

import (
	"reflect"
	"sync"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
	"gitlab.com/evatix-go/reflecthelper/isreflect"
	"gitlab.com/evatix-go/reflecthelper/reflectkind"
)

type newStructFieldsMapCreator struct{}

func (it *newStructFieldsMapCreator) AnyDefault(
	anyItem interface{},
) *StructFieldsMap {
	return it.Any(
		defaultPointerReductionLevel,
		anyItem)
}

func (it *newStructFieldsMapCreator) AnyStruct(
	anyItem interface{},
) *StructFieldsMap {
	return it.Any(
		0,
		anyItem)
}

func (it *newStructFieldsMapCreator) Invalid() *StructFieldsMap {
	return &StructFieldsMap{
		WrappersMap: map[string]*Wrapper{},
	}
}

func (it *newStructFieldsMapCreator) Null() *StructFieldsMap {
	return nil
}

func (it *newStructFieldsMapCreator) UsingRawParams(
	rawAnyValue interface{},
	kind reflect.Kind,
	structValue reflect.Value,
	structType reflect.Type,
	items map[string]*Wrapper,
) *StructFieldsMap {
	length := len(items)
	isNil := isreflect.Null(structValue)
	isPtr := kind == reflect.Ptr
	isInterface := kind == reflect.Interface
	kindEnhance := reflectkind.Enhance(kind)

	if length <= 0 {
		return &StructFieldsMap{
			RawAnyValue: rawAnyValue,
			WrappersMap: map[string]*Wrapper{},
			Length:      0,
			Type:        structType,
			HasAnyNull:  isNil,
			Kind:        kind,
			KindEnhance: kindEnhance,
			IsPointer:   isPtr,
			IsInterface: isInterface,
		}
	}

	hasAnyNils := false
	wrappersMap := make(map[string]*Wrapper, length)
	wrappersSlice := make([]*Wrapper, length)
	fieldsMap := make(map[string]*reflect.StructField, length)
	fieldsNames := make([]string, length)
	publicNames := make(map[string]int, length)
	privateNames := make(map[string]int, length)
	fieldNamesMap := make(map[string]int, length)

	// requires sync call
	for _, wrapper := range items {
		index := wrapper.Index
		name := wrapper.Name
		publicName := wrapper.PublicName
		privateName := wrapper.PrivateName
		publicNames[publicName] = index
		privateNames[privateName] = index
		fieldsMap[name] = wrapper.Field
		fieldNamesMap[name] = index
		wrappersMap[name] = wrapper

		if !hasAnyNils && isreflect.Null(wrapper.Value) {
			hasAnyNils = true
		}
	}

	return &StructFieldsMap{
		RawAnyValue:            rawAnyValue,
		WrappersMap:            wrappersMap,
		FieldsMap:              fieldsMap,
		Wrappers:               wrappersSlice,
		FieldNames:             fieldsNames,
		FieldNamesMap:          fieldNamesMap,
		TranspiledPublicNames:  publicNames,
		TranspiledPrivateNames: privateNames,
		Length:                 length,
		ReflectValue:           structValue,
		Type:                   structType,
		IsValid:                true,
		Kind:                   kind,
		KindEnhance:            kindEnhance,
		IsPointer:              isPtr,
		IsInterface:            isInterface,
		HasAnyNull:             hasAnyNils,
	}
}

// UsingReflectValDefault
//
// level defaultPointerReductionLevel
func (it *newStructFieldsMapCreator) UsingReflectValDefault(
	rawAnyValue interface{},
	structValue reflect.Value,
) *StructFieldsMap {
	return it.UsingReflectVal(
		defaultPointerReductionLevel,
		rawAnyValue,
		structValue)
}

// UsingReflectValStruct
//
// level 0
func (it *newStructFieldsMapCreator) UsingReflectValStruct(
	rawAnyValue interface{},
	structValue reflect.Value,
) *StructFieldsMap {
	return it.UsingReflectVal(
		0,
		rawAnyValue,
		structValue)
}

// UsingReflectVal
//
// level -1 : represent unlimited loops to get `*****ToValue` to `ToValue`
func (it *newStructFieldsMapCreator) UsingReflectVal(
	pointerReductionLevel int,
	rawAnyValue interface{},
	structValue reflect.Value,
) *StructFieldsMap {
	structValueKind := structValue.Kind()
	hasLevel := pointerReductionLevel > consts.Invalid
	isNil := isreflect.Null(structValue)
	isPtr := structValueKind == reflect.Ptr
	isInterface := structValueKind == reflect.Interface

	// reducing ****ToValue to ToValue
	for pointerReductionLevel != 0 && structValueKind == reflect.Ptr || structValueKind == reflect.Interface {
		structValue = structValue.Elem()
		// mutating dangerous code
		structValueKind = structValue.Kind()

		pointerReductionLevel--
		if hasLevel && pointerReductionLevel <= 0 {
			break
		}
	}

	kindEnhance := reflectkind.Enhance(structValueKind)
	// getting structType later (after reduction), because if value if ptr
	// then structType will be of type `*T` where `T` is struct type
	// and later `structType.NumField()` call will fail
	structType := structValue.Type()

	if !structValue.IsValid() || structValueKind != reflect.Struct {
		// invalid
		return &StructFieldsMap{
			RawAnyValue:  rawAnyValue,
			WrappersMap:  map[string]*Wrapper{},
			ReflectValue: reflect.Value{},
			Type:         structType,
			Kind:         structValueKind,
			KindEnhance:  kindEnhance,
			IsPointer:    isPtr,
			IsInterface:  isInterface,
			HasAnyNull:   isNil,
		}
	}

	structFieldsLength := structType.NumField()

	if structFieldsLength == 0 {
		// invalid
		return &StructFieldsMap{
			RawAnyValue: rawAnyValue,
			WrappersMap: map[string]*Wrapper{},
			Type:        structType,
			HasAnyNull:  isNil,
			Kind:        structValueKind,
			KindEnhance: kindEnhance,
			IsPointer:   isPtr,
			IsInterface: isInterface,
		}
	}

	wrappersMap := make(map[string]*Wrapper, structFieldsLength)
	wrappersSlice := make([]*Wrapper, structFieldsLength)
	fieldsMap := make(map[string]*reflect.StructField, structFieldsLength)
	fieldsNames := make([]string, structFieldsLength)
	publicNames := make(map[string]int, structFieldsLength)
	privateNames := make(map[string]int, structFieldsLength)
	fieldNamesMap := make(map[string]int, structFieldsLength)

	wg := sync.WaitGroup{}
	wg.Add(structFieldsLength)
	newWrapper := New.Wrapper.UsingAllParams

	processorFunc := func(index int) {
		fieldType := structType.Field(index)
		fieldValue := structValue.Field(index)
		wrapper := newWrapper(
			index,
			&fieldType,
			fieldValue,
		)

		name := wrapper.Name
		fieldsNames[index] = name
		wrappersSlice[index] = wrapper

		wg.Done()
	}

	for i := 0; i < structFieldsLength; i++ {
		go processorFunc(i)
	}

	wg.Wait()

	hasAnyNils := false
	// requires sync call
	for index, wrapper := range wrappersSlice {
		name := wrapper.Name
		publicName := wrapper.PublicName
		privateName := wrapper.PrivateName
		publicNames[publicName] = index
		privateNames[privateName] = index
		fieldsMap[name] = wrapper.Field
		fieldNamesMap[name] = index
		wrappersMap[name] = wrapper

		if !hasAnyNils && wrapper.IsAnyNull() {
			hasAnyNils = true
		}
	}

	return &StructFieldsMap{
		RawAnyValue:            rawAnyValue,
		WrappersMap:            wrappersMap,
		FieldsMap:              fieldsMap,
		Wrappers:               wrappersSlice,
		FieldNames:             fieldsNames,
		FieldNamesMap:          fieldNamesMap,
		TranspiledPublicNames:  publicNames,
		TranspiledPrivateNames: privateNames,
		Length:                 structFieldsLength,
		ReflectValue:           structValue,
		Type:                   structType,
		IsValid:                true,
		HasAnyNull:             hasAnyNils,
		Kind:                   structValueKind,
		KindEnhance:            kindEnhance,
		IsPointer:              isPtr,
		IsInterface:            isInterface,
	}
}

// Any
//
// level -1 : represent unlimited loops to get `*****ToValue` to `ToValue`
func (it *newStructFieldsMapCreator) Any(
	pointerReduceLevel int,
	structInput interface{},
) *StructFieldsMap {
	structValue := reflect.ValueOf(structInput)

	return it.UsingReflectVal(
		pointerReduceLevel,
		structInput,
		structValue)
}

func (it *newStructFieldsMapCreator) AnyItems(
	pointerReductionLevel int,
	inputs ...interface{},
) []*StructFieldsMap {
	if inputs == nil {
		return nil
	}

	length := len(inputs)
	list := make([]*StructFieldsMap, length)
	wg := &sync.WaitGroup{}
	wg.Add(length)

	indexProcessor := func(index int) {
		list[index] = it.Any(
			pointerReductionLevel,
			inputs[index])

		wg.Done()
	}

	for i := 0; i < length; i++ {
		go indexProcessor(i)
	}

	wg.Wait()

	return list
}
