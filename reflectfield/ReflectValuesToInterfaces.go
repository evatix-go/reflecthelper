package reflectfield

import "reflect"

func ReflectValuesToInterfaces(
	reflectValues []reflect.Value,
) []interface{} {
	if len(reflectValues) == 0 {
		return []interface{}{}
	}

	list := make(
		[]interface{},
		len(reflectValues))

	for i, rv := range reflectValues {
		list[i] = ReflectValueToAnyValue(rv)
	}

	return list
}
