package reflectfield

type SimpleWrappersMap struct {
	RawInput interface{}
	Items    map[string]SimpleWrapper
}

func (it *SimpleWrappersMap) Length() int {
	if it == nil {
		return 0
	}

	return len(it.Items)
}

func (it *SimpleWrappersMap) IsEmpty() bool {
	return it.Length() == 0
}

func (it *SimpleWrappersMap) HasField(fieldName string) bool {
	if it.IsEmpty() {
		return false
	}

	_, has := it.Items[fieldName]

	return has
}

func (it *SimpleWrappersMap) IsMissing(fieldName string) bool {
	if it.IsEmpty() {
		return true
	}

	_, has := it.Items[fieldName]

	return !has
}

func (it *SimpleWrappersMap) GetSimpleWrapper(fieldName string) *SimpleWrapper {
	if it.IsEmpty() {
		return nil
	}

	simpleWrapper, has := it.Items[fieldName]

	if has {
		return &simpleWrapper
	}

	return nil
}
