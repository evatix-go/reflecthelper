package reflectfield

import "reflect"

type newMethodWrapperCreator struct{}

func (it *newMethodWrapperCreator) Invalid() *MethodWrapper {
	return &MethodWrapper{}
}

func (it *newMethodWrapperCreator) Create(
	method reflect.Method,
) *MethodWrapper {
	return &MethodWrapper{
		MethodReflectValue: method.Func,
		Method:             method,
		Name:               method.Name,
		Index:              method.Index,
		IsValid:            method.Name != "",
	}
}
