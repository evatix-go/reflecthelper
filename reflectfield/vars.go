package reflectfield

import "reflect"

var (
	New = &newCreator{
		StructFieldsMap: &newStructFieldsMapCreator{},
		Wrapper:         &newWrapperCreator{},
		SimpleHashset:   &newSimpleHashsetCreator{},
		SimpleWrapper:   &newSimpleWrapperCreator{},
		MethodWrapper:   &newMethodWrapperCreator{},
	}
	Empty           = &emptyCreator{}
	allIntegerKinds = []reflect.Kind{
		reflect.Int,
		reflect.Int8,
		reflect.Int16,
		reflect.Int32,
		reflect.Int64,
	}
)
