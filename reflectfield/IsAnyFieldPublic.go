package reflectfield

import "reflect"

func IsAnyFieldPublic(fields ...reflect.StructField) bool {
	for _, field := range fields {
		if field.PkgPath != "" {
			return true
		}
	}

	return false
}
