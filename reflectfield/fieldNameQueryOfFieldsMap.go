package reflectfield

import (
	"reflect"
	"strings"
	"sync"

	"gitlab.com/evatix-go/reflecthelper/internal/internalds"
)

type fieldNameQueryOfFieldsMap struct {
	fieldsMap *StructFieldsMap
}

// ExpandChildToFieldsMap
//
//  fieldQueryToExpand : field1.field2.field3.field4
//  It will expand  field queries ("field1.field2.field3.field4") each level of field and return the final map
//
// What is fieldNameQuery?
//
//  - It can be a field name or targeting nested field names like F1.F2.F3
//  - Here, F1 is the first field then we will find F2 inside F1, and finally F3 under f2 field.
//
// Examples:
// 	- ExpandChildToFieldsMap("F1.F2") -> get StructFieldsMap of F2 inside F1
// 	- {fieldQueryToExpand : "F1"} -> returns F1 field expanding fieldsMap
// 	- {fieldQueryToExpand : "F1.F2"} -> returns F1 field then F2 fields expanding fieldsMap
func (it *fieldNameQueryOfFieldsMap) ExpandChildToFieldsMap(fieldQueryToExpand string) *StructFieldsMap {
	finalWrapperByQuery := it.ChildWrapper(fieldQueryToExpand)

	if finalWrapperByQuery != nil {
		return finalWrapperByQuery.ExpandValueToFieldsMap()
	}

	// not found
	return nil
}

// ChildWrapper (nested child wrapper by recursive depth)
//
//  fieldQueryToExpand : field1.field2.field3.field4
//  It will expand  field queries ("field1.field2.field3.field4") each level of field and return the final map
//
// What is fieldNameQuery?
//
//  - It can be a field name or targeting nested field names like F1.F2.F3
//  - Here, F1 is the first field then we will find F2 inside F1, and finally F3 under f2 field.
//
// Examples:
// 	- `ExpandChildToFieldsMap("F1.F2")` -> get StructFieldsMap of F2 inside F1
// 	- {fieldQueryToExpand : "F1"} -> returns F1 field expanding fieldsMap
// 	- {fieldQueryToExpand : "F1.F2"} -> returns F1 field then F2 fields expanding fieldsMap
func (it *fieldNameQueryOfFieldsMap) ChildWrapper(fieldQuery string) *Wrapper {
	splitFields := strings.Split(fieldQuery, dotSplitter)
	length := len(splitFields)

	if length == 1 && splitFields[0] == emptyString {
		return nil
	} else if length == 1 {
		wrapper, _ := it.fieldsMap.WrappersMap[fieldQuery]

		return wrapper
	}

	// now the list has at least 2 items in it.
	parentFieldName := splitFields[0] // take first
	restFields := splitFields[1:]     // leave the rest as is
	parentFieldWrapper := it.fieldsMap.GetWrapper(parentFieldName)

	return it.findRecursiveChildNestedWrapper(
		restFields,
		parentFieldWrapper)
}

// ChildFieldType
//
//  fieldQueryToExpand : field1.field2.field3.field4
//  It will expand field query or fieldQueryToExpand ("field1.field2.field3.field4")
//  each level of field and returns fieldsMaps.
//
// What is fieldNameQuery or fieldQueryToExpand?
//
//  - It can be a field name or targeting nested field names like F1.F2.F3
//  - Here, F1 is the first field then we will find F2 inside F1,
//      and finally F3 under f2 field.
//
// Examples:
//  - fieldQueryToExpand : field1.field2.field3.field4
// 	    returns type of field4
func (it *fieldNameQueryOfFieldsMap) ChildFieldType(fieldQueryToType string) reflect.Type {
	finalWrapperByQuery := it.ChildWrapper(fieldQueryToType)

	if finalWrapperByQuery != nil {
		return finalWrapperByQuery.Type
	}

	// not found
	return nil
}

func (it *fieldNameQueryOfFieldsMap) ChildFieldPublicValue(fieldQueryToType string) interface{} {
	finalWrapperByQuery := it.ChildWrapper(fieldQueryToType)

	if finalWrapperByQuery != nil {
		return finalWrapperByQuery.PublicValue()
	}

	// not found
	return nil
}

func (it *fieldNameQueryOfFieldsMap) ChildFieldValueOrError(fieldQueryToType string) interface{} {
	finalWrapperByQuery := it.ChildWrapper(fieldQueryToType)

	if finalWrapperByQuery != nil {
		return finalWrapperByQuery.ValueInterfaceOrError()
	}

	// not found
	return nil
}

func (it *fieldNameQueryOfFieldsMap) ChildFieldValueForce(fieldQueryToType string) (interface{}, error) {
	finalWrapperByQuery := it.ChildWrapper(fieldQueryToType)

	if finalWrapperByQuery != nil {
		return finalWrapperByQuery.ValueInterfaceForce()
	}

	// not found
	return nil, nil
}

// ExpandMultipleToFieldsMap
//
//  fieldQueryToExpand : field1.field2.field3.field4
//  It will expand multiple field queries ("field1.field2.field3.field4")
//  each level of field and collected fieldsMaps using a map[Key:fieldNameQuery]*StructFieldsMap
//
// What is fieldNameQuery?
//
//  - It can be a field name or targeting nested field names like F1.F2.F3
//  - Here, F1 is the first field then we will find F2 inside F1, and finally F3 under f2 field.
//
// Examples:
// 	- ExpandChildToFieldsMap("F1.F2", "F1") -> get map[QueriesKey]*StructFieldsMap
// 	- fieldQueriesToExpands : []string { "field1.field2.field3.field4" } ->
// 	    returns "field1.field2.field3.field4"
// 	    field expanding fieldsMap
// 	    using a map["field1.field2.field3.field4"]*StructFieldsMap
func (it *fieldNameQueryOfFieldsMap) ExpandMultipleToFieldsMap(
	fieldQueriesToExpands ...string,
) (foundQueriesMap map[string]*StructFieldsMap, missingQueries []string) {
	foundWrappersMap, missingQueries := it.MultipleToWrapperMap(
		fieldQueriesToExpands...)

	if len(foundWrappersMap) == 0 {
		return nil, missingQueries
	}

	foundQueriesMap = make(
		map[string]*StructFieldsMap,
		len(foundWrappersMap))

	for fieldQueryName, wrapper := range foundWrappersMap {
		foundQueriesMap[fieldQueryName] =
			wrapper.ExpandValueToFieldsMap()
	}

	foundWrappersMap = nil

	return foundQueriesMap, missingQueries
}

// MultipleToWrapperMap
//
//  fieldQueries : field1.field2.field3.field4
//  It will expand multiple field queries ("field1.field2.field3.field4")
//  each level of field and collected reflect.Type using a map[Key:fieldNameQuery]*Wrapper
//
// What is fieldNameQuery?
//
//  - It can be a field name or targeting nested field names like F1.F2.F3
//  - Here, F1 is the first field then we will find F2 inside F1, and finally F3 under f2 field.
//
// Examples:
// 	- MultipleToTypeMap("F1.F2", "F1") -> get map[QueriesKey]*Wrapper
// 	- fieldQueriesToExpands : []string { "field1.field2.field3.field4" } ->
// 	    returns "field1.field2.field3.field4"
// 	    field * Wrapper
// 	    using a map["field1.field2.field3.field4"]*Wrapper
func (it *fieldNameQueryOfFieldsMap) MultipleToWrapperMap(
	fieldQueries ...string,
) (foundQueriesMap map[string]*Wrapper, missingQueries []string) {
	length := len(fieldQueries)

	if length == 0 {
		return nil, nil
	}

	if it.fieldsMap.IsInvalidOrEmptyLength() {
		// all missing
		return nil, fieldQueries
	}

	processedItems := make(
		[]*internalds.AnyKeyVal,
		length)

	wg := sync.WaitGroup{}
	wg.Add(length)
	locker := sync.Mutex{}

	processorFunc := func(index int, fieldNameQuery string) {
		defer wg.Done()

		finalWrapper := it.ChildWrapper(
			fieldNameQuery)
		if finalWrapper == nil {
			locker.Lock()

			missingQueries = append(
				missingQueries,
				fieldNameQuery)

			locker.Unlock()

			return
		}

		processedItems[index] = &internalds.AnyKeyVal{
			Key:      fieldNameQuery,
			AnyValue: finalWrapper,
		}
	}

	for i, fieldNameQuery := range fieldQueries {
		go processorFunc(i, fieldNameQuery)
	}

	wg.Wait()

	foundQueriesMap = make(
		map[string]*Wrapper,
		length)

	for _, anyKeyVal := range processedItems {
		if anyKeyVal == nil {
			continue
		}

		foundQueriesMap[anyKeyVal.Key] =
			anyKeyVal.AnyValue.(*Wrapper)
	}

	return foundQueriesMap, missingQueries
}

func (it *fieldNameQueryOfFieldsMap) MultipleToValuesMap(
	valuesProcessorFunc func(wrapper *Wrapper) (val interface{}),
	fieldQueries ...string,
) (foundQueriesMap map[string]interface{}, missingQueries []string) {
	foundWrappersMap, missingQueries := it.MultipleToWrapperMap(
		fieldQueries...)

	if len(foundWrappersMap) == 0 {
		return nil, missingQueries
	}

	foundQueriesMap = make(
		map[string]interface{},
		len(foundWrappersMap))

	for fieldQueryName, wrapper := range foundWrappersMap {
		foundQueriesMap[fieldQueryName] =
			valuesProcessorFunc(wrapper)
	}

	foundWrappersMap = nil

	return foundQueriesMap, missingQueries
}

// MultipleToTypeMap
//
//  fieldQueriesToExpands : field1.field2.field3.field4
//  It will expand multiple field queries ("field1.field2.field3.field4")
//  each level of field and collected reflect.Type using a map[Key:fieldNameQuery]reflect.Type
//
// What is fieldNameQuery?
//
//  - It can be a field name or targeting nested field names like F1.F2.F3
//  - Here, F1 is the first field then we will find F2 inside F1, and finally F3 under f2 field.
//
// Examples:
// 	- MultipleToTypeMap("F1.F2", "F1") -> get map[QueriesKey]reflect.Type
// 	- fieldQueriesToExpands : []string { "field1.field2.field3.field4" } ->
// 	    returns "field1.field2.field3.field4"
// 	    field expanding reflect.Type
// 	    using a map["field1.field2.field3.field4"]reflect.Type
func (it *fieldNameQueryOfFieldsMap) MultipleToTypeMap(
	fieldQueries ...string,
) (foundQueriesMap map[string]reflect.Type, missingQueries []string) {
	foundWrappersMap, missingQueries := it.MultipleToWrapperMap(
		fieldQueries...)

	if len(foundWrappersMap) == 0 {
		return nil, missingQueries
	}

	foundQueriesMap = make(
		map[string]reflect.Type,
		len(foundWrappersMap))

	for fieldQueryName, wrapper := range foundWrappersMap {
		foundQueriesMap[fieldQueryName] =
			wrapper.Type
	}

	foundWrappersMap = nil

	return foundQueriesMap, missingQueries
}

// MultipleToTagsMap
//
//  fieldQueriesToExpands : field1.field2.field3.field4
//  It will expand multiple field queries ("field1.field2.field3.field4")
//  each level of field and collected reflect.Type using a map[Key:fieldNameQuery]reflect.StructTag
//
// What is fieldNameQuery?
//
//  - It can be a field name or targeting nested field names like F1.F2.F3
//  - Here, F1 is the first field then we will find F2 inside F1, and finally F3 under f2 field.
//
// Examples:
// 	- MultipleToTagsMap("F1.F2", "F1") -> get map[QueriesKey]reflect.StructTag
// 	- fieldQueriesToExpands : []string { "field1.field2.field3.field4" } ->
// 	    returns "field1.field2.field3.field4"
// 	    field expanding reflect.StructTag
// 	    using a map["field1.field2.field3.field4"]reflect.StructTag
func (it *fieldNameQueryOfFieldsMap) MultipleToTagsMap(
	fieldQueries ...string,
) (foundQueriesMap map[string]reflect.StructTag, missingQueries []string) {
	foundWrappersMap, missingQueries := it.MultipleToWrapperMap(
		fieldQueries...)

	if len(foundWrappersMap) == 0 {
		return nil, missingQueries
	}

	foundQueriesMap = make(
		map[string]reflect.StructTag,
		len(foundWrappersMap))

	for fieldQueryName, wrapper := range foundWrappersMap {
		foundQueriesMap[fieldQueryName] =
			wrapper.Tag
	}

	foundWrappersMap = nil

	return foundQueriesMap, missingQueries
}

// findRecursiveChildNestedWrapper
//
//  Recursive reduce restFieldNames to nothing and returns the final StructFieldsMap
//  Called by ExpandChildToFieldsMap("F1.F2.F3.F4.F5") -> expandNestedFieldsBy
//
// For example,
//  - ExpandChildToFieldsMap("F1.F2.F3.F4.F5") sends as
//      - `{
//              restFieldChainNames : []string{"F2", "F3", ...},
//              parentFieldWrapper: F1Wrapper
//         }`
func (it *fieldNameQueryOfFieldsMap) findRecursiveChildNestedWrapper(
	restFieldChainNames []string,
	parentFieldWrapper *Wrapper,
) *Wrapper {
	if parentFieldWrapper == nil {
		// something wrong
		return nil
	}

	if len(restFieldChainNames) == 0 {
		// not rest items
		return parentFieldWrapper
	}

	currentWorkingFieldName := restFieldChainNames[0]
	restFieldChainNames = restFieldChainNames[1:]
	fieldsMap := parentFieldWrapper.
		ExpandValueToFieldsMap()
	currentFieldWrapper := fieldsMap.
		GetWrapper(currentWorkingFieldName)

	go fieldsMap.Dispose()
	fieldsMap = nil // no need to have future reference

	if currentFieldWrapper == nil {
		return nil
	}

	// reduce each field by recursive call.
	return it.findRecursiveChildNestedWrapper(
		restFieldChainNames,
		currentFieldWrapper)
}
