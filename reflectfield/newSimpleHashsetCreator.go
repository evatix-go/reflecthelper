package reflectfield

type newSimpleHashsetCreator struct{}

func (it *newSimpleHashsetCreator) Cap(capacity int) *SimpleHashset {
	newMap := make(map[string]bool, capacity)

	return &SimpleHashset{items: newMap}
}

func (it *newSimpleHashsetCreator) Empty() *SimpleHashset {
	return it.Cap(0)
}

func (it *newSimpleHashsetCreator) Strings(items ...string) *SimpleHashset {
	newMap := make(map[string]bool, len(items))

	for _, item := range items {
		newMap[item] = true
	}

	return &SimpleHashset{
		items: newMap,
	}
}
