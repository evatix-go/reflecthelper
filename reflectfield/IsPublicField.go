package reflectfield

import "reflect"

func IsPublicField(field reflect.StructField) bool {
	return field.PkgPath != "" // empty for public field
}
