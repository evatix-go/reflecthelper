package reflectfield

import (
	"errors"
	"reflect"
)

type MethodWrapper struct {
	MethodReflectValue reflect.Value
	Method             reflect.Method
	Name               string
	Index              int
	IsValid            bool
}

func (it *MethodWrapper) IsInvalid() bool {
	return it == nil || !it.IsValid
}

func (it *MethodWrapper) InvokeMethodNoArgs(methodName string) []reflect.Value {
	if it.IsInvalid() {
		panic("method not found!")
	}

	return it.
		MethodReflectValue.
		MethodByName(methodName).
		Call([]reflect.Value{})
}

func (it *MethodWrapper) InvokeMethodDirectly(
	args ...interface{},
) (returnedValues []reflect.Value, err error) {
	if it.IsInvalid() {
		return nil, errors.New("field is invalid")
	}

	argsReflectValues := ArgsReflectValues(
		args)

	values := it.MethodReflectValue.Call(argsReflectValues)

	return values, nil
}

func (it *MethodWrapper) InvokeMethodDirectlyVoid(
	args ...interface{},
) error {
	if it.IsInvalid() {
		return errors.New("field is invalid")
	}

	argsReflectValues := ArgsReflectValues(
		args)

	it.MethodReflectValue.Call(argsReflectValues)

	return nil
}

func (it *MethodWrapper) InvokeVoidMethod(
	args ...interface{},
) {
	if it.IsInvalid() {
		panic("method not found!")
	}

	argsReflectValues := ArgsReflectValues(args)
	it.
		MethodReflectValue.
		Call(argsReflectValues)
}

func (it *MethodWrapper) InvokeMethod(
	args ...interface{},
) []reflect.Value {
	if it.IsInvalid() {
		panic("method not found!")
	}

	argsReflectValues := ArgsReflectValues(args)

	return it.
		MethodReflectValue.
		Call(argsReflectValues)
}

func (it *MethodWrapper) InvokeMethodRegularValues(
	args ...interface{},
) []interface{} {
	returnedValues := it.InvokeMethod(
		args...,
	)

	return ReflectValuesToInterfaces(
		returnedValues)
}
