package reflectfield

type (
	SimpleFilterFunc                     func(wrapper *Wrapper) (isTake bool)
	SimpleSkipFilterFunc                 func(wrapper *Wrapper) (isSkip bool)
	WrapperToStringProcessorFunc         func(index int, wrapper *Wrapper) string
	WrapperToValueInterfaceProcessorFunc func(index int, wrapper *Wrapper) interface{}
	FilterFunc                           func(
		index int,
		wrapper *Wrapper,
	) (isBreak, isTake bool)
)
