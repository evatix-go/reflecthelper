package reflectfield

type emptyCreator struct{}

func (it emptyCreator) SimpleHashset() *SimpleHashset {
	return &SimpleHashset{
		items: map[string]bool{},
	}
}
