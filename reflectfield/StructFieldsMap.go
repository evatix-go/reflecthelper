package reflectfield

import (
	"errors"
	"reflect"
	"strings"
	"sync"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
	"gitlab.com/evatix-go/reflecthelper/internal/errmsg"
	"gitlab.com/evatix-go/reflecthelper/reflecterrs"
	"gitlab.com/evatix-go/reflecthelper/reflectkind"
)

// StructFieldsMap
//
// TranspiledPublicNames represents transpiled public names,
// for private field also the TranspiledPublicNames will be available here.
//
// TranspiledPrivateNames represents transpiled private names,
// for public field also the TranspiledPrivateNames will be available here.
//
// Warning:
// Field names will be overwritten if they only differ by the first char
//  Example:
//  type S struct {
//      a int
//      A string
//      Foo byte
//      foo string
//  }
//
//
//  TranspiledPrivateNames will only contain "a", "foo"
//  TranspiledPublicNames will only contain "A", "Foo"
type StructFieldsMap struct {
	RawAnyValue               interface{} // actual object which expended, usually struct
	WrappersMap               map[string]*Wrapper
	FieldsMap                 map[string]*reflect.StructField
	Wrappers                  []*Wrapper
	FieldNames                []string
	nullFields                []string
	FieldNamesMap             map[string]int
	TranspiledPublicNames     map[string]int
	TranspiledPrivateNames    map[string]int
	reflectTypesMap           map[string]reflect.Type
	Length                    int
	ReflectValue              reflect.Value
	Type                      reflect.Type
	IsValid                   bool
	Kind                      reflect.Kind
	KindEnhance               reflectkind.Enhance
	IsPointer                 bool
	IsInterface               bool
	HasAnyNull                bool // represents either the object is nil or any field is nil.
	fieldNameQueryOfFieldsMap *fieldNameQueryOfFieldsMap
}

func (it *StructFieldsMap) FieldNameQuery() *fieldNameQueryOfFieldsMap {
	if it == nil {
		return &fieldNameQueryOfFieldsMap{}
	}

	if it.fieldNameQueryOfFieldsMap != nil {
		return it.fieldNameQueryOfFieldsMap
	}

	it.fieldNameQueryOfFieldsMap = &fieldNameQueryOfFieldsMap{
		fieldsMap: it,
	}

	return it.fieldNameQueryOfFieldsMap
}

func (it *StructFieldsMap) ReflectTypesMap() map[string]reflect.Type {
	if it == nil {
		return nil
	}

	if it.reflectTypesMap != nil {
		return it.reflectTypesMap
	}

	newMap := make(map[string]reflect.Type, it.Length)

	for fieldName, structField := range it.FieldsMap {
		newMap[fieldName] = structField.Type
	}

	it.reflectTypesMap = newMap

	return newMap
}

// ExpandFieldNamesToFieldValuesMap
//
// fieldsNames : exact field names (ab should give 'ab' field, Ab gives `Ab` field)
func (it *StructFieldsMap) ExpandFieldNamesToFieldValuesMap(fieldsNames ...string) map[string]*StructFieldsMap {
	if it.IsInvalidOrEmptyLength() || len(fieldsNames) == 0 {
		return nil
	}

	newMap := make(map[string]*StructFieldsMap, len(fieldsNames))

	for _, fieldName := range fieldsNames {
		fieldWrapper, has := it.WrappersMap[fieldName]

		if !has {
			continue
		}

		newMap[fieldName] = fieldWrapper.ExpandValueToFieldsMap()
	}

	return newMap
}

func (it *StructFieldsMap) ExpandFieldValToMap(fieldName string) *StructFieldsMap {
	if it.IsInvalidOrEmptyLength() {
		return nil
	}

	fieldWrapper, has := it.WrappersMap[fieldName]

	if has {
		return fieldWrapper.ExpandValueToFieldsMap()
	}

	return nil
}

func (it *StructFieldsMap) IsInvalid() bool {
	return it == nil || !it.IsValid
}

func (it *StructFieldsMap) IsInvalidOrEmptyLength() bool {
	return it == nil || !it.IsValid || it.Length == 0
}

func (it *StructFieldsMap) IsEmpty() bool {
	return it == nil || !it.IsValid || it.Length == 0
}

func (it *StructFieldsMap) HasItems() bool {
	return it != nil && it.IsValid && it.Length > 0
}

func (it *StructFieldsMap) HasAnyItem() bool {
	return it != nil && it.IsValid && it.Length > 0
}

func (it *StructFieldsMap) HasAnyField() bool {
	return it != nil && it.IsValid && it.Length > 0
}

func (it *StructFieldsMap) HasField(fieldName string) bool {
	if it.IsInvalidOrEmptyLength() {
		return false
	}

	_, has := it.FieldNamesMap[fieldName]

	return has
}

func (it *StructFieldsMap) HasFields(fieldNames ...string) bool {
	if it.IsInvalidOrEmptyLength() || len(fieldNames) == 0 {
		return false
	}

	for _, name := range fieldNames {
		_, has := it.FieldNamesMap[name]

		if !has {
			return false
		}
	}

	return true
}

func (it *StructFieldsMap) HasFieldsByPublicNames(publicNames ...string) bool {
	if it.IsInvalidOrEmptyLength() || len(publicNames) == 0 {
		return false
	}

	for _, name := range publicNames {
		_, has := it.TranspiledPublicNames[name]

		if !has {
			return false
		}
	}

	return true
}

func (it *StructFieldsMap) HasFieldsByPrivateNames(privateNames ...string) bool {
	if it.IsInvalidOrEmptyLength() || len(privateNames) == 0 {
		return false
	}

	for _, name := range privateNames {
		_, has := it.TranspiledPrivateNames[name]

		if !has {
			return false
		}
	}

	return true
}

func (it *StructFieldsMap) AllMethodsMap() map[string]reflect.Method {
	if it.IsInvalid() {
		return nil
	}

	methodLength := it.ReflectValue.NumMethod()
	methodsMap := make(
		map[string]reflect.Method,
		methodLength)

	for i := 0; i < methodLength; i++ {
		method := it.Type.Method(i)

		methodsMap[method.Name] = method
	}

	return methodsMap
}

func (it *StructFieldsMap) AllMethodsWrappersMap() map[string]*MethodWrapper {
	if it.IsInvalid() {
		return nil
	}

	methodLength := it.ReflectValue.NumMethod()

	methodsMap := make(
		map[string]*MethodWrapper,
		methodLength)

	methodWrapperCreator := New.MethodWrapper.Create

	for i := 0; i < methodLength; i++ {
		method := it.Type.Method(i)
		name := method.Name
		methodsMap[name] = methodWrapperCreator(
			method)
	}

	return methodsMap
}

func (it *StructFieldsMap) InvokeMethodNoArgs(methodName string) []reflect.Value {
	if it.IsInvalidOrEmptyLength() {
		panic("method not found!")
	}

	return it.
		ReflectValue.
		MethodByName(methodName).
		Call([]reflect.Value{})
}

func (it *StructFieldsMap) InvokeMethodDirectly(
	args ...interface{},
) (returnedValues []reflect.Value, err error) {
	if it.IsInvalid() {
		return nil, errors.New("field is invalid")
	}

	argsReflectValues := ArgsReflectValues(
		args)

	values := it.ReflectValue.Call(argsReflectValues)

	return values, nil
}

func (it *StructFieldsMap) InvokeMethodDirectlyVoid(
	args ...interface{},
) error {
	if it.IsInvalid() {
		return errors.New("field is invalid")
	}

	argsReflectValues := ArgsReflectValues(
		args)

	it.ReflectValue.Call(argsReflectValues)

	return nil
}

func (it *StructFieldsMap) InvokeVoidMethod(
	methodName string,
	args ...interface{},
) {
	if it.IsInvalidOrEmptyLength() {
		panic("method not found!")
	}

	argsReflectValues := ArgsReflectValues(args)
	it.
		ReflectValue.
		MethodByName(methodName).
		Call(argsReflectValues)
}

func (it *StructFieldsMap) InvokeMethod(
	methodName string,
	args ...interface{},
) []reflect.Value {
	if it.IsInvalidOrEmptyLength() {
		panic("method not found!")
	}

	argsReflectValues := ArgsReflectValues(args)

	return it.
		ReflectValue.
		MethodByName(methodName).
		Call(argsReflectValues)
}

func (it *StructFieldsMap) InvokeMethodRegularValues(
	methodName string,
	args ...interface{},
) []interface{} {
	returnedValues := it.InvokeMethod(
		methodName,
		args...,
	)

	return ReflectValuesToInterfaces(
		returnedValues)
}

func (it *StructFieldsMap) AllValuesInterfaceMap() map[string]interface{} {
	if it.IsInvalidOrEmptyLength() {
		return nil
	}

	newMap := make(
		map[string]interface{},
		it.Length)

	for fieldName, wrapper := range it.WrappersMap {
		newMap[fieldName] = wrapper.PublicValue()
	}

	return newMap
}

func (it *StructFieldsMap) TypeByName(
	fieldName string,
) reflect.Type {
	if it.IsInvalidOrEmptyLength() || fieldName == "" {
		return nil
	}

	wrapper, has := it.WrappersMap[fieldName]

	if !has {
		return nil
	}

	return wrapper.Type
}

func (it *StructFieldsMap) IsType(
	fieldName string,
	expectedType reflect.Type,
) bool {
	reflectType := it.TypeByName(fieldName)

	return reflectType == expectedType
}

func (it *StructFieldsMap) AllPublicValuesInterfaceMap() map[string]interface{} {
	if it.IsInvalidOrEmptyLength() {
		return nil
	}

	newMap := make(
		map[string]interface{},
		it.Length)

	for fieldName, wrapper := range it.WrappersMap {
		if wrapper.IsPrivateField() {
			continue
		}

		newMap[fieldName] = wrapper.PublicValue()
	}

	return newMap
}

func (it *StructFieldsMap) AllPrivateValuesInterfaceMap() map[string]interface{} {
	if it.IsInvalidOrEmptyLength() {
		return nil
	}

	newMap := make(
		map[string]interface{},
		it.Length)

	for fieldName, wrapper := range it.WrappersMap {
		if wrapper.IsPublic {
			continue
		}

		newMap[fieldName] = wrapper.PrivateUnsafeValue()
	}

	return newMap
}

func (it *StructFieldsMap) FilterBy(
	filter SimpleFilterFunc,
) *StructFieldsMap {
	if it.IsInvalidOrEmptyLength() {
		return nil
	}

	newMap := make(
		map[string]*Wrapper,
		it.Length)

	for _, wrapper := range it.Wrappers {
		isTake := filter(wrapper)

		if isTake {
			newMap[wrapper.Name] = wrapper
		}
	}

	return it.newFieldsMap(newMap)
}

func (it *StructFieldsMap) SkipFilterBy(
	skipFilter SimpleSkipFilterFunc,
) *StructFieldsMap {
	if it.IsInvalidOrEmptyLength() {
		return nil
	}

	newMap := make(
		map[string]*Wrapper,
		it.Length)

	for _, wrapper := range it.Wrappers {
		isSkip := skipFilter(wrapper)

		if isSkip {
			continue
		}

		// take
		newMap[wrapper.Name] = wrapper
	}

	return it.newFieldsMap(newMap)
}

func (it *StructFieldsMap) FilterByKind(kind reflect.Kind) *StructFieldsMap {
	if it.IsInvalidOrEmptyLength() {
		return nil
	}

	newMap := make(
		map[string]*Wrapper,
		it.Length)

	for fieldName, wrapper := range it.WrappersMap {
		if wrapper.Kind == kind {
			newMap[fieldName] = wrapper
		}
	}

	return it.newFieldsMap(newMap)
}

func (it *StructFieldsMap) FilterByTypes(reflectTypes ...reflect.Type) *StructFieldsMap {
	if it.IsInvalidOrEmptyLength() {
		return nil
	}

	newMap := make(
		map[string]*Wrapper,
		it.Length)

	for fieldName, wrapper := range it.WrappersMap {
		if wrapper.IsAnyTypeOf(reflectTypes...) {
			newMap[fieldName] = wrapper
		}
	}

	return it.newFieldsMap(newMap)
}

func (it *StructFieldsMap) FilterByType(reflectType reflect.Type) *StructFieldsMap {
	if it.IsInvalidOrEmptyLength() {
		return nil
	}

	newMap := make(
		map[string]*Wrapper,
		it.Length)

	for fieldName, wrapper := range it.WrappersMap {
		if wrapper.Type == reflectType {
			newMap[fieldName] = wrapper
		}
	}

	return it.newFieldsMap(newMap)
}

func (it *StructFieldsMap) FilterByKinds(kinds ...reflect.Kind) *StructFieldsMap {
	if it.IsInvalidOrEmptyLength() || len(kinds) == 0 {
		return nil
	}

	newMap := make(
		map[string]*Wrapper,
		it.Length)

	kindsMap := make(
		map[reflect.Kind]bool,
		len(kinds))
	for _, kind := range kinds {
		kindsMap[kind] = true
	}

	for fieldName, wrapper := range it.WrappersMap {
		if kindsMap[wrapper.Kind] {
			newMap[fieldName] = wrapper
		}
	}

	return it.newFieldsMap(newMap)
}

func (it *StructFieldsMap) FilterByNames(
	fieldNames ...string,
) map[string]*Wrapper {
	if it.IsInvalidOrEmptyLength() {
		return nil
	}

	newMap := make(
		map[string]*Wrapper,
		len(fieldNames))

	for _, fieldName := range fieldNames {
		wrapper, has := newMap[fieldName]

		if has {
			newMap[fieldName] = wrapper
		}
	}

	return newMap
}

func (it *StructFieldsMap) FilterFieldNamesToFieldsMap(
	fieldNames ...string,
) *StructFieldsMap {
	if it.IsInvalidOrEmptyLength() {
		return New.StructFieldsMap.Invalid()
	}

	return it.newFieldsMap(
		it.FilterByNames(
			fieldNames...))
}

// GetWrapperByPublicName
//
// Returns nil if not invalid or empty or not found
func (it *StructFieldsMap) GetWrapperByPublicName(
	fieldName string,
) *Wrapper {
	if it.IsInvalidOrEmptyLength() {
		return nil
	}

	publicNameIndex, has := it.TranspiledPublicNames[fieldName]

	if has {
		return it.Wrappers[publicNameIndex]
	}

	return nil
}

// GetWrapperByPrivateName
//
// Returns nil if not invalid or empty or not found
func (it *StructFieldsMap) GetWrapperByPrivateName(fieldName string) *Wrapper {
	if it.IsInvalidOrEmptyLength() {
		return nil
	}

	publicNameIndex, has := it.TranspiledPrivateNames[fieldName]

	if has {
		return it.Wrappers[publicNameIndex]
	}

	return nil
}

// GetWrapper
//
// Returns nil if not invalid or empty or not found
func (it *StructFieldsMap) GetWrapper(fieldName string) *Wrapper {
	if it.IsInvalidOrEmptyLength() {
		return nil
	}

	return it.WrappersMap[fieldName]
}

func (it *StructFieldsMap) GetSimpleWrapper(fieldName string) *SimpleWrapper {
	if it.IsInvalidOrEmptyLength() {
		return nil
	}

	return it.WrappersMap[fieldName].SimpleWrapper()
}

func (it *StructFieldsMap) ConvSimpleWrappersMap() map[string]*SimpleWrapper {
	if it.IsInvalidOrEmptyLength() {
		return nil
	}

	newMap := make(
		map[string]*SimpleWrapper,
		it.Length)

	for _, wrapper := range it.Wrappers {
		newMap[wrapper.Name] =
			wrapper.SimpleWrapper()
	}

	return newMap
}

// GetWrapperUsingIndex
//
// Returns nil if not invalid or empty or not found
func (it *StructFieldsMap) GetWrapperUsingIndex(index int) *Wrapper {
	if it.IsInvalidOrEmptyLength() {
		return nil
	}

	return it.Wrappers[index]
}

func (it *StructFieldsMap) FilterFuncItems(
	processorFunc FilterFunc,
) []*Wrapper {
	length := it.Length
	wrappers := make([]*Wrapper, 0, length)

	for i := 0; i < length; i++ {
		wrapper := it.Wrappers[i]
		isBreak, isTake := processorFunc(i, wrapper)

		if isTake {
			wrappers = append(wrappers, wrapper)
		}

		if isBreak {
			break
		}
	}

	return wrappers
}

func (it *StructFieldsMap) FilterFuncMap(
	processorFunc FilterFunc,
) *StructFieldsMap {
	length := it.Length
	wrappersMap := make(map[string]*Wrapper, length)

	for i := 0; i < length; i++ {
		wrapper := it.Wrappers[i]
		isBreak, isTake := processorFunc(i, wrapper)

		if isTake {
			wrappersMap[wrapper.Name] = wrapper
		}

		if isBreak {
			break
		}
	}

	return it.newFieldsMap(wrappersMap)
}

func (it *StructFieldsMap) newFieldsMap(
	wrappersMap map[string]*Wrapper,
) *StructFieldsMap {
	return New.StructFieldsMap.UsingRawParams(
		it.RawAnyValue,
		it.Kind,
		it.ReflectValue,
		it.Type,
		wrappersMap)
}

func (it *StructFieldsMap) CollectErrorUsingProcessorFunc(
	processorFunc func(
		errorsWrappersCollector *reflecterrs.Wrapper,
		index int,
		wrapper *Wrapper,
	),
) (allCollectedError *reflecterrs.Wrapper) {
	emptyErrors := reflecterrs.Empty()

	if it.IsInvalidOrEmptyLength() {
		return emptyErrors
	}

	length := it.Length
	for i := 0; i < length; i++ {
		processorFunc(
			emptyErrors,
			i,
			it.Wrappers[i],
		)
	}

	return emptyErrors
}

// AllNullFieldsError
//
// returns nil if no nil fields
func (it *StructFieldsMap) AllNullFieldsError() error {
	if it == nil {
		return errmsg.New("StructFieldsMap is nil", reflect.TypeOf(it).Name())
	}

	if !it.HasAnyNull {
		return nil
	}

	nullFields := it.AllNullFieldsNames()
	nilFieldsNamesAsCsv := strings.Join(
		nullFields,
		consts.Comma)

	return errmsg.FieldIsNilIn.Error(
		nilFieldsNamesAsCsv)
}

func (it *StructFieldsMap) PanicOnNullFields() {
	if it == nil || !it.HasAnyNull {
		return
	}

	err := it.AllNullFieldsError()

	panic(err)
}

func (it *StructFieldsMap) NullFieldErr(name string) error {
	if it == nil || !it.HasAnyNull {
		return nil
	}

	wrapper := it.WrappersMap[name]

	if wrapper != nil && wrapper.IsAnyNull() {
		return errmsg.FieldIsNilIn.Error(
			name)
	}

	return nil
}

func (it *StructFieldsMap) PanicOnFieldNull(name string) {
	if !it.HasAnyNull {
		return
	}

	err := it.NullFieldErr(name)

	if err != nil {
		panic(err)
	}
}

// AllNullFieldsNames
//
// Returns null fields names
func (it *StructFieldsMap) AllNullFieldsNames() []string {
	if it == nil {
		return nil
	}

	if it.nullFields != nil {
		return it.nullFields
	}

	if !it.HasAnyNull {
		it.nullFields = []string{}

		return it.nullFields
	}

	fields := make([]string, 0, it.Length)

	for _, wrapper := range it.Wrappers {
		if wrapper.IsAnyNull() {
			fields = append(fields, wrapper.Name)
		}
	}

	return fields
}

func (it *StructFieldsMap) AllFunctions() []*Wrapper {
	if it == nil {
		return nil
	}

	fields := make([]*Wrapper, 0, it.Length)

	for _, wrapper := range it.Wrappers {
		if wrapper.IsFunc() {
			fields = append(fields, wrapper)
		}
	}

	return fields
}

func (it *StructFieldsMap) AllFunctionsFieldNames() []string {
	if it == nil {
		return nil
	}

	fields := make([]string, 0, it.Length)

	for _, wrapper := range it.Wrappers {
		if wrapper.IsFunc() {
			fields = append(fields, wrapper.Name)
		}
	}

	return fields
}

func (it *StructFieldsMap) ValStringUsingFunc(
	processorFunc WrapperToStringProcessorFunc,
) []string {
	length := it.Length
	list := make([]string, length)
	wg := &sync.WaitGroup{}
	wg.Add(length)
	internalProcessorFunc := func(index int) {
		list[index] = processorFunc(
			index,
			it.Wrappers[index])

		wg.Done()
	}

	for i := 0; i < length; i++ {
		go internalProcessorFunc(i)
	}

	wg.Wait()

	return list
}

func (it *StructFieldsMap) ValInterfacesUsingFunc(
	processorFunc WrapperToValueInterfaceProcessorFunc,
) []interface{} {
	length := it.Length
	list := make([]interface{}, length)
	wg := &sync.WaitGroup{}
	wg.Add(length)
	internalProcessorFunc := func(index int) {
		list[index] = processorFunc(
			index,
			it.Wrappers[index])

		wg.Done()
	}

	for i := 0; i < length; i++ {
		go internalProcessorFunc(i)
	}

	wg.Wait()

	return list
}

func (it *StructFieldsMap) ValInterfacesMapUsingFunc(
	processorFunc WrapperToValueInterfaceProcessorFunc,
) map[string]interface{} {
	if it.IsInvalidOrEmptyLength() {
		return nil
	}

	valuesInterfaces := it.ValInterfacesUsingFunc(processorFunc)

	if len(valuesInterfaces) != it.Length {
		// not possible
		panic(errmsg.New("length mismatch in values collection"))
	}

	newMap := make(map[string]interface{}, it.Length)

	for i, valuesInterface := range valuesInterfaces {
		newMap[it.Wrappers[i].Name] = valuesInterface
	}

	return newMap
}

func (it *StructFieldsMap) ValInterfacesMap() map[string]interface{} {
	if it.IsInvalidOrEmptyLength() {
		return nil
	}

	return it.ValInterfacesMapUsingFunc(func(index int, wrapper *Wrapper) interface{} {
		return wrapper.ValueInterfaceOrError()
	})
}

// ValInterfacesOrError
//
// Calls Wrapper.ValueInterfaceOrError, which returns error on fail.
// Best to check if error type first.
// However, it is rarely the case to get an error.
//
// Only possible case for getting an error is pointer reduction.
func (it *StructFieldsMap) ValInterfacesOrError() []interface{} {
	length := it.Length
	list := make([]interface{}, length)
	wg := sync.WaitGroup{}

	processorFunc := func(index int, wrapper *Wrapper) {
		list[index] = wrapper.ValueInterfaceOrError()

		wg.Done()
	}

	wg.Add(length)

	for i := 0; i < length; i++ {
		go processorFunc(i, it.Wrappers[i])
	}

	wg.Wait()

	return list
}

// ValInterfacesOrErrorMap
//
// Calls Wrapper.ValueInterfaceOrError, which returns error on fail.
// Best to check if error type first.
// However, it is rarely the case to get an error.
//
// Only possible case for getting an error is pointer reduction.
func (it *StructFieldsMap) ValInterfacesOrErrorMap() map[string]interface{} {
	length := it.Length
	newMap := make(map[string]interface{}, length)

	for key, wrapper := range it.WrappersMap {
		newMap[key] = wrapper.ValueInterfaceOrError()
	}

	return newMap
}

func (it *StructFieldsMap) Dispose() {
	if it.IsInvalidOrEmptyLength() {
		return
	}

	it.WrappersMap = map[string]*Wrapper{}
	it.FieldsMap = map[string]*reflect.StructField{}
	it.Wrappers = []*Wrapper{}
	it.FieldNames = []string{}
	it.nullFields = nil
	it.FieldNamesMap = map[string]int{}
	it.TranspiledPublicNames = map[string]int{}
	it.TranspiledPrivateNames = map[string]int{}
	it.reflectTypesMap = map[string]reflect.Type{}
	it.Length = 0
	it.RawAnyValue = nil
}

func (it *StructFieldsMap) AllFieldsExpandToFieldsMap() map[string]*StructFieldsMap {
	if it.IsInvalidOrEmptyLength() {
		return nil
	}

	return it.ExpandFieldsNamesToFieldsMap(
		it.FieldNames...)
}

func (it *StructFieldsMap) ExpandFieldsNamesToFieldsMap(
	fieldNames ...string,
) map[string]*StructFieldsMap {
	if it.IsInvalidOrEmptyLength() || len(fieldNames) == 0 {
		return nil
	}

	items := make(
		[]*StructFieldsMap,
		len(fieldNames))

	wg := sync.WaitGroup{}
	wg.Add(len(fieldNames))
	indexProcessor := func(index int, fieldName string) {
		items[index] = it.ExpandToFieldsMapByName(fieldName)

		wg.Done()
	}

	for i, fieldName := range fieldNames {
		go indexProcessor(i, fieldName)
	}

	wg.Wait()

	newMap := make(
		map[string]*StructFieldsMap,
		len(fieldNames))

	for index := range items {
		if items[index].IsInvalidOrEmptyLength() {
			continue
		}

		newMap[fieldNames[index]] = items[index]
	}

	return newMap
}

func (it *StructFieldsMap) ExpandToFieldsMapByName(fieldName string) *StructFieldsMap {
	if it.IsInvalidOrEmptyLength() || fieldName == "" {
		return nil
	}

	wrapper, has := it.WrappersMap[fieldName]

	if !has {
		return nil
	}

	return wrapper.ExpandValueToFieldsMap()
}
