package reflectfield

import (
	"reflect"
	"sync"
)

type newSimpleWrapperCreator struct{}

func (it *newSimpleWrapperCreator) Invalid() *SimpleWrapper {
	return &SimpleWrapper{}
}

func (it *newSimpleWrapperCreator) AnyItemToMap(
	anyItem interface{},
) map[string]*SimpleWrapper {
	rv := reflect.ValueOf(anyItem)

	return it.ReflectValueToMap(rv)
}

func (it *newSimpleWrapperCreator) ReflectValueToMap(
	structValue reflect.Value,
) map[string]*SimpleWrapper {
	structType := structValue.Type()
	structNumFields := structType.NumField()

	simpleWrappers := make([]*SimpleWrapper, structNumFields)
	fieldToValueMap := make(map[string]*SimpleWrapper, structNumFields)
	newSimpleWrapperFunc := New.SimpleWrapper.UsingAllParams
	wg := sync.WaitGroup{}
	wg.Add(structNumFields)

	processor := func(index int) {
		fieldType := structType.Field(index)
		fieldValue := structValue.Field(index)
		simpleWrapper := newSimpleWrapperFunc(
			index,
			fieldType,
			fieldValue)

		simpleWrappers[index] = simpleWrapper
		wg.Done()
	}

	for i := 0; i < structNumFields; i++ {
		go processor(i)
	}

	wg.Wait()

	for _, wrapper := range simpleWrappers {
		fieldToValueMap[wrapper.Name] = wrapper
	}

	return fieldToValueMap
}

func (it *newSimpleWrapperCreator) UsingAllParams(
	fieldIndex int,
	fieldType reflect.StructField,
	reflectValue reflect.Value,
) *SimpleWrapper {
	kind := reflectValue.Kind()
	fieldName := fieldType.Name

	return &SimpleWrapper{
		FieldIndex:   fieldIndex,
		Name:         fieldName,
		Field:        fieldType,
		ReflectValue: reflectValue,
		Kind:         kind,
		IsValid:      true,
	}
}
