package reflectfield

import (
	"errors"
	"fmt"
	"reflect"
	"unsafe"

	"gitlab.com/evatix-go/reflecthelper/internal/consts"
	"gitlab.com/evatix-go/reflecthelper/internal/defaulterr"
	"gitlab.com/evatix-go/reflecthelper/internal/errmsg"
	"gitlab.com/evatix-go/reflecthelper/isreflect"
	"gitlab.com/evatix-go/reflecthelper/reflecttype"
)

type Wrapper struct {
	Field                         *reflect.StructField
	Value                         reflect.Value
	Tag                           reflect.StructTag
	Type                          reflect.Type
	Name                          string
	PublicName                    string
	PrivateName                   string
	IsPublic                      bool
	IsPointer                     bool
	IsValueAddressable, HasSetter bool
	attributes                    *Attributes
	Kind                          reflect.Kind
	PkgPath                       string
	Index                         int
	fieldsMap                     *StructFieldsMap
	fieldNames                    []string
}

func (it *Wrapper) ExpandValueToFieldsMap() *StructFieldsMap {
	if it == nil {
		return nil
	}

	if it.fieldsMap != nil {
		return it.fieldsMap
	}

	return New.
		StructFieldsMap.
		UsingReflectValDefault(
			it.Value.Interface(),
			it.Value)
}

func (it *Wrapper) Attributes() *Attributes {
	if it == nil {
		return nil
	}

	if it.attributes != nil {
		return it.attributes
	}

	it.attributes = newAttrs(it.Kind)

	return it.attributes
}

func (it *Wrapper) TypeNameString() string {
	if it.IsInvalid() {
		return ""
	}

	return it.Type.String()
}

func (it *Wrapper) Size() uintptr {
	if it.IsInvalid() {
		return 0
	}

	return it.Type.Size()
}

func (it *Wrapper) UintPointer() uintptr {
	if it == nil {
		return 0
	}

	return it.Value.Pointer()
}

func (it *Wrapper) Float() float64 {
	return it.Value.Float()
}

func (it *Wrapper) SafeFloat(defaultVal float64) float64 {
	if it.IsAnyKindOf(reflect.Float32, reflect.Float64) {
		return it.Value.Float()
	}

	return defaultVal
}

func (it *Wrapper) SafeFloatDefault() float64 {
	if it.IsAnyKindOf(reflect.Float32, reflect.Float64) {
		return it.Value.Float()
	}

	return 0
}

func (it *Wrapper) Bool() bool {
	return it.Value.Bool()
}

func (it *Wrapper) SafeBool() bool {
	if it.IsKind(reflect.Bool) {
		return it.Value.Bool()
	}

	return false
}

func (it *Wrapper) SafeInteger64(defaultValue int64) int64 {
	if it.IsAnyKindOf(allIntegerKinds...) {
		return it.Value.Int()
	}

	return defaultValue
}

func (it *Wrapper) SafeInteger(defaultValue int) int {
	if it.IsAnyKindOf(reflect.Int) {
		return int(it.Value.Int())
	}

	return defaultValue
}

func (it *Wrapper) SafeIntegerDefault() int {
	if it.IsAnyKindOf(reflect.Int) {
		return int(it.Value.Int())
	}

	return 0
}

func (it *Wrapper) SafeString(defaultValue string) string {
	if it.IsString() {
		return it.Value.String()
	}

	return defaultValue
}

func (it *Wrapper) SafeStringDefault() string {
	if it.IsKind(reflect.String) {
		return it.Value.String()
	}

	return emptyString
}

func (it *Wrapper) IsAnyNull() bool {
	return it == nil ||
		it.Field == nil ||
		isreflect.Null(it.Value)
}

func (it *Wrapper) PointerReference() interface{} {
	if it.IsInvalid() {
		return nil
	}

	if it.IsPublic && it.IsPointer {
		return it.Value.Interface()
	} else if it.IsPublic && !it.IsPointer {
		value := it.Value.Interface()

		return reflect.ValueOf(&value)
	}

	return it.UnsafePointerReflectValue().Interface()
}

func (it *Wrapper) IndexOfTypes(reflectTypes ...reflect.Type) int {
	if it == nil {
		return -1
	}

	for i, reflectType := range reflectTypes {
		if it.Type == reflectType {
			return i
		}
	}

	return -1
}

func (it *Wrapper) IsPrimitiveType() bool {
	if it.IsInvalid() {
		return false
	}

	return isreflect.Primitive(it.Kind)
}

func (it *Wrapper) IsType(reflectType reflect.Type) bool {
	if it.IsInvalid() {
		return false
	}

	return it.Type == reflectType
}

func (it *Wrapper) AllFieldsNames() []string {
	if it.IsInvalid() {
		return nil
	}

	if it.fieldNames != nil {
		return it.fieldNames
	}

	length := it.FieldsLength()
	fieldsNames := make([]string, length)
	processor := func(index int) {
		fieldsNames[index] = it.Value.Field(index).Type().Name()
	}

	for i := 0; i < length; i++ {
		go processor(i)
	}

	it.fieldNames = fieldsNames

	return fieldsNames
}

func (it *Wrapper) IsAnyTypeOf(reflectTypes ...reflect.Type) bool {
	if it == nil {
		return false
	}

	for _, reflectType := range reflectTypes {
		if it.Type == reflectType {
			return true
		}
	}

	return false
}

func (it *Wrapper) IsValid() bool {
	return it != nil && it.Value.IsValid()
}

func (it *Wrapper) Length() int {
	if it == nil {
		return 0
	}

	return it.Value.Len()
}

func (it *Wrapper) Capacity() int {
	if it == nil {
		return 0
	}

	return it.Value.Cap()
}

func (it Wrapper) FieldReflectValue(fieldName string) reflect.Value {
	return it.Value.FieldByName(fieldName)
}

func (it *Wrapper) HasAnyFields() bool {
	if it.IsInvalid() {
		return false
	}

	return it.IsStruct() && it.FieldsLength() > 0
}

func (it *Wrapper) FieldsLength() int {
	if it == nil {
		return 0
	}

	return it.Value.NumField()
}

func (it *Wrapper) At(index int) reflect.Value {
	return it.Value.Index(index)
}

func (it *Wrapper) FieldsProcessor(
	processor func(index int, fieldReflectValue reflect.Value) error,
) error {
	if it.IsInvalid() {
		return errmsg.New("not valid wrapper")
	}

	length := it.FieldsLength()

	for i := 0; i < length; i++ {
		err := processor(i, it.Value.Field(i))

		if err != nil {
			return nil
		}
	}

	return nil
}

func (it *Wrapper) SliceProcessor(
	processor func(index int, sliceIndexedElementReflectValue reflect.Value) error,
) error {
	if it.IsInvalid() {
		return errmsg.New("not valid wrapper")
	}

	return it.sliceProcessorInternal(
		it.ElementValue(),
		processor)
}

func (it *Wrapper) sliceProcessorInternal(
	rv reflect.Value,
	processor func(index int, sliceIndexedElementReflectValue reflect.Value) error,
) error {
	k := rv.Kind()
	isSliceArrayOrMap := k == reflect.Slice ||
		k == reflect.Array ||
		k == reflect.Map
	isNotSliceOrMap := !isSliceArrayOrMap

	if isNotSliceOrMap {
		return errmsg.ExpectedButFoundString(
			"Slice, Array or Map",
			k.String())
	}

	length := rv.Len()

	if length == 0 {
		return nil
	}

	for i := 0; i < length; i++ {
		err := processor(i, rv.Index(i))

		if err != nil {
			return err
		}
	}

	return nil
}

func (it *Wrapper) IsInvalid() bool {
	return it == nil || !it.Value.IsValid()
}

func (it *Wrapper) IsPrivateField() bool {
	return it != nil && !it.IsPublic
}

func (it *Wrapper) IsNonPointer() bool {
	return it != nil && !it.IsPointer
}

func (it *Wrapper) IsStruct() bool {
	return it != nil && it.Kind == reflect.Struct
}

func (it *Wrapper) IsSlice() bool {
	return it != nil && it.Kind == reflect.Slice
}

func (it *Wrapper) IsArrayOrSlice() bool {
	return it != nil &&
		it.Kind == reflect.Slice ||
		it.Kind == reflect.Array
}

func (it *Wrapper) IsArrayOrSliceOrMap() bool {
	return it != nil &&
		it.Kind == reflect.Slice ||
		it.Kind == reflect.Array ||
		it.Kind == reflect.Map
}

func (it *Wrapper) IsMap() bool {
	return it != nil && it.Kind == reflect.Map
}

func (it *Wrapper) IsFunc() bool {
	return it != nil && it.Kind == reflect.Func
}

func (it *Wrapper) IsString() bool {
	return it != nil && it.Kind == reflect.String
}

func (it *Wrapper) IsInteger() bool {
	return it != nil && it.Kind == reflect.Int
}

func (it *Wrapper) IsAnyIntegerType() bool {
	return it.IsAnyKindOf(allIntegerKinds...)
}

func (it *Wrapper) IsInterface() bool {
	return it != nil && it.Kind == reflect.Interface
}

func (it *Wrapper) AllMethodsMap() map[string]reflect.Method {
	if it.IsInvalid() {
		return nil
	}

	methodLength := it.Value.NumMethod()

	methodsMap := make(
		map[string]reflect.Method,
		methodLength)

	for i := 0; i < methodLength; i++ {
		method := it.Type.Method(i)

		methodsMap[method.Name] = method
	}

	return methodsMap
}

func (it *Wrapper) AllMethodsWrappersMap() map[string]*MethodWrapper {
	if it.IsInvalid() {
		return nil
	}

	methodLength := it.Value.NumMethod()

	methodsMap := make(
		map[string]*MethodWrapper,
		methodLength)

	methodWrapperCreator := New.MethodWrapper.Create

	for i := 0; i < methodLength; i++ {
		method := it.Type.Method(i)
		name := method.Name
		methodsMap[name] = methodWrapperCreator(
			method)
	}

	return methodsMap
}

func (it *Wrapper) InvokeMethodNoArgs(methodName string) []reflect.Value {
	if it.IsInvalid() {
		panic("method not found!")
	}

	return it.
		Value.
		MethodByName(methodName).
		Call([]reflect.Value{})
}

func (it *Wrapper) InvokeMethodDirectly(
	args ...interface{},
) (returnedValues []reflect.Value, err error) {
	if it.IsInvalid() {
		return nil, errors.New("field is invalid")
	}

	argsReflectValues := ArgsReflectValues(
		args)

	values := it.Value.Call(argsReflectValues)

	return values, nil
}

func (it *Wrapper) InvokeMethodDirectlyVoid(
	args ...interface{},
) error {
	if it.IsInvalid() {
		return errors.New("field is invalid")
	}

	argsReflectValues := ArgsReflectValues(
		args)

	it.Value.Call(argsReflectValues)

	return nil
}

func (it *Wrapper) InvokeVoidMethod(
	methodName string,
	args ...interface{},
) {
	if it.IsInvalid() {
		panic("method not found!")
	}

	argsReflectValues := ArgsReflectValues(args)
	it.
		Value.
		MethodByName(methodName).
		Call(argsReflectValues)
}

func (it *Wrapper) InvokeMethod(
	methodName string,
	args ...interface{},
) []reflect.Value {
	if it.IsInvalid() {
		panic("method not found!")
	}

	argsReflectValues := ArgsReflectValues(args)

	return it.
		Value.
		MethodByName(methodName).
		Call(argsReflectValues)
}

func (it *Wrapper) InvokeMethodRegularValues(
	methodName string,
	args ...interface{},
) []interface{} {
	returnedValues := it.InvokeMethod(
		methodName,
		args...,
	)

	return ReflectValuesToInterfaces(
		returnedValues)
}

func (it *Wrapper) MethodWrapper(
	methodName string,
) *MethodWrapper {
	method, isFound := it.
		Type.
		MethodByName(methodName)

	if !isFound {
		return New.
			MethodWrapper.
			Invalid()
	}

	return New.
		MethodWrapper.
		Create(
			method)
}

// IndexOfAnyKind
//
// -1 represent not found any
func (it *Wrapper) IndexOfAnyKind(kinds ...reflect.Kind) (index int) {
	if it == nil {
		return -1
	}

	for i, kind := range kinds {
		if it.Kind == kind {
			return i
		}
	}

	return -1
}

// IsAnyKindOf
//
// returns true if kind is any
func (it *Wrapper) IsAnyKindOf(kinds ...reflect.Kind) bool {
	if it == nil || len(kinds) == 0 {
		return false
	}

	for _, kind := range kinds {
		if it.Kind == kind {
			return true
		}
	}

	return false
}

// IsNotAnyKindOf
//
// returns true if none of the kinds matches
func (it *Wrapper) IsNotAnyKindOf(kinds ...reflect.Kind) bool {
	if it == nil || len(kinds) == 0 {
		return true
	}

	for _, kind := range kinds {
		if it.Kind == kind {
			return false
		}
	}

	return true
}

func (it *Wrapper) IsKind(kind reflect.Kind) bool {
	return it != nil && it.Kind == kind
}

func (it *Wrapper) IsNotKind(notKind reflect.Kind) bool {
	return it == nil || it.Kind != notKind
}

// ElementValue
//
// returns the value that the interface v contains
// or that the pointer v points to.
// It panics if v's Kind is not Interface or Ptr.
// It returns the zero Value if v is nil.
func (it Wrapper) ElementValue() reflect.Value {
	if it.IsPointer || it.IsInterface() {
		return it.Value.Elem()
	}

	return it.Value
}

// PublicValue
//
// If pointer then reduce once or else just interface
func (it *Wrapper) PublicValue() interface{} {
	if it == nil || it.IsPrivateField() {
		return nil
	}

	if it.IsPointer || it.IsInterface() {
		return it.Value.Elem().Interface()
	}

	return it.Value.Interface()
}

// ValueInterfaceForce
//
// Expensive operation : Get value of private or public field value
//
// Reduces pointers recursively.
// Can panic if it is invalid type.
//
// Returns nil if nil
func (it *Wrapper) ValueInterfaceForce() (interface{}, error) {
	if it.IsAnyNull() {
		return nil, nil
	}

	if it.IsNonPointer() && it.IsPublic {
		return it.Value.Interface(), nil
	}

	if it.IsPrivateField() {
		return it.PrivateUnsafeValue(), nil
	}

	// pointer
	reducedValue, err := it.PointerReduceToValue()

	if err != nil {
		return nil, err
	}

	return reducedValue.Interface(), nil
}

// ValueInterfaceOrError
//
// First check if the result is error type if not then proceed further.
// which returns error on fail.
// Best to check if error type first.
// However, it is rarely the case to get an error.
//
// Only possible case for getting an error is pointer reduction issue.
func (it *Wrapper) ValueInterfaceOrError() interface{} {
	val, err := it.ValueInterfaceForce()

	if err != nil {
		return err
	}

	return val
}

// PrivateUnsafeValue
//
// Extremely slow, works for both private and public
//
// Usages unsafe pointer and can panic without valid data.
func (it *Wrapper) PrivateUnsafeValue() interface{} {
	if it == nil {
		return nil
	}

	newAt := reflect.NewAt(
		it.Field.Type,
		unsafe.Pointer(it.Value.UnsafeAddr()))

	elementOfPointer := newAt.Elem()

	return elementOfPointer.Interface()
}

// PublicValueNonPointer
//
//  returns it.Value.Interface()
//  returns nil on invalid or private value.
func (it *Wrapper) PublicValueNonPointer() interface{} {
	if it == nil || !it.IsPublic {
		return nil
	}

	return it.Value.Interface()
}

// ValueInterface
//
// it.Value.Interface()
func (it *Wrapper) ValueInterface() interface{} {
	if it.IsInvalid() {
		return nil
	}

	return it.Value.Interface()
}

func (it *Wrapper) ValForceString() string {
	val, err := it.ValueInterfaceForce()

	if err != nil {
		return err.Error()
	}

	return fmt.Sprintf(consts.SprintFValueFormat, val)
}

// SafeValString
//
// Empty string if not string value
func (it *Wrapper) SafeValString() string {
	if it.Kind != reflect.String {
		return ""
	}

	return it.Value.String()
}

func (it *Wrapper) SafeValInt(defaultVal int) int {
	if it.Kind != reflect.Int {
		return defaultVal
	}

	return int(it.Value.Int())
}

func (it *Wrapper) SafeValInt64(defaultVal int64) int64 {
	if it.Kind != reflect.Int {
		return defaultVal
	}

	return it.Value.Int()
}

// SafeValUint64
//
// Expensive
func (it *Wrapper) SafeValUint64(defaultVal uint64) uint64 {
	if !it.Attributes().IsUnsignedInteger {
		return defaultVal
	}

	return it.Value.Uint()
}

func (it *Wrapper) SafeValFloat64(defaultVal float64) float64 {
	if !it.Attributes().IsFloatingPoint {
		return defaultVal
	}

	return it.Value.Float()
}

func (it *Wrapper) PointerReduceToValue() (reflect.Value, error) {
	structValue := it.Value
	structValueKind := it.Kind

	for structValueKind == reflect.Ptr || structValueKind == reflect.Interface {
		structValue = structValue.Elem()
		// mutating dangerous code
		structValueKind = structValue.Kind()
	}

	if structValueKind == reflect.Invalid {
		return reflecttype.InvalidValue, defaulterr.InvalidTypeCannotReduce
	}

	return structValue, nil
}

func (it *Wrapper) PointerReduceToValueUsingMax(maxTries int) (reflect.Value, error) {
	if maxTries == -1 {
		return it.PointerReduceToValue()
	}

	structValue := it.Value
	structValueKind := it.Kind

	for structValueKind == reflect.Ptr || structValueKind == reflect.Interface {
		structValue = structValue.Elem()
		// mutating dangerous code
		structValueKind = structValue.Kind()

		if maxTries <= 0 {
			break
		}

		maxTries--
	}

	if structValueKind == reflect.Invalid {
		return reflecttype.InvalidValue, defaulterr.InvalidTypeCannotReduce
	}

	return structValue, nil
}

func (it *Wrapper) UnsafePointerReflectValue() reflect.Value {
	if it == nil {
		return reflect.Value{}
	}

	return reflect.NewAt(
		it.Field.Type,
		unsafe.Pointer(it.Value.UnsafeAddr()))
}

func (it *Wrapper) SimpleWrapper() *SimpleWrapper {
	if it.IsInvalid() {
		return New.SimpleWrapper.Invalid()
	}

	return New.SimpleWrapper.UsingAllParams(
		it.Index,
		*it.Field,
		it.Value)
}
