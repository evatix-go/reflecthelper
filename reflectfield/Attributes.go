package reflectfield

import (
	"reflect"

	"gitlab.com/evatix-go/reflecthelper/reflectkind"
)

type Attributes struct {
	IsArray              bool
	IsMap                bool
	IsFunc               bool
	IsSlice              bool
	IsStruct             bool
	IsInterface          bool
	IsChan               bool
	IsInteger            bool
	IsFloatingPoint      bool
	IsUnsignedInteger    bool
	IsArrayOrSlice       bool
	IsString             bool
	IsUnsafePointer      bool
	IsPointerOrInterface bool
	KindEnhance          reflectkind.Enhance
}

func newAttrs(k reflect.Kind) *Attributes {
	enhance := reflectkind.Enhance(k)

	attrs := Attributes{
		IsArray:              k == reflect.Array,
		IsMap:                k == reflect.Map,
		IsFunc:               k == reflect.Func,
		IsSlice:              k == reflect.Slice,
		IsStruct:             k == reflect.Struct,
		IsInterface:          k == reflect.Interface,
		IsChan:               k == reflect.Chan,
		IsInteger:            enhance.IsInteger(),
		IsFloatingPoint:      enhance.IsFloatingPoint(),
		IsUnsignedInteger:    enhance.IsUnsignedInteger(),
		IsArrayOrSlice:       enhance.IsArrayOrSlice(),
		IsPointerOrInterface: enhance.IsPointerOrInterface(),
		IsString:             k == reflect.String,
		IsUnsafePointer:      k == reflect.UnsafePointer,
		KindEnhance:          enhance,
	}

	return &attrs
}
