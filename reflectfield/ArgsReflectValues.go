package reflectfield

import "reflect"

func ArgsReflectValues(args []interface{}) []reflect.Value {
	if len(args) == 0 {
		return []reflect.Value{}
	}

	list := make(
		[]reflect.Value,
		len(args))

	for i, arg := range args {
		list[i] = reflect.ValueOf(arg)
	}

	return list
}
