package reflectfield

import "reflect"

func IsAllFieldPublic(fields ...reflect.StructField) bool {
	if len(fields) == 0 {
		return false
	}

	for _, field := range fields {
		if field.PkgPath == "" {
			return false
		}
	}

	return true
}
