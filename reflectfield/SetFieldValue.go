package reflectfield

import (
	"reflect"

	"gitlab.com/evatix-go/reflecthelper/internal/errmsg"
)

type valueSetter struct{}

// WithError
//
// Reference:
//  - How to set private field value using reflect stackoverflow : https://t.ly/JlWR, https://t.ly/UO84
//      - https://stackoverflow.com/questions/6395076/using-reflect-how-do-you-set-the-value-of-a-struct-field/6396678#6396678
//  - Private field set values : https://pastebin.com/2Q9FrbzB
//  - Field set example        : https://pastebin.com/n8P6RRy0
func (it *valueSetter) WithError(
	isValidBeforeSet bool,
	rv reflect.Value,
	valueSet interface{},
) error {
	rvType := rv.Type()
	settingValueType := reflect.TypeOf(
		valueSet)

	if isValidBeforeSet && rv.Type() != settingValueType {
		return errmsg.ExpectedButFoundString(
			rvType.String(),
			settingValueType.String())
	}

	settingReflectVal := reflect.ValueOf(
		valueSet)

	if rv.CanSet() {
		rv.Set(settingReflectVal)
	}

	return errmsg.New(
		"cannot set reflect value")
}
