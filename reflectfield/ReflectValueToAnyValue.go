package reflectfield

import (
	"reflect"

	"gitlab.com/evatix-go/reflecthelper/isreflect"
)

func ReflectValueToAnyValue(rv reflect.Value) interface{} {
	if isreflect.Null(rv) {
		return nil
	}

	k := rv.Kind()

	switch k {
	case reflect.Ptr, reflect.Interface:
		return rv.Elem().Interface()
	default:
		return rv.Interface()
	}
}
