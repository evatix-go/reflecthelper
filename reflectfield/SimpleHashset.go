package reflectfield

type SimpleHashset struct {
	items map[string]bool
}

func (it *SimpleHashset) Add(item string) *SimpleHashset {
	it.items[item] = true

	return it
}

func (it *SimpleHashset) Adds(items ...string) *SimpleHashset {
	if items == nil {
		return it
	}

	for _, item := range items {
		it.items[item] = true
	}

	return it
}

func (it *SimpleHashset) Remove(item string) *SimpleHashset {
	_, has := it.items[item]

	if has {
		delete(it.items, item)
	}

	return it
}

func (it *SimpleHashset) Has(item string) bool {
	return it.items[item] == true
}

func (it *SimpleHashset) IsEmpty() bool {
	return it.Length() == 0
}

func (it *SimpleHashset) HasItems() bool {
	return it.Length() > 0
}

func (it *SimpleHashset) Length() int {
	if it == nil || it.items == nil {
		return 0
	}

	return len(it.items)
}
