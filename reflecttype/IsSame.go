package reflecttype

import (
	"reflect"
)

func IsSame(thisTypeJSON Identifier, kind reflect.Kind) bool {
	switch thisTypeJSON {
	case MapType:
		return kind == reflect.Map
	case StructType:
		return kind == reflect.Struct
	case SliceType:
		return kind == reflect.Slice
	case PrimitiveType:
		return isPrimitive(kind)
	default:
		return false
	}
}
