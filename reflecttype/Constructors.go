package reflecttype

func NewArbitraryMap(value *GenericMap) *Arbitrary {
	return &Arbitrary{
		GenericMap: value,
		ThisType:   MapType,
	}
}

func NewArbitrarySlice(value *GenericSlice) *Arbitrary {
	return &Arbitrary{
		GenericSlice: value,
		ThisType:     SliceType,
	}
}

func NewArbitraryStruct(value *GenericStruct) *Arbitrary {
	return &Arbitrary{
		GenericStruct: value,
		ThisType:      StructType,
	}
}

func NewArbitraryPrimitive(value *GenericPrimitive) *Arbitrary {
	return &Arbitrary{
		GenericPrimitive: value,
		ThisType:         PrimitiveType,
	}
}
