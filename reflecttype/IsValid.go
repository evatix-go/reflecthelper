package reflecttype

func IsTypeValid(typeNum Identifier) bool {
	return typeNum > dummyStart && typeNum < dummyEnd
}
