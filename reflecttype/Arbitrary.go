package reflecttype

import (
	"gitlab.com/evatix-go/reflecthelper/internal/defaulterr"
)

type Arbitrary struct {
	*GenericMap
	*GenericPrimitive
	*GenericSlice
	*GenericStruct
	ThisType Identifier
}

func (arbitrary *Arbitrary) Type() Identifier {
	return arbitrary.ThisType
}

func (arbitrary *Arbitrary) Map() (*GenericMap, error) {
	if arbitrary.Type() != MapType {
		return nil, defaulterr.TypeMismatchMapType
	}

	return arbitrary.GenericMap, nil
}

func (arbitrary *Arbitrary) Slice() (*GenericSlice, error) {
	if arbitrary.Type() != SliceType {
		return nil, defaulterr.TypeMismatchSliceType
	}

	return arbitrary.GenericSlice, nil
}

func (arbitrary *Arbitrary) Struct() (*GenericStruct, error) {
	if arbitrary.Type() != StructType {
		return nil, defaulterr.TypeMismatchStructType
	}

	return arbitrary.GenericStruct, nil
}

func (arbitrary *Arbitrary) Primitive() (*GenericPrimitive, error) {
	if arbitrary.Type() != PrimitiveType {
		return nil, defaulterr.TypeMismatchPrimitiveType
	}

	return arbitrary.GenericPrimitive, nil
}
