package reflecttype

// KeyValuePair stores key and value of a map
type KeyValuePair struct {
	Key   *Arbitrary
	Value *Arbitrary
}
