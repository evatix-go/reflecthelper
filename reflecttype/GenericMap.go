package reflecttype

// GenericMap
//
// Map stores map as a slice of KeyValuePair
type GenericMap struct {
	ResultKeyValuePairSlice *[]*KeyValuePair
	ReflectionType          string
}
