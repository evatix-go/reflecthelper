package reflectfields

import (
	"reflect"

	"gitlab.com/evatix-go/reflecthelper/internal/errmsg"
	"gitlab.com/evatix-go/reflecthelper/reflectfield"
)

// GetNamesUsingReflectVal returns a set of all struct fields
//
// input can be ptr, interface and will be
// reduced to struct and returns the hashset of field names for it
func GetNamesUsingReflectVal(rv reflect.Value) (*reflectfield.SimpleHashset, error) {
	structValue := rv
	structValueKind := structValue.Kind()

	for structValueKind == reflect.Ptr || structValueKind == reflect.Interface {
		// mutating dangerous code
		structValue = structValue.Elem()
		structValueKind = structValue.Kind()
	}

	if !structValue.IsValid() || structValueKind != reflect.Struct {
		return reflectfield.Empty.SimpleHashset(),
			errmsg.ExpectedButFound(reflect.Struct, structValueKind)
	}

	structType := structValue.Type()
	fieldsLength := structType.NumField()
	fieldsHashset :=
		reflectfield.New.SimpleHashset.Cap(fieldsLength)
	var name string

	for i := 0; i < fieldsLength; i++ {
		name = structType.Field(i).Name
		fieldsHashset.Add(name)
	}

	return fieldsHashset, nil
}
