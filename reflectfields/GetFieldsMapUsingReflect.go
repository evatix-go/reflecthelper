package reflectfields

import "reflect"

func GetFieldsMapUsingReflect(
	rv reflect.Value,
) map[string]reflect.StructField {
	structValue := rv
	structValueKind := structValue.Kind()

	for structValueKind == reflect.Ptr || structValueKind == reflect.Interface {
		// mutating dangerous code
		structValue = structValue.Elem()
		structValueKind = structValue.Kind()
	}

	if !structValue.IsValid() || structValueKind != reflect.Struct {
		return nil
	}

	structType := structValue.Type()
	fieldsLength := structType.NumField()
	fieldsHashset :=
		make(
			map[string]reflect.StructField,
			fieldsLength)

	var name string

	for i := 0; i < fieldsLength; i++ {
		field := structType.Field(i)
		name = field.Name
		fieldsHashset[name] = field
	}

	return fieldsHashset
}
