package reflectfields

import (
	"reflect"

	"gitlab.com/evatix-go/reflecthelper/reflectfield"
)

// GetNames returns a set of all struct fields
//
// input can be ptr, interface and will be
// reduced to struct and returns the hashset of field names for it
func GetNames(input interface{}) (*reflectfield.SimpleHashset, error) {
	structValue := reflect.ValueOf(input)

	return GetNamesUsingReflectVal(structValue)
}
