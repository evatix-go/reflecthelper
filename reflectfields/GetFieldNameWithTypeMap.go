package reflectfields

import "reflect"

func GetFieldNameWithTypeMap(input interface{}) map[string]reflect.Type {
	structValue := reflect.ValueOf(input)

	return GetFieldNameWithTypeMapUsingReflect(structValue)
}
