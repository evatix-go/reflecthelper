package reflectfields

import (
	"reflect"
)

// GetValuesToMap unlike GetPublicValuesToMap to map it collects
// all fields with values including the private ones.
//
// However, this one will be slower in performance than GetPublicValuesToMap.
//
// Under the hood it usages GetValuesToMapUsingReflectValue
func GetValuesToMap(inputStruct interface{}) (map[string]interface{}, error) {
	rv := reflect.ValueOf(inputStruct)

	return GetValuesToMapUsingReflectValue(rv)
}
