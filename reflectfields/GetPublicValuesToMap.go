package reflectfields

import (
	"reflect"
)

// GetPublicValuesToMap unlike GetPublicValuesToMap to map it collects
// all fields with values including the private ones.
//
// However, this one will be slower in performance than GetPublicValuesToMap.
func GetPublicValuesToMap(input interface{}) (map[string]interface{}, error) {
	rv := reflect.ValueOf(input)

	return GetPublicValuesToMapUsingReflectValue(rv)
}
