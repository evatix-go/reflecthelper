package reflectfields

import (
	"reflect"

	"gitlab.com/evatix-go/reflecthelper/reflectfield"
)

// GetNullFields
//
// Includes both public privates.
func GetNullFields(
	any interface{},
	level int,
) *reflectfield.SimpleHashset {
	rv := reflect.ValueOf(any)

	return GetNullFieldsUsingReflectVal(rv, level)
}
