package reflectfields

import (
	"reflect"

	"gitlab.com/evatix-go/reflecthelper/internal/errmsg"
)

// GetPublicValuesToMapUsingReflectValue only returns exported or public fields map value information.
func GetPublicValuesToMapUsingReflectValue(structValue reflect.Value) (
	map[string]interface{}, error,
) {
	if structValue.Kind() != reflect.Struct {
		return nil, errmsg.ExpectedButFound(reflect.Struct, structValue.Kind())
	}

	structType := structValue.Type()
	structNumFields := structType.NumField()
	fieldToValueMap := make(map[string]interface{}, structNumFields)

	for i := 0; i < structNumFields; i++ {
		fieldStruct := structType.Field(i)

		// ignore unexported fields
		if fieldStruct.PkgPath != "" {
			continue
		}

		field := structValue.Field(i)
		fieldToValueMap[fieldStruct.Name] = field.Interface()
	}

	return fieldToValueMap, nil
}
