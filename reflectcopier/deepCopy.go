package reflectcopier

import (
	"reflect"
)

// deepCopy copies everything from `from` to `to`
//
// @params:
//  - to:       addressable value of any type
// 	- from:     same type as `to`
//	- level:    start level, start from 0
//  - maxLevel: maximum depth level, (-1) for all level
func deepCopy(to, from reflect.Value, level, maxLevel int) {
	// exit the function if:
	//  - user doesn't want all level and (maxLevel == -1 means user wants all level)
	//  - we've reached the maximum level
	if maxLevel != -1 && level >= maxLevel {
		return
	}

	switch from.Kind() {
	case reflect.Ptr:
		// TODO: infinite recursion may occur
		// To solve this issue we may need to keep track of ptrs
		// in a map[uintptr]interface{}, if we encounter same ptr
		// second time we'll simply replace the value with the ptr

		// get the underlying value
		srcValue := from.Elem()

		if !srcValue.IsValid() {
			return
		}

		to.Set(reflect.New(srcValue.Type()))
		deepCopy(to.Elem(), srcValue, level+1, maxLevel)
	case reflect.Interface:
		// interface is nil, nothing to do
		if from.IsNil() {
			return
		}

		// get the underlying value
		srcValue := from.Elem()

		// create the from value type in to
		toValuePtr := reflect.New(from.Type())
		toValue := toValuePtr.Elem()

		deepCopy(toValue, srcValue, level+1, maxLevel)
		toValue.Set(srcValue)
	case reflect.Struct:
		srcType := from.Type()
		srcNumFields := srcType.NumField()

		for i := 0; i < srcNumFields; i++ {
			// field is unexported, ignore
			if srcType.Field(i).PkgPath != "" {
				continue
			}

			deepCopy(to.Field(i), from.Field(i), level+1, maxLevel)
		}
	case reflect.Slice:
		if from.IsNil() {
			return
		}

		srcSliceLen := from.Len()

		to.Set(reflect.MakeSlice(from.Type(), srcSliceLen, from.Cap()))

		for i := 0; i < srcSliceLen; i++ {
			deepCopy(to.Index(i), from.Index(i), level+1, maxLevel)
		}
	case reflect.Array:
		srcArrayLen := from.Len()

		// create new array ptr
		newArrayPtr := reflect.New(from.Type())
		newArray := newArrayPtr.Elem()
		to.Set(newArray)

		for i := 0; i < srcArrayLen; i++ {
			deepCopy(to.Index(i), from.Index(i), level+1, maxLevel)
		}
	default:
		to.Set(from)
	}
}
