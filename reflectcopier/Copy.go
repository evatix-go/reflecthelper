package reflectcopier

import (
	"reflect"

	"gitlab.com/evatix-go/reflecthelper/internal/errmsg"
)

// Deprecated:
// Copy copies everything from fromObj toObj
//
// @params:
//  - toObj:       addressable value of any type
// 	- fromObj:     same type as `toObj`
//  - maxDepth:    maximum depth level, (-1) for all level
//
// Returns: error
// 	- If toObj is not addressable
func Copy(toObj, fromObj interface{}, maxDepth int) error {
	if fromObj == nil {
		// fromObj is nil, do nothing
		return nil
	}

	fromValue := reflect.ValueOf(fromObj)
	toValue := reflect.ValueOf(toObj)

	if toValue.Kind() != reflect.Ptr {
		return errmsg.ExpectedButFound(reflect.Ptr, toValue.Kind())
	}

	// Get the underlying value
	toValue = toValue.Elem()

	// TODO: should we create the type? or let the user do it?
	toValue.Set(reflect.New(fromValue.Type()).Elem())
	deepCopy(toValue, fromValue, 0, maxDepth)

	return nil
}
