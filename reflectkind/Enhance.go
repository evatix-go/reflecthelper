package reflectkind

import "reflect"

type Enhance uint

const (
	Invalid Enhance = iota
	Bool
	Int
	Int8
	Int16
	Int32
	Int64
	Uint
	Uint8
	Uint16
	Uint32
	Uint64
	Uintptr
	Float32
	Float64
	Complex64
	Complex128
	Array
	Chan
	Func
	Interface
	Map
	Ptr
	Slice
	String
	Struct
	UnsafePointer
)

func (e Enhance) IsKind(kind reflect.Kind) bool {
	return e == Enhance(kind)
}

func (e Enhance) Is(another Enhance) bool {
	return e == another
}

// IsAny
//
// nil returns false
//
// returns true if any true
func (e Enhance) IsAny(anotherConditions ...Enhance) bool {
	if anotherConditions == nil {
		return false
	}

	for _, condition := range anotherConditions {
		if condition == e {
			return true
		}
	}

	return false
}

func (e Enhance) IsInvalid() bool {
	return e == Invalid
}

func (e Enhance) IsBool() bool {
	return e == Bool
}

func (e Enhance) IsInt() bool {
	return e == Int
}

func (e Enhance) IsInt8() bool {
	return e == Int8
}

func (e Enhance) IsInt16() bool {
	return e == Int16
}

func (e Enhance) IsInt32() bool {
	return e == Int32
}

func (e Enhance) IsInt64() bool {
	return e == Int64
}

func (e Enhance) IsUint() bool {
	return e == Uint
}

func (e Enhance) IsUint8() bool {
	return e == Uint8
}

func (e Enhance) IsUint16() bool {
	return e == Uint16
}

func (e Enhance) IsUint32() bool {
	return e == Uint32
}

func (e Enhance) IsUint64() bool {
	return e == Uint64
}

func (e Enhance) IsUintptr() bool {
	return e == Uintptr
}

func (e Enhance) IsFloat32() bool {
	return e == Float32
}

func (e Enhance) IsFloat64() bool {
	return e == Float64
}

func (e Enhance) IsComplex64() bool {
	return e == Complex64
}

func (e Enhance) IsComplex128() bool {
	return e == Complex128
}

func (e Enhance) IsArray() bool {
	return e == Array
}

func (e Enhance) IsChan() bool {
	return e == Chan
}

func (e Enhance) IsFunc() bool {
	return e == Func
}

func (e Enhance) IsInterface() bool {
	return e == Interface
}

func (e Enhance) IsMap() bool {
	return e == Map
}

func (e Enhance) IsPtr() bool {
	return e == Ptr
}

func (e Enhance) IsSlice() bool {
	return e == Slice
}

func (e Enhance) IsString() bool {
	return e == String
}

func (e Enhance) IsStruct() bool {
	return e == Struct
}

func (e Enhance) IsUnsafePointer() bool {
	return e == UnsafePointer
}

func (e Enhance) IsArrayOrSlice() bool {
	return e == Array || e == Slice
}

func (e Enhance) IsArrayOrSliceOrMap() bool {
	return e == Array || e == Slice || e == Map
}

// IsInteger
//
// int, int8, 16, 32, 64
func (e Enhance) IsInteger() bool {
	return e == Int ||
		e == Int8 ||
		e == Int16 ||
		e == Int32 ||
		e == Int64
}

// IsFloatingPoint
//
// float32, 64
func (e Enhance) IsFloatingPoint() bool {
	return e == Float32 ||
		e == Float64
}

// IsUnsignedInteger
//
// uint, uint8, 16, 32, 64
func (e Enhance) IsUnsignedInteger() bool {
	return e == Uint ||
		e == Uint8 ||
		e == Uint16 ||
		e == Uint32 ||
		e == Uint64
}

// IsStructOrInterface
//
// uint, uint8, 16, 32, 64
func (e Enhance) IsStructOrInterface() bool {
	return e == Struct ||
		e == Interface
}

func (e Enhance) IsPointerOrInterface() bool {
	return e == Ptr ||
		e == Interface
}
