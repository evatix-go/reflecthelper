package main

import (
	"fmt"

	"github.com/davecgh/go-spew/spew"

	"gitlab.com/evatix-go/reflecthelper"
	"gitlab.com/evatix-go/reflecthelper/reflectfield"
)

func main() {
	// a := 9
	// b := 0
	//
	// reflecthelper.Get(a, &b)
	//
	// fmt.Println(a, b)
	//
	// fmt.Println(isreflect.Primitive(reflect.Struct))

	// TestFieldMap()
	// TestWithDifferentFieldType()
	// TestSameNameDifferentType()
	// TestNewGetter()
	// TestNewGetterArray()
	// TestNewGetterTypeConvertible()
	nestedFieldsExpandExampleTest01()
}

func TestWithDifferentFieldType() {
	type A struct {
		i int
	}

	type C struct {
		i string
	}

	a := A{65}
	c := C{}

	reflecthelper.Get(nil, a, &c)

	fmt.Printf("%#v\n", a)
	fmt.Printf("%#v\n", c)
}

func TestFieldMap() {
	type T struct {
		a int `json:"blah",xml:"more_blah"`
		b string
		k *int
		c struct {
			p []int
			q *int
		}
	}

	t := T{
		a: 12,
		b: "Hello",
		k: nil,
		c: struct {
			p []int
			q *int
		}{
			p: []int{1, 2, 3},
			q: new(int),
		},
	}

	f := reflectfield.New.StructFieldsMap.Any(1, &t)

	aw := f.GetWrapper("a")

	spew.Dump(aw)

	// spew.Dump((*f.Wrappers)[:2])

	// spew.Dump(f)
}

/*

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"reflect"
	"strings"

	"github.com/davecgh/go-spew/spew"
	"github.com/go-test/deep"

	"gitlab.com/evatix-go/reflecthelper"
	"gitlab.com/evatix-go/reflecthelper/reflectcopier"
	"gitlab.com/evatix-go/reflecthelper/deepserialize"
	"gitlab.com/evatix-go/reflecthelper/errorwrapper"
	"gitlab.com/evatix-go/reflecthelper/reflectcast"
)

type t struct {
	A int
	B string
}

func main() {
	// log.SetFlags(0)

	// uncomment for test each case
	// generalTest()
	// testSlice()
	// testCopy()
	// testCopyStructPtr()
	// testCopyArray()
	// testLinkedList()

	// testRawStructDataMap()
	// testSerializerStructSlice()
	// testSerializeAndDeserialize()
	// testMapSerialization()
	// testMapStructAsKey()
	// testArraySerializer()
	// testReflectCast()
	// testStructToMap()
	// testDeepSerializeEncode()
	// testDeepSerializeEncodeJSON()
	// testDeserializeSlice()
	// testGetInt()
	// testGet()
	// testErr()
}

func generalTest() {
	// int cases
	srcInt := 123
	destInt := 0
	err := reflecthelper.Get(&destInt, srcInt)

	if err != nil {
		log.Println(err)
	}

	fmt.Println(srcInt, destInt)

	// slice append
	srcSlice := []int{1, 2, 3, 4}
	destSlice := []int{0}

	err = reflecthelper.Get(&destSlice, srcSlice)

	if err != nil {
		log.Println(err)
	}

	log.Println(srcSlice, destSlice)

	// slice of struct
	tSliceSrc := []t{
		{A: 32, B: "Hello"},
		{A: 99, B: "World"},
	}

	var tSliceDst []t

	reflecthelper.Get(&tSliceDst, tSliceSrc)

	log.Printf("%#v %#v", tSliceSrc, tSliceDst)
}

func testSlice() {
	a := 7
	b := 8
	// Example with struct types
	type t struct {
		A *int
		B string
	}

	sliceOfStructSrc := []t{{&a, "Hello"}, {&b, "World"}}
	var sliceOfStructDest []t
	e := reflecthelper.Get(&sliceOfStructDest, sliceOfStructSrc)

	if e != nil {
		fmt.Println(e)
	}

	fmt.Println(sliceOfStructSrc, sliceOfStructDest)
}

func testCopy() {
	src1DSlice := []int{1, 2, 3}
	var dest1DSlice []int
	reflectcopier.Copy(&dest1DSlice, src1DSlice)
	fmt.Println(src1DSlice, dest1DSlice)

	src2DSlice := [][]int{{1, 2}, {3, 4}}
	dest2DSlice := make([][]int, 0)
	reflectcopier.Copy(&dest2DSlice, src2DSlice)
	fmt.Println(src2DSlice, dest2DSlice)

	type S struct {
		A int
		B *string
	}

	helloWorld := "Hello World"
	srcStruct := S{77, &helloWorld}
	destStruct := S{}
	reflectcopier.Copy(&destStruct, srcStruct)
	fmt.Printf("src: %#v dest %#v\n", srcStruct, destStruct)
	fmt.Printf("src: %#v dest %#v\n", *srcStruct.B, *destStruct.B)

	type P struct {
		A int
		b rune
		C string
	}

	srcStructSlice := []P{
		{3, 'A', "Hello"},
		{4, 'B', "World"},
	}
	var destStructSlice []P
	reflectcopier.Copy(&destStructSlice, srcStructSlice)
	fmt.Printf("src: %#v dest %#v\n", srcStructSlice, destStructSlice)
}

func testCopyStructPtr() {
	type A struct {
		V   int
		S   *string
		Arr *[]int
	}
	type Sp struct {
		P *A
	}

	s := "Hello World"

	srcSp := Sp{&A{17, &s, &[]int{1, 2, 3}}}
	destSp := Sp{}

	reflecthelper.Get(&destSp, srcSp)
	fmt.Println(reflect.DeepEqual(srcSp, destSp))
}

func testCopyArray() {
	p, q := 1, 2
	s := [2]*int{&p, &q}
	var d [2]*int

	e := reflectcopier.Copy(&d, s)

	if e != nil {
		log.Fatal(e)
	}

	fmt.Printf("src: %#v, dest: %#v\n", s, d)
}

func testLinkedList() {
	type Node struct {
		Value int
		Next  *Node
	}

	n := Node{7, nil}
	n.Next = &Node{8, nil}
	n.Next.Next = &Node{9, nil}

	var cpy Node

	reflectcopier.Copy(&cpy, n)

	fmt.Println("Printing src: ")

	i := 0
	ptr := &n
	for ptr != nil {
		fmt.Print(strings.Repeat(" ", i))
		fmt.Println(ptr.Value, ptr)
		ptr = ptr.Next
		i += 4
	}

	fmt.Println("Printing dest: ")

	i = 0
	ptr = &cpy
	for ptr != nil {
		fmt.Print(strings.Repeat(" ", i))
		fmt.Println(ptr.Value, ptr)
		ptr = ptr.Next
		i += 4
	}
}

func testRawStructDataMap() {
	type Another struct {
		P    int
		iptr *int32
	}
	type S struct {
		A       int
		S       string
		P       rune
		N       *S
		more    *Another
		andMore Another
	}

	var i, j, k int32 = 1, 2, 3
	s := S{99, "Hello World", 'P', nil, &Another{3, &i}, Another{2, &j}}
	s.N = &S{A: 77, S: "1 level deep", P: 'U', N: nil, more: &Another{9, &j}, andMore: Another{15, &k}}
	s.N.N = &S{23, "Opps!", 'Z', nil, nil, Another{32, &i}}

	out, _ := deepserialize.Encode(s)

	spew.Dump(out)

	var dest S

	deepserialize.Decode(&dest, out)

	spew.Dump(dest)
	spew.Dump(s)

	fmt.Println("----- ", reflect.DeepEqual(s, dest))
}

func testSerializerStructSlice() {
	type p struct {
		i *int
	}
	type S struct {
		a int
		b *[]*p
	}

	i, j := 1, 2
	p1, p2 := p{&i}, p{&j}
	b := []*p{&p1, &p2}

	s := S{33, &b}
	out, _ := deepserialize.Encode(s)

	var d S
	deepserialize.Decode(&d, out)

	spew.Dump(out)
	spew.Dump(s)
	spew.Dump(d)

	fmt.Println("--- reflect deepequal", reflect.DeepEqual(s, d))
}

func testSerializeAndDeserialize() {
	type S struct {
		a int
		p *int32
		n *S
	}

	i := int32(32)
	j := int32(i + 9)

	s := S{99, &i, nil}
	s.n = &S{12, &j, nil}
	d := S{}

	out, _ := deepserialize.Encode(s)

	deepserialize.Decode(&d, out)

	spew.Dump(s)
	spew.Dump(d)
}

func testMapSerialization() {
	type S struct {
		m *map[string]int
	}

	s := S{&map[string]int{"one": 1, "two": 2}}

	var d S

	e, _ := deepserialize.Encode(s)
	deepserialize.Decode(&d, e)

	spew.Dump(e)
	spew.Dump(s)
	spew.Dump(d)

	fmt.Println("--- Eq: ", reflect.DeepEqual(s, d))
}

func testMapStructAsKey() {
	type Key struct {
		i float64
		s string
	}

	type S struct {
		m map[Key]int
	}

	s := S{map[Key]int{{1, "one"}: 1, {2, "two"}: 2}}

	var d S

	e, _ := deepserialize.Encode(s)
	deepserialize.Decode(&d, e)

	conf := spew.NewDefaultConfig()
	conf.DisablePointerAddresses = true
	conf.SortKeys = true

	conf.Dump(e)
	// spew.Dump(s)
	// spew.Dump(d)

	fmt.Println("--- Eq: ", reflect.DeepEqual(s, d))

	fmt.Println("JSON test:")

	bytes, err := json.Marshal(e)

	if err != nil {
		panic(err)
	}

	// fmt.Println(string(bytes))

	fmt.Println(strings.Repeat("-=-", 10))

	val, errVal := deepserialize.EncodeJSON(bytes)

	if errVal != nil {
		fmt.Println(errVal.String())
	}

	conf.Dump(val)

	fmt.Println("e == val: ", reflect.DeepEqual(e, val))

	// var uu reflecttype.GenericStruct
	// err = json.Unmarshal(bytes, &uu)

	// var dd S

	// serializer.Decode(&dd, uu)
	// spew.Dump(uu)
	// spew.Dump(dd)
}

func testArraySerializer() {
	pp := spew.NewDefaultConfig()
	pp.DisablePointerAddresses = true
	pp.SortKeys = true

	type arr struct {
		a []int
	}

	aa := arr{[]int{1, 2, 3}}

	var d arr

	e, _ := deepserialize.Encode(aa)
	deepserialize.Decode(&d, e)

	pp.Dump(aa)
	pp.Dump(e)
	pp.Dump(d)
}

func testReflectCast() {
	type T struct {
		a int
		b string
	}

	type actual struct {
		b    string
		k    int
		abc  *T
		nnn  interface{}
		next *actual
	}

	type Model struct {
		B    int
		r    int
		Abc  *T
		nnn  interface{}
		Next *Model
	}

	// i := 17

	es := Model{12, 88, &T{17, "Hello World"}, nil, nil}
	es.Next = &es
	us := actual{}

	ess := Model{}

	emap, err := reflectcast.ToActual(es, &us)

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Converting: model to actual")
	spew.Dump(emap)
	fmt.Println()
	fmt.Println()

	fmt.Println("Converting: actual to model")
	emap, err = reflectcast.ToModel(us, &ess)

	if err != nil {
		fmt.Println(err)
	}

	spew.Dump(emap)
	fmt.Println()
	fmt.Println()

	spew.Dump(es)
	spew.Dump(us)
	spew.Dump(ess)
}

func testStructToMap() {
	type S struct {
		K interface{}
		a int
		b string
	}

	s := S{'A', 18, "Hello World"}

	mp, _ := reflecthelper.StructToMap(s)
	mp2, _ := reflecthelper.StructToMapUnexported(s)

	spew.Dump(mp)
	spew.Dump(mp2)
}

func testDeepSerializeEncode() {
	type E struct {
		s string
	}
	type T struct {
		i int
		b string
		C E
		K []int
	}

	t := T{i: 2, b: "Hello", C: E{"World"}, K: []int{5, 7, 9}}

	encoded, err := deepserialize.Encode(t)
	if err != nil {
		fmt.Println(err)
	}
	spew.Dump(encoded)

	d := T{}

	err = deepserialize.Decode(&d, encoded)

	if err != nil {
		fmt.Println("Decode Error:", err)
	}

	diff := deep.Equal(t, d)
	if diff != nil {
		fmt.Println(diff)
	} else {
		fmt.Println("equals")
	}

	spew.Dump(d)
	spew.Dump(t)

	data, e := json.Marshal(encoded)

	if e != nil {
		fmt.Println(e)
	}

	fmt.Println(string(data))
}
func testDeepSerializeEncodeJSON() {
	type s struct {
		a int
		b float64
		c *int
	}

	type T struct {
		name    string
		slice   []int
		mp      map[string]int
		another s
	}

	t := T{
		name:  "John Doe",
		slice: []int{1, 2, 3, 34},
		mp: map[string]int{
			"one": 1,
			"two": 2,
		},
		another: s{
			a: 9999,
			b: 3.1315,
			c: new(int),
		},
	}
	t.mp = nil

	encoded, errEncode := deepserialize.Encode(t)

	fmt.Println("Encode errors", errEncode.String())

	spew.Dump(encoded)

	data, errEncodeJson := deepserialize.EncodeJSON(t)

	fmt.Println("Encode JSON errors", errEncodeJson.String())

	fmt.Println(string(data))

	dest := T{}

	e := deepserialize.DecodeJSON(data, &dest)

	fmt.Println("Decode JSON errors", e.String())

	spew.Dump(t)
	spew.Dump(dest)

	diff := deep.Equal(dest, t)
	if diff != nil {
		fmt.Println(diff)
	} else {
		fmt.Println("equals")
	}
}

func testDeserializeSlice() {
	s := []int{1, 2, 3}
	gt, e := deepserialize.Encode(s)

	if e != nil {
		fmt.Println(e)
	}

	encodedJson, _ := deepserialize.EncodeJSON(s)

	spew.Dump(gt)
	fmt.Println(string(encodedJson))
	d := []int{}
	e = deepserialize.DecodeJSON(encodedJson, &d)
	if e != nil {
		fmt.Println(e)
	}

	spew.Dump(s)
	spew.Dump(d)
}

func testGetInt() {
	j := 8

	var i *int

	reflecthelper.Get(&i, j)
	fmt.Println(i, j)
}

func testGetStruct() {
	type t struct {
		i *int
		s string
	}

	type u struct {
		i int
		s string
	}

	i := 18
	t1 := t{&i, "Hello world"}
	t2 := u{19, "Some random string"}

	reflecthelper.Get(&t1, t2)

	spew.Dump(t1, t2)
}

func testGetSlice() {
	s := []int{1, 2, 3}
	d := []int{}

	reflecthelper.Get(&d, s)

	spew.Dump(s, d)
}

func testGetArray() {
	s := [3]int{1, 2, 3}
	var d *[3]int

	reflecthelper.Get(&d, s)

	spew.Dump(s, d)
}

func testGetInterface() {
	s := "Hello World"
	var d interface{}

	reflecthelper.Get(&d, s)

	spew.Dump(s, d)
}

func testGetComplexStruct() {
	type t1 struct {
		i int
		s string
	}

	type t2 struct {
		a []int
		b string
	}

	type S struct {
		A int
		*t2
		t t1
	}

	s := &S{8, &t2{[]int{12, 14}, "Hello World"}, t1{101, "Oops"}}
	var d S
	reflecthelper.Get(&d, s)

	spew.Dump(s, d)
}

func testGetMap() {
	m1 := map[string]int{
		"one": 1,
		"two": 2,
	}

	m2 := map[string]int{}

	reflecthelper.Get(&m2, m1)

	spew.Dump(m1, m2)
}

func testErr() {
	ew := errorwrapper.NewErrorWrapper()
	ew.Add(errors.New("error 1"))
	ew.Add(errors.New("error 2"))
	ew.Add(errors.New("error 3"))

	fmt.Println(ew.String())

	ew2 := errorwrapper.NewErrorWrapper()
	ew2.Add(errors.New("another err 1"))
	ew2.Add(errors.New("another err 2"))
	ew2.Add(errors.New("another err 3"))

	fmt.Println(ew2.String())

	ew.AppendInto(ew2)

	fmt.Println(ew)

}

func testGet() {
	// testGetSlice()
	// testGetStruct()
	// testGetArray()
	// testGetInterface()
	// testGetComplexStruct()
	testGetMap()
}
*/
func TestSameNameDifferentType() {
	type T1 struct {
		a []int
		b string
	}

	type T2 struct {
		a string
		b []int
	}

	original := T1{
		a: []int{1, 2, 3, 4},
		b: "Hello World",
	}

	var cpy T2

	err := reflecthelper.Get(nil, original, &cpy)

	fmt.Println(err)
}

func TestNewGetter() {
	type T1 struct {
		a []int
		b string
	}

	type T2 struct {
		a string
		b []int
	}

	original := T1{
		a: []int{1, 2, 3, 4},
		b: "Hello World",
	}

	var cpy T2

	reflecthelper.Get(nil, original, &cpy)
}

func TestNewGetterArray() {
	type T1 struct {
		a [2]int
	}

	type T2 struct {
		a [3]int
	}

	original := T1{
		a: [...]int{1, 2},
	}

	var cpy T2

	// reflecthelper.Get(original, &cpy)

	reflecthelper.Get(nil, original, &cpy)
	fmt.Printf("%#v\n", original)
	fmt.Printf("%#v\n", cpy)
}

func TestNewGetterTypeConvertible() {
	type T1 struct {
		a int
	}

	type T2 struct {
		a uint8
	}

	original := T1{
		a: 255,
	}

	var cpy T2

	// reflecthelper.Get(original, &cpy)

	reflecthelper.Get(nil, original, &cpy)
	fmt.Printf("%#v\n", original)
	fmt.Printf("%#v\n", cpy)
}
