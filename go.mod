module gitlab.com/evatix-go/reflecthelper

go 1.15

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/smartystreets/goconvey v1.7.2
)
