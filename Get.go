package reflecthelper

// Get gets the data in to out by using reflection
//
// Example
//  TODO example
//
func Get(opt *Options, from interface{}, to interface{}) error {
	g := newGetter(opt, false)

	return g.Get(from, to)
}
